const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')
const { logEvent } = require('component-activity-log/server')

const useCases = require('./use-cases')
const s3Service = require('./s3Service')

const resolvers = {
  Query: {},
  Mutation: {
    async deleteFile(_, { fileId }, ctx) {
      return useCases.deleteFileUseCase
        .initialize({ logEvent, models, s3Service })
        .execute({ fileId, reqUserId: ctx.user })
    },
    async getSignedUrl(_, { fileId }, ctx) {
      return useCases.getSignedUrlUseCase
        .initialize({ models, s3Service })
        .execute(fileId)
    },
    async uploadFile(_, params, ctx) {
      return useCases.uploadFileUseCase
        .initialize({ logEvent, models, s3Service })
        .execute({ params, reqUserId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
