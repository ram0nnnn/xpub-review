const initialize = ({ logEvent, models: { File, Manuscript }, s3Service }) => ({
  execute: async ({ fileId, reqUserId }) => {
    const file = await File.find(fileId)
    await s3Service.delete(file.fileName)

    if (file.manuscriptId) {
      const manuscript = await Manuscript.find(file.manuscriptId)
      if (manuscript.status !== Manuscript.Statuses.draft) {
        logEvent({
          userId: reqUserId,
          manuscriptId: manuscript.id,
          action: logEvent.actions.file_removed,
          objectType: logEvent.objectType.file,
          objectId: file.id,
        })
      }
    }
    await file.delete()
  },
})

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
