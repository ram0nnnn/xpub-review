import React from 'react'
import { withProps } from 'recompose'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import {
  Text,
  Fade,
  Icon,
  Label,
  Loader,
  marginHelper,
} from 'component-hindawi-ui'

import { fileHasPreview, parseFileSize } from '../fileUtils'
import { useFileDownload } from '../decorators'

const FileLayout = ({
  onDelete,
  fileSize,
  hasDelete = false,
  item: file,
  hasPreview,
  dragHandle = null,
  onPreview = () => () => {},
  ...rest
}) => {
  const { isFetching, downloadFile, fetchingError } = useFileDownload(file)
  return (
    <Root data-test-id={`file-${file.id}`} {...rest}>
      {typeof dragHandle === 'function' ? dragHandle() : dragHandle}
      <FileInfo>
        {file.originalName}
        <Label ml={1}>{fileSize}</Label>
      </FileInfo>

      {hasPreview && (
        <Icon
          color="colorSecondary"
          data-test-id={`${file.id}-preview`}
          fontSize="15px"
          icon="preview"
          ml={1}
          onClick={onPreview(file)}
          secondary
        />
      )}

      {isFetching ? (
        <Loader iconSize={2} mb={1 / 2} ml={1} mr={1} />
      ) : (
        <Icon
          color="colorSecondary"
          data-test-id={`${file.id}-preview`}
          fontSize="15px"
          icon="download"
          ml={1}
          mr={1}
          onClick={downloadFile}
          secondary
        />
      )}

      {hasDelete && (
        <Icon
          color="colorSecondary"
          data-test-id={`${file.id}-delete`}
          fontSize="14px"
          icon="delete"
          ml={1}
          mr={1}
          onClick={onDelete}
          secondary
        />
      )}
      {fetchingError && (
        <Fade>
          <ErrorWrapper>
            <Text error>{fetchingError}</Text>
          </ErrorWrapper>
        </Fade>
      )}
    </Root>
  )
}

export default withProps(({ item }) => ({
  fileSize: parseFileSize(item),
  hasPreview: fileHasPreview(item),
}))(FileLayout)

// #region styles
const Root = styled.div`
  align-items: center;
  background-color: ${th('colorBackgroundHue')};
  box-shadow: ${({ shadow }) => (shadow ? th('boxShadow') : 'none')};
  border-radius: ${th('borderRadius')};
  display: flex;
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  height: calc(${th('gridUnit')} * 5);
  position: relative;
  white-space: nowrap;

  ${marginHelper};
`

const ErrorWrapper = styled.div`
  position: absolute;
  top: calc(${th('gridUnit')} * 5);
  left: 0;

  width: calc(${th('gridUnit')} * 20);
`

const FileInfo = styled.div`
  align-items: center;
  border-right: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  display: flex;
  flex: 1;
  height: inherit;
  justify-content: space-between;
  padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')} 0 ${th('gridUnit')};
`
// #endregion
