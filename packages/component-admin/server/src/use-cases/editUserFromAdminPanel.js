const config = require('config')
const { omit, flatMap } = require('lodash')

const initialize = ({ User, TeamMember, Team, Journal }) => ({
  execute: async ({ input, id, currentUser }) => {
    const user = await User.find(id, 'identities')

    const localIdentity = user.getDefaultIdentity()

    const identityInput = { ...omit(input, ['role']) }

    localIdentity.updateProperties(identityInput)

    await localIdentity.save()
    const teamMembers = await TeamMember.findBy({ userId: id }, 'team.members')
    const teams = flatMap(teamMembers, tm => tm.team)

    const oldTeam = teams.find(
      team => team.role !== 'author' && team.role !== 'reviewer',
    )
    if (oldTeam && oldTeam.role !== input.role) {
      if (oldTeam.role === 'admin' && currentUser === user.id) {
        throw new Error('Admin role cannot be changed.')
      } else {
        const teamMemberId = teamMembers.find(
          teamM => teamM.teamId === oldTeam.id,
        ).id
        oldTeam.removeMember(teamMemberId)
        await oldTeam.saveRecursively()
      }
    }

    if (
      input.role !== 'user' &&
      ((oldTeam && oldTeam.role !== input.role) || !oldTeam)
    ) {
      let userTeam = await Team.findOneBy({
        queryObject: { role: input.role, manuscriptId: null },
        eagerLoadRelations: 'members.[user.[identities]]',
      })
      if (!userTeam) {
        const journal = await Journal.findOneByField(
          'title',
          config.get('journal.name'),
        )
        userTeam = new Team({
          role: input.role,
          journalId: journal.id,
        })
      }
      userTeam.addMember(user)
      await userTeam.saveRecursively()
    }
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
