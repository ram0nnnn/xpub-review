const uuid = require('uuid')
const config = require('config')

const initialize = ({
  notification: notificationService,
  models: { User, Identity, Team, Journal },
}) => ({
  execute: async input => {
    const userIdentity = await Identity.findOneByField('email', input.email)
    if (userIdentity) {
      throw new Error('User already exists in the database.')
    }
    const user = new User({
      defaultIdentity: 'local',
      isActive: true,
      isSubscribedToEmails: true,
      confirmationToken: uuid.v4(),
      unsubscribeToken: uuid.v4(),
      agreeTc: true,
    })

    const identity = new Identity({
      type: 'local',
      isConfirmed: false,
      passwordHash: null,
      givenNames: input.givenNames,
      surname: input.surname,
      email: input.email,
      aff: input.aff,
      title: input.title,
      userId: user.id,
      country: input.country,
    })
    await user.assignIdentity(identity)

    if (input.role === 'user') {
      await user.saveRecursively()
    } else {
      let team = await Team.findOneBy({
        queryObject: { role: input.role, manuscriptId: null },
        eagerLoadRelations: 'members.[user.[identities]]',
      })

      if (!team) {
        const journal = await Journal.findOneByField(
          'title',
          config.get('journal.name'),
        )
        team = new Team({
          role: input.role,
          journalId: journal.id,
        })
      }

      team.addMember(user)
      await team.saveRecursively()
    }

    await notificationService.notifyUserAddedByAdmin({
      user,
      identity,
      role: input.role,
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
