const initialize = ({ User, Team }) => ({
  execute: async ({ id, currentUser }) => {
    const user = await User.find(id)
    if (!user.isActive) return

    const adminTeam = await Team.findOneByField('role', 'admin', 'members')
    const admin = adminTeam.members.find(member => member.userId === user.id)
    if (admin && currentUser === user.id) {
      throw new Error('Admin cannot be deactivated.')
    } else {
      user.updateProperties({ isActive: false })
      await user.save()
    }
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
