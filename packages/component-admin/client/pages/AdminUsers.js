import React from 'react'
import { get } from 'lodash'
import { graphql } from 'react-apollo'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'
import { withModal, Modal } from 'component-modal'
import { compose, withHandlers, withProps } from 'recompose'

import {
  ActionLink,
  Breadcrumbs,
  Icon,
  Item,
  Label,
  MultiAction,
  Pagination,
  Row,
  withPagination,
  withRoles,
} from 'component-hindawi-ui'

import AdminUserForm from '../components/AdminUserForm'
import withUsersGQL from '../graphql'
import { currentUser } from '../graphql/queries'

const Users = ({
  page,
  history,
  currentUser,
  journal,
  addUserFromAdmin,
  editUserFromAdmin,
  getUserName,
  getUserRole,
  itemsPerPage,
  getStatusLabel,
  paginatedItems,
  toggleUserStatus,
  ...rest
}) => (
  <Root>
    <Row alignItems="center" justify="space-between" mb={1}>
      <Item alignItems="center">
        <Breadcrumbs mr={2}>Admin Dashboard</Breadcrumbs>
        <Modal
          component={AdminUserForm}
          modalKey="addUserFromAdmin"
          onConfirm={addUserFromAdmin}
        >
          {showModal => (
            <ActionLink
              data-test-id="add-user"
              fontWeight={600}
              onClick={showModal}
            >
              <Icon fontSize="12px" icon="expand" mr={1 / 2} />
              ADD USER
            </ActionLink>
          )}
        </Modal>
      </Item>

      <Pagination {...rest} itemsPerPage={itemsPerPage} page={page} />
    </Row>

    <Table>
      <thead>
        <tr>
          <th>
            <Label>Full Name</Label>
          </th>
          <th colSpan={2}>
            <Label>Email</Label>
          </th>
          <th>
            <Label>Affiliation</Label>
          </th>
          <th>
            <Label>Role</Label>
          </th>
          <th>
            <Label>Status</Label>
          </th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        {paginatedItems.map(user => (
          <UserRow key={user.id}>
            <Td>{getUserName(user)}</Td>
            <Td colSpan={2}>{user.email}</Td>
            <Td>{user.aff}</Td>
            <Td customId>{getUserRole(user)}</Td>
            <Td secondary>{getStatusLabel(user)}</Td>

            <HiddenCell>
              <Modal
                component={AdminUserForm}
                edit
                modalKey={`editUserFromAdmin-${user.id}`}
                onConfirm={editUserFromAdmin}
                user={user}
              >
                {showModal => (
                  <Icon icon="edit" iconSize={2} onClick={showModal} />
                )}
              </Modal>
              {user.id !== currentUser.id && (
                <ActionLink
                  data-test-id="deactivate-user"
                  flex={1}
                  fontWeight={700}
                  onClick={toggleUserStatus(user)}
                >
                  {user.isActive ? 'DEACTIVATE' : 'ACTIVATE'}
                </ActionLink>
              )}
            </HiddenCell>
          </UserRow>
        ))}
      </tbody>
    </Table>
  </Root>
)

// #region compose
export default compose(
  withJournal,
  withRoles,
  withUsersGQL,
  withModal({
    component: MultiAction,
    modalKey: 'deactivateUserFromAdmin',
  }),
  withProps(({ journal: { roles = {} }, data }) => ({
    roles: Object.keys(roles),
    items: get(data, 'getUsersForAdminPanel', []).map(user => ({
      id: get(user, 'id'),
      givenNames: get(user, 'identities[0].name.givenNames', ''),
      surname: get(user, 'identities[0].name.surname', ''),
      aff: get(user, 'identities[0].aff', ''),
      email: get(user, 'identities[0].email', ''),
      title: get(user, 'identities[0].name.title', ''),
      isActive: get(user, 'isActive', false),
      isConfirmed: get(user, 'identities[0].isConfirmed', false),
      country: get(user, 'identities[0].country', ''),
      role: get(user, 'role'),
    })),
  })),
  withPagination,
  graphql(currentUser),
  withProps(({ data }) => ({
    currentUser: get(data, 'currentUser', {}),
  })),
  withHandlers({
    getStatusLabel: () => ({ admin, isConfirmed, isActive = true }) => {
      if (admin) return 'ACTIVE'
      if (!isActive) {
        return 'INACTIVE'
      }
      return isConfirmed ? 'ACTIVE' : 'INVITED'
    },
    addUserFromAdmin: ({ addUserFromAdminPanel }) => (
      input,
      { setFetching, hideModal, setError, clearError },
    ) => {
      clearError()
      setFetching(true)
      addUserFromAdminPanel({
        variables: {
          input,
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    editUserFromAdmin: ({ editUserFromAdminPanel }) => (
      { id, givenNames, surname, aff, title, role, country },
      { hideModal, setFetching, setError, clearError },
    ) => {
      clearError()
      setFetching(true)
      editUserFromAdminPanel({
        variables: {
          id,
          input: {
            givenNames,
            surname,
            aff,
            title,
            role,
            country,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    getUserName: () => user => {
      if (user.admin) {
        return 'Admin'
      }
      return `${user.givenNames || ''} ${user.surname || ''}`
    },
    getUserRole: ({ journal: { roles = {} } }) => user => {
      let role
      if (user.role !== 'user') {
        role = roles[user.role]
      }

      return role || 'User'
    },
  }),
  withHandlers({
    toggleUserStatus: ({
      activateUser,
      deactivateUser,
      getUserName,
      showModal,
    }) => user => () => {
      showModal({
        modalKey: 'deactivateUserFromAdmin',
        title: `Are you sure you want to ${
          user.isActive ? 'deactivate' : 'activate'
        } user?`,
        subtitle: getUserName(user),
        confirmText: 'YES',
        cancelText: 'CLOSE',
        onConfirm: ({ hideModal, setFetching, setError, clearError }) => {
          clearError()
          setFetching(true)
          user.isActive
            ? deactivateUser({
                variables: {
                  id: user.id,
                },
              })
                .then(() => {
                  setFetching(false)
                  hideModal()
                })
                .catch(e => {
                  setFetching(false)
                  setError(e.message)
                })
            : activateUser({
                variables: {
                  id: user.id,
                },
              })
                .then(() => {
                  setFetching(false)
                  hideModal()
                })
                .catch(e => {
                  setFetching(false)
                  setError(e.message)
                })
        },
      })
    },
  }),
)(Users)
// #endregion

// #region styled-components
const colorFn = props => {
  if (props.secondary) {
    return th('colorSecondary')
  }

  if (props.customId) {
    return th('colorPrimary')
  }

  return th('colorText')
}

const Table = styled.table`
  border-collapse: collapse;
  margin-bottom: calc(${th('gridUnit')} * 4);
  width: 100%;

  & th {
    border: none;
    height: calc(${th('gridUnit')} * 5);
    padding-left: calc(${th('gridUnit')} * 2);
    text-align: start;
    vertical-align: middle;
  }
`

const Td = styled.td`
  border: none;
  color: ${colorFn};
  height: calc(${th('gridUnit')} * 5);
  padding-left: calc(${th('gridUnit')} * 2);
  text-align: start;
  vertical-align: middle;
`

const HiddenCell = styled(Td)`
  align-items: center;
  display: flex;
  justify-content: flex-start;
  opacity: 0;
`

const UserRow = styled.tr`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom: 1px solid ${th('colorBorder')};

  &:hover {
    background-color: ${th('colorBackgroundHue3')};

    ${HiddenCell} {
      opacity: 1;
    }
  }
`

const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 10);
  padding-top: calc(${th('gridUnit')} * 2);
  height: calc(100vh - ${th('gridUnit')} * 14);
  margin-bottom: calc(${th('gridUnit')} * 4);
  overflow-y: scroll;
`
// #endregion
