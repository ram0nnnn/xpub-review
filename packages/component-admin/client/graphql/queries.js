import gql from 'graphql-tag'
import { userFragment } from './fragments'

export const getUsersForAdminPanel = gql`
  query {
    getUsersForAdminPanel {
      ...userDetails
    }
  }
  ${userFragment}
`
export const currentUser = gql`
  query {
    currentUser {
      id
      role
    }
  }
`
