import { graphql } from 'react-apollo'
import { compose, withProps } from 'recompose'

import * as queries from './queries'
import * as mutations from './mutations'

export default compose(
  graphql(queries.getUsersForAdminPanel),
  graphql(mutations.addUserFromAdminPanel, {
    name: 'addUserFromAdminPanel',
    options: {
      refetchQueries: [{ query: queries.getUsersForAdminPanel }],
    },
  }),
  graphql(mutations.editUserFromAdminPanel, {
    name: 'editUserFromAdminPanel',
    options: {
      refetchQueries: [{ query: queries.getUsersForAdminPanel }],
    },
  }),
  graphql(mutations.activateUser, {
    name: 'activateUser',
    options: {
      refetchQueries: [{ query: queries.getUsersForAdminPanel }],
    },
  }),
  graphql(mutations.deactivateUser, {
    name: 'deactivateUser',
    options: {
      refetchQueries: [{ query: queries.getUsersForAdminPanel }],
    },
  }),
  withProps(({ data }) => ({
    users: data.users,
  })),
)
