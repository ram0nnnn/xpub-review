const { NotFoundError } = require('@pubsweet/errors')
const { orderBy } = require('lodash')
const { filter } = require('lodash')
const bcrypt = require('bcrypt')

const { User } = require('../fixtures/users')
const { Identity } = require('../fixtures/identities')
const { Team } = require('../fixtures/teams')
const { Manuscript } = require('../fixtures/manuscripts')
const { File } = require('../fixtures/files')
const { Review } = require('../fixtures/reviews')
const { Comment } = require('../fixtures/comments')
const { AuditLog } = require('../fixtures/audit-logs')
const { ReviewerSuggestion } = require('../fixtures/reviewer-suggestions')
const { TeamMember } = require('../fixtures/team-members')

const build = fixtures => {
  const models = {
    User: {
      find: id => findMock(id, 'users', fixtures),
    },
    Identity: {},
    Team: {},
    Manuscript: {},
    File: {},
    Review: {},
    Comment: {},
    AuditLog: {},
    TeamMember: {},
    Journal: {
      findOneByField: (field, value) =>
        findOneByFieldMock(field, value, 'journals', fixtures),
      all: () => allMock('journals', fixtures),
      find: id => findMock(id, 'journals', fixtures),
    },
  }

  User.find = id => findMock(id, 'users', fixtures)
  User.findOneByField = (field, value) =>
    findOneByFieldMock(field, value, 'users', fixtures)
  User.findOneBy = values => findOneByMock(values, 'users', fixtures)
  User.all = () => allMock('users', fixtures)

  Identity.findOneByField = (field, value) =>
    findOneByFieldMock(field, value, 'identities', fixtures)
  Identity.findOneBy = values => findOneByMock(values, 'identities', fixtures)
  Identity.hashPassword = password => bcrypt.hash(password, 1)

  Team.findOneByField = (field, value) =>
    findOneByFieldMock(field, value, 'teams', fixtures)
  Team.findOneBy = values => findOneByMock(values, 'teams', fixtures)
  Team.find = id => findMock(id, 'teams', fixtures)
  Team.findIn = (field, options) =>
    findInMock(field, options, 'teams', fixtures)

  Manuscript.find = (id, related) => findMock(id, 'manuscripts', fixtures)
  Manuscript.findBy = values => findByMock(values, 'manuscripts', fixtures)
  Manuscript.all = () => allMock('manuscripts', fixtures)
  Manuscript.findByField = (field, value) =>
    findByFieldMock(field, value, 'manuscripts', fixtures)
  Manuscript.findAllOrderedBy = ({ orderByField, order }) =>
    findAllOrderedByMock('manuscripts', fixtures, orderByField, order)
  Manuscript.findManuscriptsBySubmissionId = ({
    submissionId,
    excludedStatus,
    orderByField,
    order,
  }) =>
    findManuscriptsBySubmissionIdMock(
      'manuscripts',
      fixtures,
      submissionId,
      excludedStatus,
      orderByField,
      order,
    )
  Manuscript.findOneByField = (field, value) =>
    findOneByFieldMock(field, value, 'manuscripts', fixtures)
  Manuscript.findOneBy = values =>
    findOneByMock(values, 'manuscripts', fixtures)
  Manuscript.findOrderedBy = ({ orderByField, order, queryObject }) =>
    findOrderedByMock('manuscripts', fixtures, orderByField, order, queryObject)
  File.find = id => findMock(id, 'files', fixtures)

  AuditLog.findByField = (field, value) =>
    findByFieldMock(field, value, 'logs', fixtures)
  AuditLog.findBy = values => findByMock(values, 'logs', fixtures)
  Review.find = id => findMock(id, 'reviews', fixtures)

  ReviewerSuggestion.findOneByField = (field, value) =>
    findOneByFieldMock(field, value, 'reviewerSuggestions', fixtures)
  ReviewerSuggestion.findOneBy = values =>
    findOneByMock(values, 'reviewerSuggestions', fixtures)

  TeamMember.findOneByField = (field, value) =>
    findOneByFieldMock(field, value, 'teamMembers', fixtures)
  TeamMember.findByField = (field, value) =>
    findByFieldMock(field, value, 'teamMembers', fixtures)
  TeamMember.findBy = values => findByMock(values, 'teamMembers', fixtures)
  TeamMember.find = id => findMock(id, 'teamMembers', fixtures)
  TeamMember.findOneBy = values => findOneByMock(values, 'teams', fixtures)

  models.User = User
  models.Identity = Identity
  models.Team = Team
  models.Manuscript = Manuscript
  models.File = File
  models.Review = Review
  models.Comment = Comment
  models.AuditLog = AuditLog
  models.ReviewerSuggestion = ReviewerSuggestion
  models.TeamMember = TeamMember

  return models
}

const findMock = async (id, type, fixtures) => {
  const foundObj = Object.values(fixtures[type]).find(
    fixtureObj => fixtureObj.id === id,
  )
  if (!foundObj)
    throw new NotFoundError(`Object not found: ${type} with 'id' ${id}`)

  return Promise.resolve(foundObj)
}

const allMock = (type, fixtures) => Object.values(fixtures[type])

const findAllOrderedByMock = (type, fixtures, orderByField, order) =>
  orderBy(Object.values(fixtures[type]), orderByField, order)

const findManuscriptsBySubmissionIdMock = (
  type,
  fixtures,
  submissionId,
  excludedStatus,
  orderByField,
  order,
) => {
  const foundObjs = Object.values(fixtures[type]).filter(
    fixtureObj => fixtureObj.submissionId === submissionId,
  )
  const filteredData = foundObjs.filter(obj => obj.status !== excludedStatus)
  return orderBy(filteredData, orderByField, order)
}

const findOrderedByMock = (type, fixtures, orderByField, order, queryProps) => {
  const filteredData = filter(fixtures[type], queryProps)
  return orderBy(filteredData, orderByField, order)
}

const findOneByMock = (values, type, fixtures) => {
  delete values.eagerLoadRelations
  return filter(fixtures[type], values.queryObject)[0]
}

const findOneByFieldMock = (field, value, type, fixtures) => {
  const foundObj = Object.values(fixtures[type]).find(
    fixtureObj => fixtureObj[field] === value,
  )

  return Promise.resolve(foundObj)
}

const findByFieldMock = (field, value, type, fixtures) => {
  const foundObjs = Object.values(fixtures[type]).filter(
    fixtureObj => fixtureObj[field] === value,
  )

  return Promise.resolve(foundObjs)
}

const findByMock = (values, type, fixtures) => {
  const foundObjs = filter(Object.values(fixtures[type]), values)
  return Promise.resolve(foundObjs)
}

const findInMock = (field, options, type, fixtures) => {
  const foundObjs = Object.values(fixtures[type]).filter(fixtureObj =>
    options.includes(fixtureObj[field]),
  )

  return Promise.resolve(foundObjs)
}

module.exports = { build }
