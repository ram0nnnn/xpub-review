const Chance = require('chance')

const chance = new Chance()

module.exports = {
  async createUserOnManuscript({ manuscript, fixtures, input = {}, role }) {
    let team = fixtures.getTeamByRoleAndManuscriptId({
      id: manuscript.id,
      role,
    })
    if (!team) {
      team = fixtures.generateTeam({
        role,
        manuscriptId: manuscript.id,
      })
      manuscript.teams.push(team)
    }

    const user = fixtures.generateUser({})

    const teamMember = fixtures.generateTeamMember({
      userId: user.id,
      teamId: team.id,
      ...input,
    })

    await setRelationsToManuscript({ teamMember, user, team, manuscript })

    return teamMember
  },
  async createUserOnJournal({ journal, fixtures, input = {}, role }) {
    let team = fixtures.getTeamByJournalId(journal.id)
    if (!team) {
      team = fixtures.generateTeam({
        role,
        journalId: journal.id,
      })
      journal.teams.push(team)
    }

    const user = fixtures.generateUser({})

    const teamMember = fixtures.generateTeamMember({
      userId: user.id,
      teamId: team.id,
      ...input,
    })

    await setRelationsToJournal({ teamMember, user, journal, team })

    return teamMember
  },
  async addUserOnManuscript({ manuscript, fixtures, input = {}, role, user }) {
    let team = fixtures.getTeamByRoleAndManuscriptId({
      id: manuscript.id,
      role,
    })
    if (!team) {
      team = fixtures.generateTeam({
        role,
        manuscriptId: manuscript.id,
      })
      manuscript.teams.push(team)
    }

    const teamMember = fixtures.generateTeamMember({
      userId: user.id,
      teamId: team.id,
      ...input,
    })

    await setRelationsToManuscript({ teamMember, user, team, manuscript })

    return teamMember
  },
  async createReviewOnManuscript({
    manuscript,
    fixtures,
    recommendation,
    teamMember,
  }) {
    const review = fixtures.generateReview({
      manuscriptId: manuscript.id,
      recommendation,
      teamMemberId: teamMember.id,
    })
    review.manuscript = manuscript
    review.member = teamMember
    manuscript.reviews.push(review)

    const comment = fixtures.generateComment({
      reviewId: review.id,
      content: chance.sentence(),
      type: 'public',
    })
    comment.review = review
    review.comments.push(comment)

    return review
  },
}

const setRelationsToManuscript = async ({
  user,
  team,
  teamMember,
  manuscript,
}) => {
  await createTeamMemberRelations({ teamMember, user, team })
  team.manuscript = manuscript
}

const setRelationsToJournal = async ({ user, team, teamMember, journal }) => {
  await createTeamMemberRelations({ teamMember, user, team })
  team.journal = journal
}

const createTeamMemberRelations = async ({ teamMember, user, team }) => {
  teamMember.user = user
  teamMember.team = team
  await teamMember.linkUser(user)
  team.members.push(teamMember)
  user.teamMemberships.push(teamMember)
}
