const Chance = require('chance')
const { assign, isEmpty } = require('lodash')

const chance = new Chance()
const { Comment } = require('./comments')

const reviews = []

function Review(props) {
  return {
    id: chance.guid(),
    manuscriptId: props.manuscriptId,
    teamMemberId: props.teamMemberId,
    submitted: props.submitted || new Date().toISOString(),
    comments: props.comments || [],
    recommendation: props.recommendation || [],
    async save() {
      const existingReview = reviews.find(m => m.id === this.id)
      if (existingReview) {
        assign(existingReview, this)
      } else {
        reviews.push(this)
      }
      return Promise.resolve(this)
    },
    async saveRecursively() {
      await this.save()
      if (this.comments.length > 0) {
        await Promise.all(this.comments.map(async comment => comment.save()))
      }
    },
    updateProperties(properties) {
      assign(this, properties)
      return this
    },
    hasPublicComment() {
      if (!this.comments) {
        throw new Error('Comments are required.')
      }

      const publicComment = this.comments.find(comm => comm.type === 'public')

      if (!publicComment) return false

      if (!publicComment.files) {
        throw new Error('Files are required.')
      }

      return !!publicComment.content || publicComment.files.length > 0
    },
    stripEmptyPrivateComments() {
      if (!this.comments) {
        throw new Error('Comments are required.')
      }

      const privateComment = this.comments.find(comm => comm.type === 'private')
      if (privateComment && isEmpty(privateComment.content)) {
        this.comments = this.comments.filter(comm => comm.type !== 'private')
      }
    },
    assignMember(teamMember) {
      this.member = teamMember
    },
    toDTO() {
      return {
        ...this,
        comments: this.comments
          ? this.comments.map(comment => comment.toDTO())
          : [],
        member: this.member ? this.member.toDTO() : undefined,
      }
    },
    addComment({ content, type }) {
      this.comments = this.comments || []

      const commentTypeAlreadyExists = this.comments.some(c => c.type === type)
      if (commentTypeAlreadyExists)
        throw new Error('Cannot add multiple comments of the same type')

      const comment = new Comment({
        content,
        type,
      })

      this.comments.push(comment)

      return comment
    },
  }
}

const generateReview = properties => {
  const review = new Review(properties || {})
  reviews.push(review)

  return review
}

Review.Recommendations = {
  responseToRevision: 'responseToRevision',
  minor: 'minor',
  major: 'major',
  reject: 'reject',
  publish: 'publish',
  revision: 'revision',
}

module.exports = { Review, reviews, generateReview }
