const Chance = require('chance')
const config = require('config')
const { Team } = require('./teams')

const chance = new Chance()
function Journal() {
  return {
    id: chance.guid(),
    title: config.get('journal.name'),
    teams: [],
    toDTO() {
      const eicTeam = this.teams.find(t => t.role === Team.Role.editorInChief)
      const eic = eicTeam.members[0]
      return {
        ...this,
        editorInChief: eic ? eic.toDTO() : undefined,
      }
    },
    getEditorInChief() {
      const eicTeam = this.teams.find(t => t.role === Team.Role.editorInChief)
      if (!eicTeam) {
        throw new Error('Could not find Editor in Chief team')
      }
      const eic = eicTeam.members[0]
      return eic
    },
  }
}

module.exports = { journals: [new Journal()] }
