const Chance = require('chance')
const { TeamMember } = require('./team-members')
const { assign, remove } = require('lodash')

const chance = new Chance()
const teams = []

function Team(props) {
  return {
    id: chance.guid(),
    role: props.role || null,
    manuscriptId: props.manuscriptId || null,
    journalId: props.manuscriptId || null,
    members: props.members || [],
    async save() {
      if (!this.id) {
        this.id = chance.guid()
      }
      const existingTeam = teams.find(t => t.id === this.id)
      if (existingTeam) {
        assign(existingTeam, this)
      } else {
        teams.push(this)
      }
      return Promise.resolve(this)
    },
    addMember(user, invitationOptions) {
      this.members = this.members || []
      const memberAlreadyExists = this.members.some(
        member => member.userId === user.id,
      )
      if (memberAlreadyExists) {
        const defaultIdentity = user.getDefaultIdentity()

        throw new Error(
          `User ${defaultIdentity.email} is already invited in the ${
            this.role
          } team`,
        )
      }

      const newMember = new TeamMember({
        ...invitationOptions,
        userId: user.id,
        teamId: this.id,
        position: this.members.length,
      })
      newMember.linkUser(user)
      newMember.team = this

      this.members.push(newMember)

      return newMember
    },
    removeMember(memberId) {
      this.members = this.members || []
      const member = this.members.find(member => member.id === memberId)

      if (!member) {
        throw new Error(`User does not exist in the ${this.role} team`)
      }
      if (member.isSubmitting) {
        throw new Error(`Submitting authors can't be deleted from the team`)
      }

      this.members = this.members.filter(member => member.id !== memberId)
    },
    async delete() {
      remove(teams, f => f.id === this.id)
    },
    saveRecursively() {
      this.save()
      if (this.members.length > 0) {
        this.members.map(member => member.saveRecursively())
      }
    },
  }
}
Team.Role = {
  author: 'author',
  admin: 'admin',
  editorInChief: 'editorInChief',
  reviewer: 'reviewer',
  handlingEditor: 'handlingEditor',
}

Team.GlobalRoles = [
  Team.Role.admin,
  Team.Role.editorInChief,
  Team.Role.handlingEditor,
]

const generateTeam = properties => {
  const team = new Team(properties || {})

  teams.push(team)

  return team
}

const getTeamByRole = role => teams.find(team => team.role === role)
const getTeamByManuscriptId = id => teams.find(team => team.manuscriptId === id)
const getTeamByJournalId = id => teams.find(team => team.journalId === id)
const getTeamByRoleAndManuscriptId = ({ id, role }) =>
  teams.find(team => team.role === role && team.manuscriptId === id)

module.exports = {
  Team,
  teams,
  generateTeam,
  getTeamByRole,
  getTeamByManuscriptId,
  getTeamByJournalId,
  getTeamByRoleAndManuscriptId,
}
