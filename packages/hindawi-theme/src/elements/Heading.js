import { get } from 'lodash'
import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { marginHelper, paddingHelper } from 'component-hindawi-ui'

const heading = () => css`
  align-items: ${props => get(props, 'alignItems', 'center')};
  justify-content: ${props => get(props, 'justify', 'center')};
  display: flex;
`

export default {
  H1: css`
    color: ${th('heading.h1Color')};
    font-family: ${th('defaultFont')};
    font-weight: ${th('fontWeightSemibold')};

    ${heading};
    ${marginHelper};
    ${paddingHelper};
  `,
  H2: css`
    color: ${th('heading.h2Color')};
    font-family: ${th('defaultFont')};
    font-weight: ${th('fontWeightSemibold')};

    ${heading};
    ${marginHelper};
    ${paddingHelper};
  `,
  H3: css`
    color: ${th('heading.h3Color')};
    font-family: ${th('defaultFont')};
    font-weight: ${th('fontWeightSemibold')};

    ${heading};
    ${marginHelper};
    ${paddingHelper};
  `,
  H4: css`
    color: ${th('heading.h4Color')};
    font-family: ${th('defaultFont')};
    font-weight: ${th('fontWeightSemibold')};

    ${heading};
    ${marginHelper};
    ${paddingHelper};
  `,
}
