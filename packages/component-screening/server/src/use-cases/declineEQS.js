const initialize = ({ models: { Manuscript }, logEvent }) => ({
  async execute({ manuscriptId, input, reqUserId }) {
    const { token } = input
    const manuscript = await Manuscript.find(manuscriptId)

    if (manuscript.hasPassedEqs !== null) {
      throw new Error('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    manuscript.updateProperties({
      hasPassedEqs: false,
      technicalCheckToken: null,
      status: 'rejected',
    })
    await manuscript.save()

    logEvent({
      userId: null,
      manuscriptId,
      action: logEvent.actions.eqs_declined,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
