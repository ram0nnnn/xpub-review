const Chance = require('chance')

const { models, fixtures } = require('fixture-service')
const { approveEQSUseCase } = require('../src/use-cases')

const chance = new Chance()
const logEvent = () => jest.fn(async () => {})
logEvent.actions = { eqs_approved: 'eqs_approved' }
logEvent.objectType = { manuscript: 'manuscript' }
describe('Approve EQS use case', () => {
  it('sets a custom ID on the manuscript and hasEQS to true', async () => {
    const mockedModels = models.build(fixtures)
    const token = chance.guid()
    const manuscript = fixtures.generateManuscript({
      technicalCheckToken: token,
      status: 'technicalChecks',
      hasPassedEqs: null,
    })

    await approveEQSUseCase
      .initialize({ models: mockedModels, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          customId: chance.natural({ min: '1000000', max: '9999999' }),
          token,
        },
      })

    expect(manuscript.customId).not.toBeNull()
    expect(manuscript.hasPassedEqs).toBeTruthy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual('submitted')
  })

  it('returns an error when the token is invalid', async () => {
    const mockedModels = models.build(fixtures)

    const manuscript = fixtures.generateManuscript({
      technicalCheckToken: chance.guid(),
      hasPassedEqs: null,
    })

    const result = approveEQSUseCase
      .initialize({ models: mockedModels, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          customId: chance.natural({ min: '1000000', max: '9999999' }),
          token: chance.guid(),
        },
      })

    expect(result).rejects.toThrow('Invalid token.')
  })

  it('returns an error if the manuscript has already passed/failed EQS', async () => {
    const mockedModels = models.build(fixtures)
    const token = chance.guid()
    const manuscript = fixtures.generateManuscript({
      technicalCheckToken: null,
      hasPassedEqs: chance.pickone([false, true]),
    })

    const result = approveEQSUseCase
      .initialize({ models: mockedModels, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          customId: chance.natural({ min: '1000000', max: '9999999' }),
          token,
        },
      })

    expect(result).rejects.toThrow('Manuscript already handled.')
  })

  it('returns an error if the provided custom ID already exists', async () => {
    const token = chance.guid()
    const customId = chance.natural({ min: '1000000', max: '9999999' })
    const mockedModels = models.build(fixtures)
    fixtures.generateManuscript({
      technicalCheckToken: null,
      hasPassedEqs: chance.pickone([false, true]),
      customId,
    })
    const manuscript = fixtures.generateManuscript({
      technicalCheckToken: token,
      hasPassedEqs: null,
    })

    const result = approveEQSUseCase
      .initialize({ models: mockedModels, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          customId,
          token,
        },
      })

    expect(result).rejects.toThrow('The provided ID already exists.')
  })
})
