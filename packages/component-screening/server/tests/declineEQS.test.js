const Chance = require('chance')

const { models, fixtures } = require('fixture-service')
const { declineEQSUseCase } = require('../src/use-cases')

const chance = new Chance()
const logEvent = () => jest.fn(async () => {})
logEvent.actions = { eeqs_declined: 'eqs_declined' }
logEvent.objectType = { manuscript: 'manuscript' }
describe('Decline EQS use case', () => {
  it('sets a custom ID on the manuscript and hasEQS to true', async () => {
    const mockedModels = models.build(fixtures)
    const token = chance.guid()
    const manuscript = fixtures.generateManuscript({
      technicalCheckToken: token,
      hasPassedEqs: null,
    })

    await declineEQSUseCase
      .initialize({ models: mockedModels, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token,
        },
      })

    expect(manuscript.customId).toBeNull()
    expect(manuscript.hasPassedEqs).toBeFalsy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual('rejected')
  })

  it('returns an error when the token is invalid', async () => {
    const mockedModels = models.build(fixtures)

    const manuscript = fixtures.generateManuscript({
      technicalCheckToken: chance.guid(),
      hasPassedEqs: null,
    })

    const result = declineEQSUseCase
      .initialize({ models: mockedModels, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token: chance.guid(),
        },
      })

    expect(result).rejects.toThrow('Invalid token.')
  })

  it('returns an error if the manuscript has already passed/failed EQS', async () => {
    const mockedModels = models.build(fixtures)
    const token = chance.guid()
    const manuscript = fixtures.generateManuscript({
      technicalCheckToken: token,
      hasPassedEqs: chance.pickone([false, true]),
    })

    const result = declineEQSUseCase
      .initialize({ models: mockedModels, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token,
        },
      })

    expect(result).rejects.toThrow('Manuscript already handled.')
  })
})
