process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Promise = require('bluebird')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { approveEQAUseCase } = require('../src/use-cases')

const notification = {
  notifyHEWhenEQAApprovesManuscript: jest.fn(),
  notifyAuthorsWhenEQAApprovesManuscript: jest.fn(),
  notifyReviewersWhenEQAApprovesManuscript: jest.fn(),
}
const chance = new Chance()

describe('Approve EQA use case', () => {
  let journal
  let manuscript
  let token
  let admin
  let mockedModels

  beforeEach(async () => {
    // eslint-disable-next-line prefer-destructuring
    journal = fixtures.journals[0]
    token = chance.guid()
    admin = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'admin',
    })
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })
    manuscript = fixtures.generateManuscript({
      journal,
      technicalCheckToken: token,
      status: 'inQA',
      hasPassedEqa: null,
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'submitted' },
      role: 'reviewer',
    })
    mockedModels = models.build(fixtures)
  })
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('changes manucript fields', async () => {
    await approveEQAUseCase
      .initialize({ notification, models: mockedModels })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
        userId: admin.userId,
      })

    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.hasPassedEqa).toBe(true)
    expect(manuscript.status).toBe(mockedModels.Manuscript.Statuses.accepted)
  })
  it('should send email to HE, authors and reviewers', async () => {
    await approveEQAUseCase
      .initialize({ notification, models: mockedModels })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
        userId: admin.userId,
      })

    expect(
      notification.notifyHEWhenEQAApprovesManuscript,
    ).toHaveBeenCalledTimes(1)
    expect(
      notification.notifyAuthorsWhenEQAApprovesManuscript,
    ).toHaveBeenCalledTimes(1)
    expect(
      notification.notifyReviewersWhenEQAApprovesManuscript,
    ).toHaveBeenCalledTimes(1)
  })
  it('should throw error if the manuscript already passed EQA', async () => {
    manuscript.hasPassedEqa = true
    const result = approveEQAUseCase
      .initialize({ notification, models: mockedModels })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
        userId: admin.userId,
      })
    expect(result).rejects.toThrow('Manuscript already handled.')
  })
  it('should throw error if the token is invalid', async () => {
    token = chance.guid()
    const result = approveEQAUseCase
      .initialize({ notification, models: mockedModels })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
        userId: admin.userId,
      })
    expect(result).rejects.toThrow('Invalid token.')
  })
  it('should throw error if the manuscript is in a wrong status', async () => {
    const statuses = mockedModels.Manuscript.Statuses
    const wrongStatuses = [
      statuses.draft,
      statuses.technicalChecks,
      statuses.submitted,
      statuses.heInvited,
      statuses.heAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
      statuses.reviewCompleted,
      statuses.revisionRequested,
      statuses.pendingApproval,
      statuses.accepted,
      statuses.rejected,
      statuses.deleted,
      statuses.published,
      statuses.withdrawn,
      statuses.withdrawalRequested,
      statuses.olderVersion,
    ]
    await Promise.each(wrongStatuses, async wrongStatus => {
      const manuscript = await fixtures.generateManuscript({
        status: wrongStatus,
        journal,
        technicalCheckToken: token,
        hasPassedEqa: null,
      })
      dataService.createUserOnManuscript({
        manuscript,
        fixtures,
        input: { isSubmitting: true, isCorresponding: false },
        role: 'author',
      })
      await dataService.createUserOnManuscript({
        manuscript,
        fixtures,
        input: { status: 'submitted' },
        role: 'reviewer',
      })
      try {
        await approveEQAUseCase
          .initialize({ notification, models: mockedModels })
          .execute({
            manuscriptId: manuscript.id,
            input: { token },
            userId: admin.userId,
          })
        throw new Error(`Use-case should fail for status ${wrongStatus}`)
      } catch (err) {
        expect(err.message).toEqual(`Cannot approve EQA in the current status.`)
      }
    })
  })
})
