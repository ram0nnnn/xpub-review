import React from 'react'

import { TextField } from '@pubsweet/ui'
import {
  Row,
  Item,
  Label,
  FormModal,
  ValidatedFormField,
} from 'component-hindawi-ui'
import { required } from 'xpub-validators'
import { validators } from 'component-hindawi-ui/src/helpers'

const lengthValidator = validators.exactLength(7)

const AcceptEQSModal = ({ handleApproveEQS, hideModal }) => (
  <FormModal
    cancelText="CANCEL"
    confirmText="OK"
    content={() => (
      <Row mt={3}>
        <Item vertical>
          <Label required>Manuscript ID</Label>
          <ValidatedFormField
            component={TextField}
            data-test-id="customId"
            inline
            name="customId"
            validate={[required, validators.digitValidator, lengthValidator]}
          />
        </Item>
      </Row>
    )}
    hideModal={hideModal}
    onSubmit={handleApproveEQS}
    title="Accept manuscript"
  />
)

export default AcceptEQSModal
