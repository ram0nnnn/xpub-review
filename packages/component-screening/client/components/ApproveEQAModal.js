import React from 'react'

import { MultiAction } from 'component-hindawi-ui'

const ApproveEQAModal = ({ handleApproveEQA, hideModal }) => (
  <MultiAction
    cancelText="CANCEL"
    confirmText="OK"
    hideModal={hideModal}
    onConfirm={handleApproveEQA}
    subtitle="Are you sure you want to accept this manuscript?"
    title="Accept manuscript"
  />
)

export default ApproveEQAModal
