import React from 'react'

import { MultiAction } from 'component-hindawi-ui'

const DeclineEQSModal = ({ handleDeclineEQS, hideModal }) => (
  <MultiAction
    cancelText="CANCEL"
    confirmText="OK"
    hideModal={hideModal}
    onConfirm={handleDeclineEQS}
    subtitle="Are you sure you want to reject this manuscript?"
    title="Reject manuscript"
  />
)

export default DeclineEQSModal
