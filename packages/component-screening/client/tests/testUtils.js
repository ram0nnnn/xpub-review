import React from 'react'
import theme from 'hindawi-theme'
import { render as rtlRender } from 'react-testing-library'
import { ThemeProvider } from 'styled-components'

export const render = ui => {
  const Component = () => <ThemeProvider theme={theme}>{ui}</ThemeProvider>

  return rtlRender(<Component />)
}
