import React from 'react'

import { H2, Button } from '@pubsweet/ui'
import { Text, Row, ShadowedBox } from 'component-hindawi-ui'
import { Modal } from 'component-modal'
import { compose, withHandlers, withProps } from 'recompose'

import { ReturnManuscriptToEiCModal, ApproveEQAModal } from '../components'
import withGQL from '../graphql'
import { parseSearchParams } from '../utils'

const EQADecision = ({ handleReturnToEiC, handleApproveEQA }) => (
  <ShadowedBox center mt={3} width={65}>
    <H2 mt={3}>Editorial decision</H2>

    <Row justify="center" mt={1}>
      <Text secondary>Take a decision for manuscript.</Text>
    </Row>

    <Row justify="space-around" mb={3} mt={3}>
      <Modal
        component={ReturnManuscriptToEiCModal}
        handleReturnToEiC={handleReturnToEiC}
        modalKey="returnToEiC"
      >
        {showModal => (
          <Button
            data-test-id="reject-button"
            mr={1}
            onClick={showModal}
            secondary
            width={24}
          >
            Return to eic
          </Button>
        )}
      </Modal>
      <Modal
        component={ApproveEQAModal}
        handleApproveEQA={handleApproveEQA}
        modalKey="approveEQA"
      >
        {showModal => (
          <Button
            data-test-id="accept-button"
            onClick={showModal}
            primary
            width={24}
          >
            Accept
          </Button>
        )}
      </Modal>
    </Row>
  </ShadowedBox>
)

export default compose(
  withGQL,
  withProps(({ history }) => {
    const { token, manuscriptId } = parseSearchParams(history.location.search)
    return { token, manuscriptId }
  }),
  withHandlers({
    handleReturnToEiC: ({ returnToEiC, token, manuscriptId, history }) => (
      { reason },
      { setFetching, setError, hideModal },
    ) => {
      setFetching(true)
      returnToEiC({
        variables: {
          manuscriptId,
          input: {
            token,
            reason,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
          history.push('/info-page', {
            content: 'Manuscript decision submitted. Thank you!',
            title: 'Editorial decision',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    handleApproveEQA: ({ approveEQA, token, manuscriptId, history }) => ({
      setFetching,
      setError,
      hideModal,
    }) => {
      setFetching(true)
      approveEQA({
        variables: {
          manuscriptId,
          input: {
            token,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
          history.push('/info-page', {
            content: 'Manuscript decision submitted. Thank you!',
            title: 'Editorial decision',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(EQADecision)
