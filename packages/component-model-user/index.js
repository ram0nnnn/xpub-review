const User = require('./src/entities/user')
const resolvers = require('./src/resolvers')
const fs = require('fs')
const path = require('path')

module.exports = {
  model: User,
  modelName: 'User',
  resolvers,
  typeDefs: fs.readFileSync(
    path.join(__dirname, '/src/typeDefs.graphqls'),
    'utf8',
  ),
}
