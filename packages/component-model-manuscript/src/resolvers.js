const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')
const { requestReviewers } = require('component-reviewer-suggestions')

const useCases = require('./use-cases')

const resolvers = {
  Query: {
    async getManuscript(_, { manuscriptId }, ctx) {
      return useCases.getManuscriptUseCase
        .initialize({ models })
        .execute({ manuscriptId, userId: ctx.user })
    },
    async getManuscriptVersions(_, { submissionId }, ctx) {
      return useCases.getManuscriptVersionsUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
    async getDraftRevision(_, { submissionId }, ctx) {
      return useCases.getDraftRevisionUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
  },
  Manuscript: {
    async teams(parent, query, ctx) {
      return useCases.getManuscriptTeamsUseCase
        .initialize({ models, useCases })
        .execute(parent)
    },
    async authors(parent, query, ctx) {
      return useCases.getManuscriptAuthorsUseCase
        .initialize(models)
        .execute(parent)
    },
    async reviews(parent, query, ctx) {
      return useCases.getManuscriptReviewsUseCase
        .initialize(models)
        .execute({ manuscript: parent, userId: ctx.user })
    },
    async reviewers(parent, query, ctx) {
      return useCases.getManuscriptReviewersUseCase
        .initialize(models)
        .execute({ manuscript: parent, userId: ctx.user })
    },
    async handlingEditor(parent, query, ctx) {
      return useCases.getManuscriptHandlingEditorUseCase
        .initialize(models)
        .execute(parent)
    },
    async reviewerSuggestions(parent, query, ctx) {
      return useCases.getManuscriptReviewerSuggestionsUseCase
        .initialize({ models, requestReviewers })
        .execute(parent)
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
