const config = require('config')
const { isEmpty } = require('lodash')

const PROVIDERS = config.get('reviewerSuggestionsProviders')

const initialize = ({
  models: { Team, Manuscript, ReviewerSuggestion },
  requestReviewers,
}) => ({
  async execute(manuscript) {
    const wrongStatuses = [
      Manuscript.Statuses.draft,
      Manuscript.Statuses.technicalChecks,
    ]
    if (wrongStatuses.includes(manuscript.status)) return []
    if (manuscript.reviewerSuggestions.length > 0)
      return manuscript.reviewerSuggestions
    const reviewerSuggestions = await ReviewerSuggestion.findBy({
      manuscriptId: manuscript.id,
    })
    if (!isEmpty(reviewerSuggestions)) return reviewerSuggestions
    return requestReviewers(manuscript, PROVIDERS.publons)
  },
})

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
