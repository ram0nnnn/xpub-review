const initialize = ({ Team, TeamMember }) => {
  const filterStrategies = {
    author(handlingEditor) {
      if (TeamMember.Statuses.declined === handlingEditor.status) {
        return
      }

      return handlingEditor
    },
  }

  function parseByRole(object, role, filterOptions) {
    return filterStrategies[role]
      ? filterStrategies[role](object, filterOptions)
      : object
  }

  return {
    async execute(manuscript) {
      let handlingEditorDTO = manuscript.handlingEditor

      if (!handlingEditorDTO) {
        const team = await Team.findOneBy({
          queryObject: {
            role: Team.Role.handlingEditor,
            manuscriptId: manuscript.id,
          },
          eagerLoadRelations: ['members'],
        })

        if (!team) return

        const handlingEditorTeamMember = team.members[0]
        if (!handlingEditorTeamMember) return
        handlingEditorDTO = handlingEditorTeamMember.toDTO()
      }

      return parseByRole(handlingEditorDTO, manuscript.role)
    },
  }
}

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
