const config = require('config')

const GLOBAL_ROLES = config.get('globalRoles')

const initialize = ({ models: { Manuscript, User, Journal, TeamMember } }) => ({
  execute: async ({ manuscriptId, userId }) => {
    const manuscript = await Manuscript.find(manuscriptId, [
      'files',
      'journal.[teams.[members]]',
      'teams.[members.[user.[identities]]]',
      'reviews.[member, comments.[files]]',
      'reviewerSuggestions',
    ])

    const user = await User.find(userId, 'teamMemberships.team')

    const globalRole = user.getGlobalRole()

    if (GLOBAL_ROLES.includes(globalRole)) {
      manuscript.role = globalRole
    } else {
      const matchingMember = user.teamMemberships.find(
        member => member.team.manuscriptId === manuscript.id,
      )
      manuscript.role = matchingMember.team.role
    }

    if (!manuscript.editorInChief) {
      const editorInChief = manuscript.journal.getEditorInChief()
      manuscript.editorInChief = editorInChief
    }

    return manuscript.toDTO()
  },
})

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
