const { omit } = require('lodash')

module.exports.initialize = ({ Review, Manuscript, Comment, Team }) => {
  const filterStrategies = {
    author(reviews, { manuscriptStatus }) {
      if (
        ![
          Manuscript.Statuses.rejected,
          Manuscript.Statuses.accepted,
          Manuscript.Statuses.pendingApproval,
          Manuscript.Statuses.revisionRequested,
          Manuscript.Statuses.inQA,
          Manuscript.Statuses.olderVersion,
          Manuscript.Statuses.heAssigned,
          Manuscript.Statuses.reviewCompleted,
          Manuscript.Statuses.submitted,
        ].includes(manuscriptStatus)
      ) {
        return []
      }

      return reviews.map(r => {
        r.comments =
          r.comments && r.comments.filter(c => c.type === Comment.Types.public)
        return r
      })
    },
    reviewer(reviews, { manuscriptStatus, userId }) {
      if (
        [
          Manuscript.Statuses.accepted,
          Manuscript.Statuses.rejected,
          Manuscript.Statuses.olderVersion,
        ].includes(manuscriptStatus)
      ) {
        return reviews.map(r => {
          if (
            r.member.userId !== userId &&
            r.member.role === Team.Role.reviewer
          ) {
            r.member = omit(r.member, ['alias'])
            r.comments =
              r.comments &&
              r.comments.filter(c => c.type === Comment.Types.public)
          }
          return r
        })
      }
      return reviews.filter(
        r => r.member.userId === userId || r.member.role !== Team.Role.reviewer,
      )
    },
  }

  function parseByRole(object, role, filterOptions) {
    return filterStrategies[role]
      ? filterStrategies[role](object, filterOptions)
      : object
  }

  return {
    async execute({ manuscript, userId }) {
      const reviews = await Review.findBy({ manuscriptId: manuscript.id }, [
        'comments.files',
        'member.team',
      ])

      const reviewsDTOs = reviews.map(r => r.toDTO())

      return parseByRole(reviewsDTOs, manuscript.role, {
        manuscriptStatus: manuscript.status,
        userId,
      })
    },
  }
}
