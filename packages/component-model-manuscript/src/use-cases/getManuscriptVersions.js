const config = require('config')

const GLOBAL_ROLES = config.get('globalRoles')

const initialize = ({ Journal, Manuscript, User }) => ({
  execute: async ({ submissionId, userId }) => {
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      excludedStatus: Manuscript.Statuses.draft,
      orderByField: 'version',
      order: 'desc',
      eagerLoadRelations: [
        'files',
        'teams.[members.[user.[identities]]]',
        'journal',
      ],
    })
    const manuscriptIds = manuscripts.map(m => m.id)

    const user = await User.find(userId, 'teamMemberships.[team]')
    const globalRole = user.getGlobalRole()

    const userRoleOnManuscripts = user.teamMemberships
      .filter(({ team }) => manuscriptIds.includes(team.manuscriptId))
      .reduce(
        (acc, tm) => ({ ...acc, [tm.team.manuscriptId]: tm.team.role }),
        {},
      )

    const journal = await Journal.find(
      manuscripts[0].journalId,
      'teams.members',
    )
    const editorInChief = await journal.getEditorInChief()

    manuscripts.forEach(m => {
      m.editorInChief = editorInChief
      m.role = GLOBAL_ROLES.includes(globalRole)
        ? globalRole
        : userRoleOnManuscripts[m.id]
    })

    const visibleManuscripts = manuscripts.filter(m => m.role)

    return visibleManuscripts.map(m => m.toDTO())
  },
})

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
