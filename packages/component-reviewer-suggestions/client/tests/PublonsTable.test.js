import React from 'react'
import 'jest-styled-components'
import 'jest-dom/extend-expect'
import { cleanup, fireEvent } from 'react-testing-library'
import PublonsTable from '../components/PublonsTable'
import { render } from './testUtils'

const Chance = require('chance')

const chance = new Chance()

const basicReviewer = () => ({
  id: chance.guid(),
  givenNames: chance.first(),
  surname: chance.first(),
  email: chance.email(),
  profileUrl: chance.url(),
  aff: chance.company(),
  numberOfReviews: chance.natural(),
  type: 'publons',
  isInvited: false,
})

describe('Publons Table ', () => {
  const canInvitePublons = true
  const reviewers = []
  beforeEach(() => {
    reviewers.push(basicReviewer())
    reviewers.push(basicReviewer())
  })
  afterEach(cleanup)

  it('Should render all reviews', () => {
    const { getByTestId } = render(
      <PublonsTable
        canInvitePublons={canInvitePublons}
        reviewers={reviewers}
      />,
    )

    const renderNames = reviewers.map(
      ({ id }) => getByTestId(`name-${id}`).textContent,
    )
    const names = reviewers.map(
      ({ givenNames, surname }) => `${givenNames} ${surname}`,
    )

    const renderAffiliations = reviewers.map(
      ({ id }) => getByTestId(`aff-${id}`).textContent,
    )
    const affiliations = reviewers.map(({ aff }) => aff)

    const renderNumberOfReviews = reviewers.map(
      ({ id }) => getByTestId(`numberOfReviews-${id}`).textContent,
    )
    const numberOfReviews = reviewers.map(({ numberOfReviews }) =>
      numberOfReviews.toString(),
    )

    const renderInviteButtons = reviewers.map(
      ({ id }) => getByTestId(`reviewerPublonsButton-${id}`).textContent,
    )

    expect(renderNames).toHaveLength(2)
    expect(renderNames).toEqual(names)
    expect(renderAffiliations).toHaveLength(2)
    expect(renderAffiliations).toEqual(affiliations)
    expect(renderNumberOfReviews).toHaveLength(2)
    expect(renderNumberOfReviews).toEqual(numberOfReviews)
    expect(renderInviteButtons).toHaveLength(2)
  })

  it('Should open modal after inviting one reviewer', () => {
    const { getByTestId, getByText } = render(
      <PublonsTable
        canInvitePublons={canInvitePublons}
        reviewers={reviewers}
      />,
    )

    expect(
      getByTestId(`reviewerPublonsButton-${reviewers[0].id}`),
    ).toBeInTheDocument()
    fireEvent.click(getByTestId(`reviewerPublonsButton-${reviewers[0].id}`))
    expect(getByText('Send Invitation to Review?')).toBeInTheDocument()
    expect(getByText('SEND')).toBeInTheDocument()
    expect(getByText('BACK')).toBeInTheDocument()
  })

  it('Should be able to click on Send from Modal', () => {
    const onInviteMock = jest.fn()
    const { getByTestId } = render(
      <PublonsTable
        canInvitePublons={canInvitePublons}
        onInvite={onInviteMock}
        reviewers={reviewers}
      />,
    )

    expect(
      getByTestId(`reviewerPublonsButton-${reviewers[0].id}`),
    ).toBeInTheDocument()
    fireEvent.click(getByTestId(`reviewerPublonsButton-${reviewers[0].id}`))
    fireEvent.click(getByTestId('modal-confirm'))
    expect(onInviteMock).toHaveBeenCalledTimes(1)
  })

  it('Should return error if there are no reviewers', () => {
    const { getByTestId } = render(<PublonsTable reviewers={[]} />)
    expect(getByTestId('error-empty-state')).toBeInTheDocument()
    expect(getByTestId('error-empty-state')).toHaveTextContent(
      'There are no reviewer suggestions to display',
    )
  })
})
