import React from 'react'
import theme from 'hindawi-theme'
import { ModalProvider } from 'component-modal'
import { ThemeProvider } from 'styled-components'
import { render as rtlRender } from 'react-testing-library'

export const render = ui => {
  const Component = () => (
    <ModalProvider>
      <div id="ps-modal-root" />
      <ThemeProvider theme={theme}>{ui}</ThemeProvider>
    </ModalProvider>
  )

  return rtlRender(<Component />)
}
