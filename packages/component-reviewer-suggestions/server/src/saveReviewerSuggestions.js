const { get, chain, last, initial } = require('lodash')
const Promise = require('bluebird')
const ReviewerSuggestion = require('component-model-reviewer-suggestion').model

const parseReviewerSuggestions = data =>
  chain(data)
    .filter(rev => rev.profileUrl && get(rev, 'contact.emails.length', 0) > 0)
    .map(rev => ({
      email: process.env.PUBLONS_MOCK_EMAIL
        ? process.env.PUBLONS_MOCK_EMAIL.replace(
            '__NAME__',
            `${rev.contact.emails[0].email.split('@')[0]}`,
          )
        : rev.contact.emails[0].email,
      givenNames: initial(rev.publishingName.split(' ')).join(' '),
      surname: last(rev.publishingName.split(' ')),
      profileUrl: rev.profileUrl,
      numberOfReviews: rev.numVerifiedReviews,
      aff: get(rev, 'recentOrganizations[0].name', 'Non-affiliated'),
      manuscriptId: rev.manuscriptId,
      type: rev.type,
    }))
    .uniqBy('email')
    .value()

const saveReviewerSuggestions = async data => {
  const reviewers = parseReviewerSuggestions(data)
  return Promise.each(reviewers, reviewer => {
    const newReviewer = new ReviewerSuggestion(reviewer)
    return newReviewer.save()
  })
}

module.exports = {
  saveReviewerSuggestions,
  parseReviewerSuggestions,
}
