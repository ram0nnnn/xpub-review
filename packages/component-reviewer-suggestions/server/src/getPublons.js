const config = require('config')
const axios = require('axios')
const { get, chain } = require('lodash')

const publonsKey = config.get('publons.key')
const publonsUrl = config.get('publons.reviewersUrl')

const getPublons = async ({
  id,
  meta: { title, abstract },
  journal,
  teams,
}) => {
  journal = {
    name: journal.title,
  }
  const authors = chain(teams)
    .find(t => t.role === 'author')
    .get('members', [])
    .map(m => ({
      email: get(m, 'alias.email', ''),
      firstName: get(m, 'alias.givenNames', ''),
      lastName: get(m, 'alias.surname', ''),
    }))
    .value()

  let { data } = await axios({
    method: 'post',
    url: publonsUrl,
    headers: {
      'x-apikey': publonsKey,
    },
    data: {
      searchArticle: {
        title,
        abstract,
        journal,
        authors,
      },
    },
  })
  data = data.recommendedReviewers.map(rev => ({
    ...rev,
    type: 'publons',
    manuscriptId: id,
  }))
  return data
}

module.exports = {
  getPublons,
}
