const { getPublons } = require('./getPublons')
const { saveReviewerSuggestions } = require('./saveReviewerSuggestions')

const services = {
  publons: getPublons,
}

const requestReviewers = async (manuscript, provider) => {
  if (!manuscript.journal) throw new Error('Journal is required.')
  if (!manuscript.teams) throw new Error('Teams are required.')
  let reviewers
  try {
    reviewers = await services[provider](manuscript)
  } catch (err) {
    throw new Error(
      `Could not fetch reviewer suggestions. Please check provider integration: ${provider}`,
    )
  }
  return saveReviewerSuggestions(reviewers)
}

module.exports = {
  requestReviewers,
}
