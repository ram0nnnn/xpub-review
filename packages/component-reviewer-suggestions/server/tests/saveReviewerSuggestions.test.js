process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
process.env.PUBLONS_MOCK_EMAIL = 'hindawi+__NAME__@thinslices.com'

const { parseReviewerSuggestions } = require('../src/saveReviewerSuggestions')

const publonsReviewers = [
  {
    contact: {
      emails: [{ email: 'email@email.com' }],
    },
    publishingName: 'John Smith',
    profileUrl: 'url',
    numVerifiedReviews: 16,
    recentOrganizations: [
      {
        name: 'MIT',
      },
    ],
    manuscriptId: '1234567-ddaffsgg-1523',
    type: 'publon',
  },
  {
    contact: {
      emails: [{ email: 'email2@email.com' }],
    },
    publishingName: 'Marcel Iures',
    profileUrl: 'url',
    numVerifiedReviews: 2,
    recentOrganizations: [
      {
        name: 'CalTech',
      },
    ],
    manuscriptId: '1234567-ddaffsgg-1523',
    type: 'publon',
  },
  {
    contact: {
      emails: [{ email: 'email3@email.com' }],
    },
    publishingName: 'Akim Vasilescu',
    profileUrl: null,
    numVerifiedReviews: 34,
    recentOrganizations: [
      {
        name: 'CalTech',
      },
    ],
    manuscriptId: '1234567-ddaffsgg-1523',
    type: 'publon',
  },

  {
    publishingName: 'Cristina Ionescu',
    profileUrl: null,
    numVerifiedReviews: 26,
    recentOrganizations: [
      {
        name: 'CalTech',
      },
    ],
    manuscriptId: '1234567-ddaffsgg-1523',
    type: 'publon',
  },
]

describe('Parse reviewer suggestions', () => {
  it('should return a properly formatted list of reviewer suggestions', () => {
    const reviewers = parseReviewerSuggestions(publonsReviewers)

    expect(reviewers).toHaveLength(2)
    expect(reviewers[0]).toEqual(
      expect.objectContaining({
        aff: expect.any(String),
        numberOfReviews: expect.any(Number),
        givenNames: expect.any(String),
        surname: expect.any(String),
        email: expect.any(String),
        profileUrl: expect.any(String),
        manuscriptId: expect.anything(),
        type: expect.anything(),
      }),
    )
  })
  it('should only return reviewers that have profileUrl and email address', () => {
    const reviewers = parseReviewerSuggestions(publonsReviewers)

    expect(reviewers).toHaveLength(2)
    expect(reviewers[0].givenNames).toEqual('John')
    expect(reviewers[1].givenNames).toEqual('Marcel')
  })
})
