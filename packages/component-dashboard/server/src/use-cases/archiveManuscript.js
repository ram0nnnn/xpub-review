const initialize = ({ Manuscript, User }) => ({
  async execute(submissionId) {
    const manuscripts = await Manuscript.findBy({ submissionId })
    await Promise.all(
      manuscripts.map(async manuscript => {
        if (
          manuscript.status === Manuscript.Statuses.draft &&
          manuscript.version === 1
        ) {
          throw new Error('Draft manuscripts cannot be deleted.')
        }
        if (manuscript.status === Manuscript.Statuses.deleted) {
          throw new Error('Manuscript already deleted.')
        }

        await manuscript.updateStatus(Manuscript.Statuses.deleted)
        await manuscript.save()
      }),
    )
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
