const { orderBy, get } = require('lodash')
const config = require('config')

const statuses = config.get('statuses')
const FILTER_VALUES = {
  ALL: 'all',
  NEEDS_ATTENTION: 'needsAttention',
  IN_PROGRESS: 'inProgress',
  ARCHIVED: 'archived',
}

const initialize = ({ Manuscript, TeamMember }) => ({
  async execute({ input = {}, user }) {
    let manuscripts = []
    const { dateOrder = 'desc', priorityFilter = FILTER_VALUES.ALL } = input

    manuscripts = user.teamMemberships
      .filter(
        member =>
          member.status !== TeamMember.Statuses.declined &&
          member.status !== TeamMember.Statuses.expired,
      )
      .filter(member => member.team.manuscript)
      .map(member => {
        member.team.manuscript.role = member.team.role

        return member.team.manuscript
      })
    manuscripts = manuscripts.filter(
      m => m.status !== Manuscript.Statuses.deleted,
    )
    if (priorityFilter !== FILTER_VALUES.ALL) {
      manuscripts = manuscripts.filter(manuscript =>
        get(
          statuses,
          `${manuscript.status}.${manuscript.role}.${priorityFilter}`,
        ),
      )
    }

    manuscripts = orderBy(manuscripts, 'updated', dateOrder)
    manuscripts = Manuscript.filterOlderVersions(manuscripts)

    return manuscripts.map(m => m.toDTO())
  },
})

module.exports = {
  initialize,
}
