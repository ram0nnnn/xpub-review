const { orderBy, get } = require('lodash')
const config = require('config')

const statuses = config.get('statuses')
const FILTER_VALUES = {
  ALL: 'all',
  NEEDS_ATTENTION: 'needsAttention',
  IN_PROGRESS: 'inProgress',
  ARCHIVED: 'archived',
}

const initialize = ({ Manuscript, Team }) => ({
  async execute({ input = {}, role }) {
    let manuscripts = []
    const { dateOrder = 'desc', priorityFilter = FILTER_VALUES.ALL } = input

    if (priorityFilter === FILTER_VALUES.ALL) {
      manuscripts = await Manuscript.findAllOrderedBy({
        eagerLoadRelations: 'teams.[members.[user.[identities]]]',
        orderByField: 'updated',
        order: dateOrder,
      })
      manuscripts = manuscripts.filter(
        m => m.status !== Manuscript.Statuses.deleted,
      )
      if (role === Team.Role.editorInChief) {
        manuscripts = manuscripts.filter(
          m =>
            m.status !== Manuscript.Statuses.draft &&
            m.status !== Manuscript.Statuses.technicalChecks,
        )
      }
    } else {
      manuscripts = await Manuscript.all()
      manuscripts = manuscripts.filter(
        manuscript =>
          get(statuses, `${manuscript.status}.${role}.${priorityFilter}`) &&
          manuscript.status !== Manuscript.Statuses.deleted,
      )
      manuscripts = orderBy(manuscripts, 'updated', dateOrder)
    }
    manuscripts = manuscripts.map(m => {
      m.role = role
      return m
    })

    manuscripts = Manuscript.filterOlderVersions(manuscripts)

    return manuscripts.map(m => m.toDTO())
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
