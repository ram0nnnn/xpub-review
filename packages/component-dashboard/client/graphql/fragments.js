import gql from 'graphql-tag'

// TODO: move all fragments to a different package
import { fragments } from 'component-submission/client/graphql'

export const manuscriptMeta = gql`
  fragment manuscriptMeta on ManuscriptMeta {
    articleType
    abstract
    agreeTc
    title
  }
`

export const dashboardManuscriptDetails = gql`
  fragment dashboardManuscriptDetails on Manuscript {
    id
    submissionId
    role
    status
    visibleStatus
    customId
    created
    submissionId
    meta {
      ...manuscriptMeta
    }
    handlingEditor {
      ...teamMember
    }
    authors {
      ...teamMember
    }
    reviewers {
      ...teamMember
    }
  }
  ${manuscriptMeta}
  ${fragments.teamMember}
`
