import React, { Fragment } from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { Spinner } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withProps, withHandlers } from 'recompose'
import withGQL from '../graphql/withGQL'
import { DashboardFilters, ManuscriptCard, withFilters } from '../components'

const Dashboard = ({
  history,
  data,
  filters,
  file,
  setFile,
  isConfirmed,
  deleteManuscript,
}) =>
  get(data, 'getManuscripts', []).length === 0 && data.loading ? (
    <Spinner />
  ) : (
    <Fragment>
      <DashboardFilters {...filters} />
      <Root isConfirmed={isConfirmed}>
        {data.getManuscripts.map((m, index) => (
          <ManuscriptCard
            deleteManuscript={deleteManuscript}
            handlingEditor={m.handlingEditor}
            isLast={index === data.getManuscripts.length - 1}
            key={m.id}
            manuscript={m}
            role={m.role}
          />
        ))}
      </Root>
    </Fragment>
  )

export default compose(
  withFilters,
  withGQL,
  withProps(({ currentUser }) => ({
    isConfirmed: get(currentUser, 'identities.0.isConfirmed', true),
  })),
  withHandlers({
    deleteManuscript: ({ deleteManuscript, archiveManuscript }) => (
      manuscript,
      modalProps,
    ) => {
      if (manuscript.status === 'draft') {
        modalProps.setFetching(true)
        deleteManuscript({
          variables: {
            manuscriptId: manuscript.id,
          },
        })
          .then(() => {
            modalProps.setFetching(false)
            modalProps.hideModal()
          })
          .catch(e => {
            modalProps.setFetching(false)
            modalProps.setError(e.message)
          })
      } else if (manuscript.role === 'admin') {
        modalProps.setFetching(true)
        archiveManuscript({
          variables: {
            submissionId: manuscript.submissionId,
          },
        })
          .then(() => {
            modalProps.setFetching(false)
            modalProps.hideModal()
          })
          .catch(e => {
            modalProps.setFetching(false)
            modalProps.setError(e.message)
          })
      }
    },
  }),
)(Dashboard)

// #region styles
const Root = styled.div`
  overflow-y: scroll;
  padding: 0 calc(${th('gridUnit')} * 10);
  /* 100vh from which we subtract the header, the footer and the dashboard filters */
  height: calc(
    100vh - ${th('gridUnit')} * ${props => (props.isConfirmed ? 22 : 25)}
  );
`
// #endregion
