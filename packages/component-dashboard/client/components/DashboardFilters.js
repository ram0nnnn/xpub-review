import React from 'react'
import { Row, Item, Text, Label, Menu } from 'component-hindawi-ui'

const DashboardFilters = ({ values, options, changeSort, changePriority }) => (
  <Row alignItems="flex-end" justify="flex-start" mb={1} mt={2} pl={10} pr={10}>
    <Text mr={1} pb={1} secondary>
      Filters
    </Text>
    <Item
      alignItems="flex-start"
      data-test-id="dashboard-filter-priority"
      flex={0}
      mr={1}
      vertical
    >
      <Label>Priority</Label>
      <Menu
        inline
        onChange={changePriority}
        options={options.priority}
        placeholder="Please select"
        value={values.priority}
        width={14}
      />
    </Item>
    <Item
      alignItems="flex-start"
      data-test-id="dashboard-filter-order"
      flex={0}
      vertical
    >
      <Label>Order</Label>
      <Menu
        inline
        onChange={changeSort}
        options={options.sort}
        placeholder="Please select"
        value={values.sort}
      />
    </Item>
  </Row>
)

export default DashboardFilters
