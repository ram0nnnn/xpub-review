import React, { Fragment } from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { withRouter } from 'react-router'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'
import { H3, H4, DateParser } from '@pubsweet/ui'
import {
  Row,
  Tag,
  Text,
  Icon,
  TextTooltip,
  AuthorTagList,
  ReviewerBreakdown,
  Item,
  ActionLink,
  MultiAction,
  withHEStatus,
} from 'component-hindawi-ui'
import { Modal } from 'component-modal'
import { compose, setDisplayName, withProps, withHandlers } from 'recompose'

const ManuscriptCard = ({
  onClick,
  journalName,
  manuscriptType = {},
  manuscript: {
    meta,
    status,
    created,
    customId,
    visibleStatus,
    id: manuscriptId,
    authors = [],
    reviewers = [],
    role,
  },
  manuscript,
  isLast,
  onDeleteManuscript,
  canSeeReviewerReports,
  handlingEditorVisibleStatus,
}) => {
  const { articleType = '' } = meta
  const title = meta.title || 'No title'
  return (
    <Root
      data-test-id={`manuscript-${manuscriptId}`}
      isLast={isLast}
      onClick={onClick}
    >
      <MainContainer>
        <Row alignItems="center" justify="space-between">
          <TextTooltip title={title}>
            <H3>{title}</H3>
          </TextTooltip>
          <Tag data-test-id="manuscript-status" status>
            {visibleStatus}
          </Tag>
        </Row>
        {authors.length > 0 && (
          <Row alignItems="center" justify="flex-start" mb={1}>
            <AuthorTagList authors={authors} withTooltip />
          </Row>
        )}
        <Row alignItems="center" justify="flex-start">
          {customId && !!articleType && (
            <Text customId mr={1}>{`ID ${customId}`}</Text>
          )}
          {created && status !== 'draft' && (
            <DateParser humanizeThreshold={0} timestamp={created}>
              {(timestamp, timeAgo) => (
                <Text
                  mr={3}
                >{`Submitted on ${timestamp} (${timeAgo} ago)`}</Text>
              )}
            </DateParser>
          )}
          <Text primary>{manuscriptType.label || articleType}</Text>
          {!!articleType && (
            <Text journal ml={1}>
              {journalName}
            </Text>
          )}
        </Row>
        <Row alignItems="center" height={4} justify="flex-start">
          <H4>Handling editor</H4>
          <Text display="flex" ml={1} mr={3} whiteSpace="nowrap">
            {handlingEditorVisibleStatus}
          </Text>

          {canSeeReviewerReports && (
            <Fragment>
              <H4 mr={1 / 2}>Reviwer Reports</H4>
              <ReviewerBreakdown reviewers={reviewers} />
            </Fragment>
          )}
          {status === 'draft' || (status !== 'draft' && role === 'admin') ? (
            <Item justify="flex-end" onClick={e => e.stopPropagation()}>
              <Modal
                cancelText={`${
                  manuscript.status === 'draft' ? 'CANCEL' : 'NO'
                }`}
                component={MultiAction}
                confirmText={`${
                  manuscript.status === 'draft' ? 'DELETE' : 'OK'
                }`}
                modalKey={`deleteManuscript-${manuscriptId}`}
                onConfirm={onDeleteManuscript}
                title={`Are you sure you want to ${
                  manuscript.status === 'draft'
                    ? 'delete this submission?'
                    : 'remove this manuscript?'
                }`}
              >
                {showModal => (
                  <ActionLink
                    fontSize="12px"
                    fontWeight={700}
                    onClick={showModal}
                  >
                    <Icon fontSize="10px" icon="delete" mr={1 / 2} />
                    DELETE
                  </ActionLink>
                )}
              </Modal>
            </Item>
          ) : null}
        </Row>
      </MainContainer>
      <SideNavigation>
        <Icon color="colorSecondary" icon="caretRight" />
      </SideNavigation>
    </Root>
  )
}
export default compose(
  withRouter,
  withJournal,
  withHEStatus,
  withProps(({ manuscript: { meta, role, reviewers }, journal = {} }) => ({
    manuscriptType: get(journal, 'manuscriptTypes', []).find(
      t => t.value === get(meta, 'articleType', ''),
    ),

    journalName: get(journal, 'metadata.nameText', 'The journal name here'),
    canSeeReviewerReports:
      reviewers &&
      reviewers.length > 0 &&
      !['author', 'reviewer'].includes(role),
  })),

  withHandlers({
    onClick: ({ manuscript, history, ...rest }) => () => {
      if (manuscript.status === 'draft') {
        history.push(`/submit/${manuscript.submissionId}/${manuscript.id}`)
      } else {
        history.push(`/details/${manuscript.submissionId}/${manuscript.id}`)
      }
    },
    onDeleteManuscript: ({ manuscript, deleteManuscript }) => modalProps => {
      deleteManuscript(manuscript, modalProps)
    },
  }),
  setDisplayName('ManuscriptCard'),
)(ManuscriptCard)
// #region styles

const teamMember = PropTypes.shape({
  id: PropTypes.string,
  isSubmitting: PropTypes.bool,
  isCorresponding: PropTypes.bool,
  alias: PropTypes.shape({
    aff: PropTypes.string,
    email: PropTypes.string,
    country: PropTypes.string,
    name: PropTypes.shape({
      surname: PropTypes.string,
      givenNames: PropTypes.string,
      title: PropTypes.string,
    }),
  }),
})

ManuscriptCard.propTypes = {
  manuscript: PropTypes.shape({
    id: PropTypes.string,
    role: PropTypes.string,
    created: PropTypes.string,
    customId: PropTypes.string,
    visibleStatus: PropTypes.string,
    meta: PropTypes.shape({
      agreeTc: PropTypes.bool,
      title: PropTypes.string,
      abstract: PropTypes.string,
      articleType: PropTypes.string,
    }),
    handlingEditor: teamMember,
    authors: PropTypes.arrayOf(teamMember),
  }),
}

ManuscriptCard.defaultProps = {
  manuscript: {},
}

// #region styles
const MainContainer = styled.div`
  justify-content: flex-start;
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 2);
  padding-bottom: ${th('gridUnit')};
  width: calc(100% - (${th('gridUnit')} * 5 / 2));
  overflow: hidden;
  ${Row} {
    [data-tooltipped] {
      overflow: hidden;
    }
    ${H4} {
      white-space: nowrap;
    }
    ${H3} {
      display: block;
      margin-bottom: 0;
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      padding-right: ${th('gridUnit')};
    }
  }
`
const SideNavigation = styled.div`
  align-items: center;
  background-color: ${th('colorBackgroundHue2')
    ? th('colorBackgroundHue2')
    : th('colorBackgroundHue')};
  border-top-right-radius: ${th('borderRadius')};
  border-bottom-right-radius: ${th('borderRadius')};
  display: flex;
  justify-content: center;
  width: calc(${th('gridUnit')} * 3);
`

const Root = styled.div`
  background-color: #fff;
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  cursor: pointer;
  display: flex;
  margin: calc(${th('gridUnit')} / 4) calc(${th('gridUnit')} / 4)
    ${th('gridUnit')} calc(${th('gridUnit')} / 4);

  ${H3} {
    margin: 0;
    margin-bottom: ${th('gridUnit')};
  }

  &:hover {
    box-shadow: ${th('dashboardCard.hoverShadow')};
  }

  margin-bottom: calc(${props => (props.isLast ? 3 : 1)} * ${th('gridUnit')});
`
// #endregion
