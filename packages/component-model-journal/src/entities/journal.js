const { HindawiBaseModel } = require('component-model')

class Journal extends HindawiBaseModel {
  static get tableName() {
    return 'journal'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        title: { type: 'string' },
        publisherName: { type: 'string' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-manuscript').model,
        join: {
          from: 'journal.id',
          to: 'manuscript.journalId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-team').model,
        join: {
          from: 'journal.id',
          to: 'team.journalId',
        },
      },
    }
  }

  getEditorInChief() {
    if (!this.teams) {
      throw new Error('Teams are required')
    }
    const Team = require('component-model-team').model
    const eicTeam = this.teams.find(t => t.role === Team.Role.editorInChief)

    return eicTeam ? eicTeam.members[0] : undefined
  }

  toDTO() {
    const Team = require('component-model-team').model
    const eicTeam = this.teams.find(t => t.role === Team.Role.editorInChief)
    const eic = eicTeam.members[0]
    return {
      ...this,
      editorInChief: eic ? eic.toDTO() : undefined,
    }
  }
}

module.exports = Journal
