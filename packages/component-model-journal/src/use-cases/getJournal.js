const initialize = ({ Journal }) => ({
  execute: async journalId => {
    const journal = await Journal.find(journalId, [
      'teams.[members.[user.[identities]]]',
    ])

    return journal.toDTO()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
