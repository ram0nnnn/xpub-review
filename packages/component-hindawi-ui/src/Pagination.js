import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { Icon, Label, Text } from '../'

const PaginationComponent = ({
  items,
  toLast,
  toFirst,
  nextPage,
  prevPage,
  page = 1,
  maxItems = 23,
  hasMore = true,
  itemsPerPage = 10,
  changeItemsPerPage = () => {},
}) => (
  <Root>
    <Text mr={1} secondary>
      Showing
    </Text>
    <TextInput onChange={changeItemsPerPage} value={itemsPerPage} />
    {page !== 0 && (
      <Fragment>
        <Icon icon="arrowEndLeft" onClick={toFirst} />
        <Icon icon="caretLeft" ml={2} mr={1} onClick={prevPage} />
      </Fragment>
    )}

    <Label>{`${page * itemsPerPage + 1} to ${
      hasMore ? itemsPerPage * (page + 1) : maxItems
    }`}</Label>
    <Text ml={1 / 2} secondary>
      out of {items.length}
    </Text>
    {hasMore && (
      <Fragment>
        <Icon icon="caretRight" ml={1} mr={2} onClick={nextPage} />
        <Icon icon="arrowEnd" onClick={toLast} />
      </Fragment>
    )}
  </Root>
)

export const Pagination = ({ children, ...props }) => (
  <Fragment>
    <PaginationComponent {...props} />
    {typeof children === 'function' && children(props)}
  </Fragment>
)

Pagination.propTypes = {
  /** Page current number. */
  page: PropTypes.number,
  /** Indicates if there are more pages to be displayed. */
  hasMore: PropTypes.bool,
  /** Maximum items displayed. */
  maxItems: PropTypes.number,
  /** Items displayed per page. */
  itemsPerPage: PropTypes.number,
  /** Change how many items should be displayed per page. */
  changeItemsPerPage: PropTypes.func,
}
Pagination.defaultProps = {
  page: 1,
  hasMore: false,
  maxItems: 23,
  itemsPerPage: 10,
  changeItemsPerPage: () => {},
}

export default PaginationComponent

// #region styles
const Root = styled.div`
  align-items: baseline;
  display: flex;
  justify-content: center;
`

const TextInput = styled(TextField)`
  margin: 0;
  margin-right: calc(${th('gridUnit')} * 2);
  width: calc(${th('gridUnit')} * 5);
  height: calc(${th('gridUnit')} * 4);

  & input {
    text-align: center;
    vertical-align: middle;
  }
`
// #endregion
