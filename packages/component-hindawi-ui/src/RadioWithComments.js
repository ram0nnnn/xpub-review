import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { YesOrNo } from '@pubsweet/ui'
import { required as requiredValidator } from 'xpub-validators'

import {
  Row,
  Item,
  Text,
  Label,
  Textarea,
  ActionLink,
  IconTooltip,
  RowOverrideAlert,
  ValidatedFormField,
} from '../'

const RadioWithComments = ({
  required,
  subtitle,
  radioLabel,
  formValues,
  commentsOn,
  radioFieldName,
  tooltipContent,
  commentsSubtitle,
  commentsFieldName,
  commentsPlaceholder,
}) => (
  <Root>
    <Row alignItems="center" justify="flex-start">
      <Item>
        <Label required={required}>{radioLabel}</Label>
        {tooltipContent && (
          <IconTooltip content={tooltipContent} interactive ml={1} primary />
        )}
      </Item>
    </Row>

    <Row
      alignItems="center"
      data-test-id={`submission-yes-or-no-${radioFieldName}`}
      justify="flex-start"
      mb={1}
      mt={1}
    >
      <ValidatedFormField
        component={YesOrNo}
        name={radioFieldName}
        validate={required && [requiredValidator]}
      />
    </Row>

    {get(formValues, radioFieldName, '') === commentsOn && (
      <RowOverrideAlert alignItems="center" justify="flex-start">
        <Item data-test-id="submission-conflicts-text" vertical>
          {commentsSubtitle && (
            <Text display="inherit" secondary>
              {commentsSubtitle}
              {subtitle && (
                <ActionLink ml={1 / 2} to={subtitle.link}>
                  {subtitle.label}
                </ActionLink>
              )}
            </Text>
          )}
          <ValidatedFormField
            component={Textarea}
            name={commentsFieldName}
            placeholder={commentsPlaceholder}
            validate={required && [requiredValidator]}
          />
        </Item>
      </RowOverrideAlert>
    )}
  </Root>
)

RadioWithComments.propTypes = {
  /** Specifies if form field is required */
  required: PropTypes.bool,
  /** Values contained by the form */
  formValues: PropTypes.object, //eslint-disable-line
  /** Name of a specific form field */
  radioFieldName: PropTypes.string,
  /** Name of text area field */
  commentsFieldName: PropTypes.string,
  /** Radio value for wich text area is visible */
  commentsOn: PropTypes.oneOf(['yes', 'no']).isRequired,
  /** Label title for a radio field */
  radioLabel: PropTypes.string,
}
RadioWithComments.defaultProps = {
  required: false,
  formValues: {},
  radioFieldName: '',
  commentsFieldName: '',
  radioLabel: '',
}

export default RadioWithComments

// #region styles
const Root = styled.div`
  width: 100%;
`
// #endregion
