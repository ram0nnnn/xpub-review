A row of items.

```js
<Row>
  <Item>Item 1</Item>
  <Item>Item 2</Item>
  <Item>Item 3</Item>
</Row>
```

Items are aligned in their containing block

```js
<Row alignItems="flex-start" vertical>
  <Item>I m the first item</Item>
  <Item>I m the second item</Item>
  <Item>I m the third item</Item>
</Row>
```

Long items wrap on the next row

```js
<Row flexWrap="wrap">
  <Item>
    wrap us together please, i m first, and i will be the longest to prove my
    point
  </Item>
  <Item>wrap us together please, i m second</Item>
  <Item>wrap us together please, i m last</Item>
</Row>
```

Adds spaces between items

```js
<Row justifyContent="space-evenly">
  <Item>group us from the left, i m first</Item>
  <Item>group us from the left, i m second</Item>
  <Item>group us from the left, i m last</Item>
</Row>
```

The height of an item is specified

```js
<Row height="100%">
  <Item>this is an item</Item>
</Row>
```

Adds color to the row

```js
<Row bgColor="aqua">
  <Item>this is an item</Item>
</Row>
```
