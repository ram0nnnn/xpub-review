Manuscript handling editors table.

```js
const handlingEditors = [
  {
    id: '1',
    firstName: 'Handling',
    lastName: 'Edi',
    email: 'handling1@edi.com',
  },
  {
    id: '2',
    firstName: 'Alex',
    lastName: 'Pricop',
    email: 'handling2@edi.com',
  },
  {
    id: '3',
    firstName: 'Bogdan',
    lastName: 'Cochior',
    email: 'handling3@edi.com',
  },
]

const currentUser = {
  permissions: {
    canAssignHE: true,
  },
}

;<RemoteOpener>
  {(expanded, toggle) => (
    <ManuscriptAssignHE
      toggle={toggle}
      expanded
      currentUser={currentUser}
      handlingEditors={handlingEditors}
      assignHE={he => console.log('assigning...', he)}
    />
  )}
</RemoteOpener>
```
