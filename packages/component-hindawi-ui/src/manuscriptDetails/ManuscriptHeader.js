import React, { Fragment } from 'react'
import { get } from 'lodash'
import { withJournal } from 'xpub-journal'
import { H2, H4, Button, DateParser } from '@pubsweet/ui'
import { compose, withProps, setDisplayName } from 'recompose'
import withLabelVisibility from './decorators/withLabelVisibility'
import withHEStatus from './decorators/withHEStatus'

import { Tag, Row, Text, Label, AuthorTagList, PersonInvitation } from '../../'

const HEAssignator = ({
  onResendHE,
  removeHE,
  toggleAssignHE,
  handlingEditor,
  onCancelHEInvitation,
  isLatestVersion,
  canSeeInviteRevoke,
  canSeeResendRemove,
}) =>
  (canSeeInviteRevoke || canSeeResendRemove) && (
    <Fragment>
      <PersonInvitation
        invitation={handlingEditor}
        label="Handling Editor"
        onRemove={removeHE}
        onResend={onResendHE}
        onRevoke={onCancelHEInvitation}
      />
      {!get(handlingEditor, 'id', null) && isLatestVersion && (
        <Button ml={1} onClick={toggleAssignHE} primary size="small">
          INVITE
        </Button>
      )}
    </Fragment>
  )

const HELabel = ({
  handlingEditorVisibleStatus,
  canSeeLabelAuthorReviewer,
  canSeeLabelOldVersionsAdminOrEiC,
  canSeeLabelUnassigned,
}) =>
  (canSeeLabelAuthorReviewer ||
    canSeeLabelOldVersionsAdminOrEiC ||
    canSeeLabelUnassigned) && (
    <Fragment>
      <Label mr={1}>Handling Editor</Label>
      <Text>{handlingEditorVisibleStatus}</Text>
    </Fragment>
  )

const ManuscriptHeader = ({
  removeHE,
  manuscript,
  onResendHE,
  journalTitle,
  editorInChief,
  handlingEditor,
  toggleAssignHE,
  isLatestVersion,
  onCancelHEInvitation,
  manuscriptType = {},
  handlingEditorVisibleStatus,
  canSeeLabelAuthorReviewer,
  canSeeLabelOldVersionsAdminOrEiC,
  canSeeInviteRevoke,
  canSeeResendRemove,
  canSeeLabelUnassigned,
}) => (
  <Fragment>
    <Row
      alignItems="flex-start"
      data-test-id="manuscript-title"
      justify="space-between"
    >
      <H2>{manuscript.meta.title || 'No title'}</H2>
      <Tag data-test-id="fragment-status" oldStatus={!isLatestVersion} status>
        {isLatestVersion
          ? manuscript.visibleStatus
          : 'Viewing An Older Version'}
      </Tag>
    </Row>

    {manuscript.authors.length > 0 && (
      <Row
        alignItems="center"
        data-test-id="authors-row"
        justify="flex-start"
        mt={1}
      >
        <AuthorTagList
          authors={manuscript.authors}
          withAffiliations
          withTooltip
        />
      </Row>
    )}
    <Row alignItems="center" justify="flex-start" mt={1}>
      {manuscript.customId && (
        <Text customId data-test-id="manuscript-id" mr={1}>{`ID ${
          manuscript.customId
        }`}</Text>
      )}
      {manuscript.created && (
        <DateParser durationThreshold={0} timestamp={manuscript.created}>
          {timestamp => <Text mr={3}>Submitted on {timestamp}</Text>}
        </DateParser>
      )}
      <Text>{manuscriptType.label}</Text>
      <Text journal ml={1}>
        {journalTitle}
      </Text>
    </Row>

    <Row alignItems="center" justify="flex-start" mt={1}>
      <H4>Editor in Chief</H4>
      <Text ml={1} mr={3}>
        {editorInChief}
      </Text>

      <HELabel
        canSeeLabelAuthorReviewer={canSeeLabelAuthorReviewer}
        canSeeLabelOldVersionsAdminOrEiC={canSeeLabelOldVersionsAdminOrEiC}
        canSeeLabelUnassigned={canSeeLabelUnassigned}
        handlingEditorVisibleStatus={handlingEditorVisibleStatus}
      />

      <HEAssignator
        canSeeInviteRevoke={canSeeInviteRevoke}
        canSeeResendRemove={canSeeResendRemove}
        handlingEditor={handlingEditor}
        isLatestVersion={isLatestVersion}
        onCancelHEInvitation={onCancelHEInvitation}
        onResendHE={onResendHE}
        removeHE={removeHE}
        toggleAssignHE={toggleAssignHE}
      />
    </Row>
  </Fragment>
)

export default compose(
  withJournal,
  withProps(({ journal = {}, manuscript }) => ({
    manuscriptType: get(journal, 'manuscriptTypes', []).find(
      t => t.value === get(manuscript, 'meta.articleType', ''),
    ),
    role: get(manuscript, 'role'),
    journalTitle: get(journal, 'metadata.nameText', ''),
    manuscriptStatus: get(manuscript, 'status', ''),

    handlingEditor: get(manuscript, 'handlingEditor', {}),
    heStatus: get(manuscript, 'handlingEditor.status', ''),
    editorInChief: `${get(
      manuscript,
      'editorInChief.alias.name.givenNames',
      '',
    )} ${get(manuscript, 'editorInChief.alias.name.surname', 'Unassigned')}`,
  })),
  withLabelVisibility,
  withHEStatus,
  setDisplayName('ManuscriptHeader'),
)(ManuscriptHeader)
