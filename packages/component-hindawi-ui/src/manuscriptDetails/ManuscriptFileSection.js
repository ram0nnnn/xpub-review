import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import { Item, File, Row, Text } from '../../'

const ManuscriptFileSection = ({ list, label }) => (
  <Fragment>
    {!!list.length && (
      <Fragment>
        {label && (
          <Text fontWeight="bold" labelLine mb={1} mt={1} smallSize>
            {label}
          </Text>
        )}
        <Row flexWrap="wrap" justify="flex-start" mb={1}>
          {list.map(file => (
            <Item
              alignItems="flex-start"
              flex={0}
              key={file.id}
              mr={1}
              vertical
            >
              <File item={file} mb={1} />
            </Item>
          ))}
        </Row>
      </Fragment>
    )}
  </Fragment>
)

ManuscriptFileSection.propTypes = {
  /** List of uploaded files */
  list: PropTypes.arrayOf(PropTypes.object),
  /** Category name of uploaded files. */
  label: PropTypes.string,
}

ManuscriptFileSection.defaultProps = {
  list: [],
  label: '',
}

export default ManuscriptFileSection
