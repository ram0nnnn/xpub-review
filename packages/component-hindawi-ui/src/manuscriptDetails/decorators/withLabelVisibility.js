import { withProps } from 'recompose'

export default withProps(
  ({ role, heStatus, isLatestVersion, manuscriptStatus }) => ({
    canSeeLabelAuthorReviewer:
      ['pending', 'accepted', ''].includes(heStatus) &&
      !['admin', 'editorInChief'].includes(role),

    canSeeLabelOldVersionsAdminOrEiC:
      ['accepted', ''].includes(heStatus) &&
      !isLatestVersion &&
      ['admin', 'editorInChief'].includes(role),

    canSeeLabelUnassigned:
      role === 'admin' && manuscriptStatus === 'technicalChecks',

    canSeeInviteRevoke:
      isLatestVersion &&
      ['', 'accepted'].includes(heStatus) &&
      ['admin', 'editorInChief'].includes(role) &&
      manuscriptStatus !== 'technicalChecks',

    canSeeResendRemove:
      heStatus === 'pending' && ['admin', 'editorInChief'].includes(role),
  }),
)
