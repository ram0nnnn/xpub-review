A shadowed box with content in it.

```js
<ShadowedBox>
  <Text>Hi there!</Text>
  <Text secondary>Hi there secondary!</Text>
  <Text error>Hi there error!</Text>
</ShadowedBox>
```
