import React, { Fragment } from 'react'
import { Formik } from 'formik'
import { isNumber, get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { required } from 'xpub-validators'

import { Checkbox, H3, TextField, Spinner } from '@pubsweet/ui'

import {
  Row,
  Text,
  Icon,
  Item,
  Label,
  validators,
  MenuCountry,
  ValidatedFormField,
} from 'component-hindawi-ui'

import { compose, withHandlers, withProps } from 'recompose'

const AuthorCardEdit = ({
  index,
  saveAuthor,
  isFetching,
  cancelEdit,
  deleteAuthor,
  fetchingError,
  initialValues,
}) => (
  <Formik initialValues={initialValues} onSubmit={saveAuthor(index)}>
    {({ handleSubmit, ...rest }) => (
      <Fragment>
        <Row data-testid="new-user" justify="space-between" mb={1}>
          <Row justify="flex-start">
            <H3 mr={1}>
              {isNumber(index) ? `#${index + 1} Author` : 'Author'}
            </H3>
            <ValidatedFormField
              component={input => (
                <Checkbox
                  checked={input.value}
                  {...input}
                  label="Corresponding"
                />
              )}
              name="isCorresponding"
            />
          </Row>

          {fetchingError && (
            <Row>
              <Text error>{fetchingError}</Text>
            </Row>
          )}

          {isFetching ? (
            <StyledSpinner>
              <Spinner />
            </StyledSpinner>
          ) : (
            <Fragment>
              <Icon
                data-test-id={`cancel-edit-${index}`}
                fontSize="16px"
                icon="remove"
                mr={3}
                onClick={cancelEdit}
              />

              <Icon
                data-test-id={`save-author-${index}`}
                fontSize="16px"
                icon="save"
                mr={1}
                onClick={handleSubmit}
              />
            </Fragment>
          )}
        </Row>
        <Fragment>
          <Row>
            <Item mr={1} vertical>
              <Label required>Email</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="email-author"
                inline
                name="email"
                validate={[required, validators.emailValidator]}
              />
            </Item>

            <Item mr={1} vertical>
              <Label required>First Name</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="givenNames-author"
                inline
                name="givenNames"
                validate={[required]}
              />
            </Item>

            <Item mr={1} vertical>
              <Label required>Last Name</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="surname-author"
                inline
                name="surname"
                validate={[required]}
              />
            </Item>

            <Item mr={1} vertical>
              <Label required>Affiliation</Label>
              <ValidatedFormField
                component={TextField}
                data-test-id="affiliation-author"
                inline
                name="aff"
                validate={[required]}
              />
            </Item>

            <Item mr={1} vertical>
              <Label required>Country</Label>
              <ValidatedFormField
                component={MenuCountry}
                data-test-id="country-author"
                inline
                name="country"
                validate={[required]}
              />
            </Item>
          </Row>
        </Fragment>
      </Fragment>
    )}
  </Formik>
)

export default compose(
  withProps(({ item }) => ({
    initialValues: {
      aff: get(item, 'alias.aff', ''),
      email: get(item, 'alias.email', ''),
      country: get(item, 'alias.country', ''),
      surname: get(item, 'alias.name.surname', ''),
      givenNames: get(item, 'alias.name.givenNames', ''),
      isSubmitting: get(item, 'isSubmitting'),
      isCorresponding: get(item, 'isCorresponding'),
    },
  })),
  withHandlers({
    changeCorresponding: ({ setCorresponding, item, ...props }) => () => {
      setCorresponding(item, props)
    },
  }),
)(AuthorCardEdit)

// #region styles
const StyledSpinner = styled.div`
  position: absolute;
  right: calc(${th('gridUnit')} * 2);
  top: ${th('gridUnit')};
`
// #endregion
