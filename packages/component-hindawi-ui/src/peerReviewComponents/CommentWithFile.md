Single comment ctx box with file

```js
const props = {
  boxLabel: 'Response to Reviewer Comments',
  commentLabel: 'Your Reply',
  date: new Date().toISOString(),
  message:
    'A sequence of images come into view on a computer monitor or on an x-ray plate as if the head had been sliced from side to side by a huge salami cutter and the slices were arranged out horizontally and in series.',
  file: {
    id: 'response_to_reviewer',
    originalName: 'response_to_reviewer.pdf',
    mimeType: 'application/pdf',
    size: 1231312,
  },
  onPreview: () => console.log('Preview or download'),
}
;<CommentWithFile {...props} />
```
