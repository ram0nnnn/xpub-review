import React from 'react'
import { Formik } from 'formik'
import PropTypes from 'prop-types'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { required } from 'xpub-validators'
import { Button } from '@pubsweet/ui'
import { withModal } from 'component-modal'
import { compose, withProps, withHandlers } from 'recompose'

import {
  Row,
  Text,
  Label,
  Textarea,
  ContextualBox,
  ValidatedFormField,
  MultiAction,
  Menu,
  Item,
} from '../../'

const RecommendationForm = ({
  options,
  onVisibility,
  formValues,
  handleSubmit,
  fieldForAuthor,
  fieldAuthorOptional,
  fieldForEiC,
}) => (
  <Root>
    <Row width={31}>
      <Item vertical>
        <Label required>Recommendation</Label>
        <ValidatedFormField
          component={Menu}
          name="recommendation"
          options={options}
          validate={[required]}
        />
      </Item>
    </Row>

    <ResponsiveRow mt={1}>
      {fieldForAuthor && (
        <ResponsiveItem
          data-test-id="editorial-recommendation-message-for-author"
          mr={1}
          vertical
        >
          <Label required>Message for Author </Label>
          <ValidatedFormField
            component={Textarea}
            name="public"
            validate={[required]}
          />
        </ResponsiveItem>
      )}

      {fieldAuthorOptional && (
        <ResponsiveItem
          data-test-id="editorial-recommendation-message-for-author"
          ml={1}
          vertical
        >
          <Label>
            Message for Author
            <Text ml={1 / 2} secondary>
              Optional
            </Text>
          </Label>
          <ValidatedFormField component={Textarea} name="public" />
        </ResponsiveItem>
      )}

      {fieldForEiC && (
        <ResponsiveItem
          data-test-id="editorial-recommendation-message-for-eic"
          ml={1}
          vertical
        >
          <Label>
            Message for Editor in Chief
            <Text ml={1 / 2} secondary>
              Optional
            </Text>
          </Label>
          <ValidatedFormField component={Textarea} name="private" />
        </ResponsiveItem>
      )}
    </ResponsiveRow>

    {onVisibility && (
      <Row justify="flex-end" mt={1}>
        <Button
          data-test-id="button-editorial-recommendation-submit"
          mb={2}
          onClick={handleSubmit}
          primary
          width={24}
        >
          {
            options.find(o => o.value === get(formValues, 'recommendation', ''))
              .button
          }
        </Button>
      </Row>
    )}
  </Root>
)

const EnhancedRecommendationForm = compose(
  withProps(({ formValues, options, status }) => ({
    fieldAuthorOptional: formValues.recommendation === 'publish',
    fieldForEiC: ['publish', 'reject'].includes(formValues.recommendation),
    fieldForAuthor: ['reject', 'major', 'minor'].includes(
      formValues.recommendation,
    ),
    onVisibility: ['reject', 'minor', 'publish', 'major'].includes(
      formValues.recommendation,
    ),
    options:
      status === 'heAssigned'
        ? options.filter(o => o.value !== 'publish')
        : options.filter(o => o.value),
  })),
)(RecommendationForm)

const HERecommendation = ({
  options,
  status,
  toggle,
  onSubmit,
  initialValues,
  manuscript,
  isVisible,
  expanded,
  highlight,
  ...rest
}) =>
  isVisible && (
    <Formik initialValues={initialValues} onSubmit={onSubmit}>
      {({ handleSubmit, values: formValues }) => (
        <ContextualBox
          expanded={expanded}
          highlight={highlight}
          label="Your Editorial Recommendation"
          mt={2}
          toggle={toggle}
          {...rest}
        >
          <EnhancedRecommendationForm
            formValues={formValues}
            handleSubmit={handleSubmit}
            options={options}
            status={status}
          />
        </ContextualBox>
      )}
    </Formik>
  )

export default compose(
  withProps(),
  withModal({
    component: MultiAction,
    modalKey: 'heRecommendation',
  }),
  withHandlers({
    onSubmit: ({ onSubmit, showModal, options }) => values => {
      const modalTitle = options.find(
        o => o.value === get(values, 'recommendation', ''),
      ).message

      const confirmMessage = options.find(
        o => o.value === get(values, 'recommendation', ''),
      ).button

      showModal({
        title: `${modalTitle}?`,
        content:
          "This will automatically remove reviewers who haven't submitted a review",
        confirmText:
          confirmMessage === 'Submit Recommendation'
            ? 'Submit'
            : confirmMessage,
        cancelText: 'CLOSE',
        onConfirm: modalProps => onSubmit(values, modalProps),
      })
    },
  }),
)(HERecommendation)

HERecommendation.propTypes = {
  /** Specifies HE recommendation options */
  options: PropTypes.arrayOf(PropTypes.object),
  /** Handles the submission of the recommendation */
  highlight: PropTypes.bool,
}

HERecommendation.defaultProps = {
  options: [],
  highlight: false,
}

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  display: flex;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 2);
  padding-bottom: 0;
`
const ResponsiveRow = styled(Row)`
  @media (max-width: 800px) {
    flex-direction: column;
  }
`
const ResponsiveItem = styled(Item)`
  @media (max-width: 800px) {
    margin-right: 0;
    margin-left: 0;
    width: 100%;
  }
`
// #endregion
