A list of reviewers.

```js
const reviewers = [
  {
    id: 'ff24930c-b767-4d36-acac-208da9ac3fff',
    isSubmitting: null,
    isCorresponding: null,
    invited: '2019-02-26T14:18:59.456Z',
    responded: '2019-02-26T14:18:59.456Z',
    user: {
      id: '15cb2e52-9c2d-44e3-a569-a38de94f600f',
    },
    status: 'pending',
    alias: {
      aff: 'TSD',
      email: 'alexandru.munteanu+rev1@thinslices.com',
      country: 'RO',
      name: {
        surname: 'REV1',
        givenNames: 'AlexREV1',
      },
    },
  },
  {
    id: '37b0a97e-7d61-4524-9b3e-55b494e05a51',
    isSubmitting: null,
    isCorresponding: null,
    invited: '2019-02-27T05:56:51.040Z',
    responded: '2019-02-27T10:12:49.267Z',
    user: {
      id: '5aead681-f12b-4528-b215-d7726cb4ef64',
    },
    status: 'pending',
    alias: {
      aff: 'TSD',
      email: 'alexandru.munteanu+rev3@thinslices.com',
      country: 'BE',
      name: {
        surname: 'REV3',
        givenNames: 'AlexREV3',
      },
    },
  },
  {
    id: 'db0cf125-195f-4cd5-aa4a-d41e151f6ffd',
    isSubmitting: null,
    isCorresponding: null,
    invited: '2019-02-27T10:12:49.257Z',
    responded: '2019-02-27T10:12:49.257Z',
    user: {
      id: 'f35cb7f6-c605-4bcf-b3ed-fc6e9f93c39f',
    },
    status: 'pending',
    alias: {
      aff: 'TSD',
      email: 'alexandru.munteanu+rev4@thinslices.com',
      country: 'AL',
      name: {
        surname: 'REV4',
        givenNames: 'AlexREV4',
      },
    },
  },
]

;<ReviewersTable reviewers={reviewers} />
```
