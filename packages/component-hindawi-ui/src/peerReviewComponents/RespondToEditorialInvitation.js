import React from 'react'
import { Formik } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { required } from 'xpub-validators'
import { Button } from '@pubsweet/ui'
import { compose, withHandlers } from 'recompose'

import {
  Item,
  Text,
  Label,
  Textarea,
  RadioGroup,
  MultiAction,
  ContextualBox,
  ValidatedFormField,
} from '../../'

const options = [
  { label: 'Agree', value: 'yes' },
  { label: 'Decline', value: 'no' },
]
const RespondToEditorialInvitation = ({
  mt,
  onSubmit,
  highlight,
  isVisible,
  startExpanded,
}) =>
  isVisible ? (
    <ContextualBox
      highlight={highlight}
      label="Respond to Editorial Invitation"
      mt={mt}
      startExpanded={startExpanded}
    >
      <Formik onSubmit={onSubmit}>
        {({ handleSubmit, values }) => (
          <Root>
            <Item vertical>
              <Label mb={1} required>
                Do you agree to be the handling editor for this manuscript?
              </Label>
              <ValidatedFormField
                component={RadioGroup}
                name="heDecision"
                options={options}
                validate={[required]}
              />
            </Item>

            {values.heDecision === 'no' && (
              <Item vertical>
                <Label>
                  Decline Reason
                  <Text ml={1 / 2} secondary>
                    Optional
                  </Text>
                </Label>
                <ValidatedFormField component={Textarea} name="reason" />
              </Item>
            )}

            <Item justify="flex-end">
              <Button onClick={handleSubmit} primary size="xLarge">
                Respond to Invitation
              </Button>
            </Item>
          </Root>
        )}
      </Formik>
    </ContextualBox>
  ) : null

export default compose(
  withModal({
    modalKey: 'he-respond',
    component: MultiAction,
  }),
  withHandlers({
    onSubmit: ({ showModal, onSubmit }) => values => {
      const title =
        values.heDecision === 'yes'
          ? 'Please confirm your agreement.'
          : 'Decline this invitation?'
      const confirmText = values.heDecision === 'yes' ? 'Agree' : 'Decline'

      showModal({
        title,
        onConfirm: modalProps => onSubmit(values, modalProps),
        confirmText,
      })
    },
  }),
)(RespondToEditorialInvitation)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-radius: ${th('borderRadius')};
  padding: calc(${th('gridUnit')} * 2);
`
// #endregion
