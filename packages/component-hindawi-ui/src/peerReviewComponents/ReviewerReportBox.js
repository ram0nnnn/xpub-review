import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import PropTypes from 'prop-types'
import { ContextualBox, Row, Text } from '../..'
import ReviewerReport from './ReviewerReport'

const SubmittedReports = ({ reports }) => (
  <Row fitContent justify="flex-end">
    <Text customId mr={1 / 2}>
      {reports}
    </Text>
    <Text mr={1 / 2} pr={1 / 2} secondary>
      submitted
    </Text>
  </Row>
)

const ReviewerReportBox = ({
  options,
  isVisible,
  startExpanded = false,
  reviewerReports,
  isLatestVersion,
  ...rest
}) =>
  isVisible && (
    <ContextualBox
      label={isLatestVersion ? 'Your Report' : 'Reviewer Reports'}
      mt={2}
      reports={reviewerReports.length}
      rightChildren={SubmittedReports}
      startExpanded={startExpanded}
    >
      <Wrapper>
        {reviewerReports.map(report => (
          <ReviewerReport
            key={report.id}
            options={options}
            reviewerReport={report}
          />
        ))}
      </Wrapper>
    </ContextualBox>
  )

export default ReviewerReportBox

ReviewerReportBox.propTypes = {
  reviewerReport: PropTypes.shape({
    /** Unique id for report. */
    id: PropTypes.string,
    /** Comments by reviewers. */
    comments: PropTypes.arrayOf(PropTypes.object),
    /** When the comment was created. */
    created: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    /** When the comment was submited. */
    submitted: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    /** The recommendation given by reviewer. */
    recommendation: PropTypes.string,
    /** The number of reviewer. */
    reviewerNumber: PropTypes.number,
    /** Details about reviewer. */
    member: PropTypes.object,
  }),
  /** State of visibility of contextual box */
  isVisible: PropTypes.bool,
}

ReviewerReportBox.defaultProps = {
  reviewerReport: {},
  isVisible: false,
}

const Wrapper = styled.div`
  background-color: ${th('colorBackgroundHue2')};
`
