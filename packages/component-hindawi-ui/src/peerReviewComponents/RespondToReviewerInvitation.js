import React from 'react'
import { Formik } from 'formik'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { required } from 'xpub-validators'
import { withModal } from 'component-modal'
import { compose, withHandlers } from 'recompose'

import {
  Item,
  Label,
  RadioGroup,
  MultiAction,
  ContextualBox,
  ValidatedFormField,
} from '../../'

const options = [
  { label: 'Agree', value: 'yes' },
  { label: 'Decline', value: 'no' },
]
const RespondToEditorialInvitation = ({
  mt,
  onSubmit,
  highlight,
  isVisible,
  startExpanded,
}) =>
  isVisible ? (
    <ContextualBox
      highlight={highlight}
      label="Respond to Invitation to Review"
      mt={mt}
      startExpanded={startExpanded}
    >
      <Formik onSubmit={onSubmit}>
        {({ handleSubmit, values }) => (
          <Root>
            <Item vertical>
              <Label mb={1} required>
                Do you agree to review this manuscript?
              </Label>
              <ValidatedFormField
                component={RadioGroup}
                name="reviewerDecision"
                options={options}
                validate={[required]}
              />
            </Item>

            <Item justify="flex-end">
              <Button onClick={handleSubmit} primary size="xLarge">
                Respond to Invitation
              </Button>
            </Item>
          </Root>
        )}
      </Formik>
    </ContextualBox>
  ) : null

export default compose(
  withModal({
    modalKey: 'reviewer-respond',
    component: MultiAction,
  }),
  withHandlers({
    onSubmit: ({ showModal, onSubmit }) => values => {
      const title =
        values.reviewerDecision === 'yes'
          ? 'Please confirm your agreement.'
          : 'Decline this invitation?'
      const confirmText =
        values.reviewerDecision === 'yes' ? 'AGREE' : 'DECLINE'

      showModal({
        title,
        onConfirm: modalProps => onSubmit(values, modalProps),
        confirmText,
      })
    },
  }),
)(RespondToEditorialInvitation)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-radius: ${th('borderRadius')};
  padding: calc(${th('gridUnit')} * 2);
`
// #endregion
