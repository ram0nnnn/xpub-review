import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import PropTypes from 'prop-types'
import { withProps, compose } from 'recompose'
import { isEmpty, get } from 'lodash'
import ReviewerReport from './ReviewerReport'
import { Row, Item, Text } from '../../'

const ReviewerReports = ({ reviewerReports, ...rest }) => {
  if (isEmpty(reviewerReports)) {
    return (
      <Row mb={2} ml={2} mt={2}>
        <Item>
          <Text data-test-id="error-empty-state" emptyState>
            No reports submitted yet
          </Text>
        </Item>
      </Row>
    )
  }
  return (
    <Wrapper>
      {reviewerReports.map(reviewerReport => (
        <ReviewerReport
          key={reviewerReport.id}
          reviewerReport={reviewerReport}
          {...rest}
        />
      ))}
    </Wrapper>
  )
}

export default compose(
  withProps(({ reviewerReports }) => ({
    reviewerReports: reviewerReports.filter(
      report =>
        get(report, 'submitted') &&
        get(report, 'recommendation') !== 'responseToRevision',
    ),
  })),
)(ReviewerReports)

ReviewerReports.propTypes = {
  reviewerReports: PropTypes.arrayOf(
    PropTypes.shape({
      /** Unique id for report. */
      id: PropTypes.string,
      /** Comments by reviewers. */
      comments: PropTypes.arrayOf(PropTypes.object),
      /** When the comment was created. */
      created: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      /** When the comment was submited. */
      submitted: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      /** The recommendation given by reviewer. */
      recommendation: PropTypes.string,
      /** The number of reviewer. */
      reviewerNumber: PropTypes.number,
      /** Details about reviewer. */
      member: PropTypes.object,
    }),
  ),
}

ReviewerReports.defaultProps = {
  reviewerReports: [],
}

const Wrapper = styled.div`
  background-color: ${th('colorBackgroundHue2')};
`
