import React from 'react'
import styled from 'styled-components'
import { DateParser } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { File } from 'component-files/client'
import { ContextualBox, Label, Item, Box, Row, Text } from '../..'

const CommentWithFile = ({
  mt,
  file,
  date,
  message,
  boxLabel,
  isVisible,
  commentLabel,
  startExpanded,
}) =>
  isVisible ? (
    <ContextualBox label={boxLabel} mt={mt} startExpanded={startExpanded}>
      <Root>
        <Box pb={2} pl={2} pr={2} pt={2}>
          <Row alignItems="flex-start" justify="space-between" mb={1 / 2}>
            <Item>
              <Label>{commentLabel}</Label>
            </Item>

            <Item alignItems="baseline" justify="flex-end">
              {date && (
                <DateParser timestamp={date}>
                  {date => <Text secondary>{date}</Text>}
                </DateParser>
              )}
            </Item>
          </Row>
          <Row justify="flex-start" mb={file ? 2 : 0}>
            <Text>{message}</Text>
          </Row>
          {file && (
            <Row justify="flex-start">
              <Item vertical>
                <Label mb={1 / 2}>File</Label>
                <File item={file} />
              </Item>
            </Row>
          )}
        </Box>
      </Root>
    </ContextualBox>
  ) : null

export default CommentWithFile

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  padding: calc(${th('gridUnit')});
`
