Button XLarge

```js
<Buttons xlarge icon="caretRight">Eu sunt XL</Buttons>
```

Button Large

```js
<Buttons large>Eu sunt Large</Buttons>
```

Button Medium

```js
<Buttons medium>Eu sunt medium</Buttons>
```

Button small

```js
<Buttons width={15} small>Eu sunt small</Buttons>
```
