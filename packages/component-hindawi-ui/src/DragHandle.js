import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Icon } from 'component-hindawi-ui'

import { marginHelper } from '../'

const DragHandle = props => (
  <Handle {...props}>
    <Icon color="colorSecondary" fontSize="17px" icon="move" />
  </Handle>
)

DragHandle.displayName = 'DragHandle'

DragHandle.protoTypes = {
  /** Designed size for icon */
  size: PropTypes.number,
}
DragHandle.defaultProps = {
  size: 2,
}

export default DragHandle

// #region styles
const Handle = styled.div`
  align-self: stretch;
  align-items: center;
  background-color: transparent;
  border-right: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  cursor: move;
  display: flex;
  flex-direction: column;
  justify-content: center;

  width: calc(${th('gridUnit')} * 4);

  span {
    padding: 0;
  }

  ${marginHelper};
`
// #endregion
