import React from 'react'
import { H1, H2, H3, H4 } from '@pubsweet/ui'

const Headings = ({ h1, h2, h3, h4 }) => (
  <div>
    {h1 && <H1>Heading 1</H1>}
    {h2 && <H2>Heading 2</H2>}
    {h3 && <H3>Heading 3</H3>}
    {h4 && <H4>Heading 4</H4>}
  </div>
)

export default Headings
