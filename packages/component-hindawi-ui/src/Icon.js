import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {
  marginHelper,
  paddingHelper,
  positionHelper,
} from 'component-hindawi-ui'

import { colorHelper } from './styledHelpers'

const Icon = ({ icon, onClick, className, ...rest }) => (
  <StyledIcon
    className={`icn_icn_${icon} ${className}`}
    onClick={onClick}
    {...rest}
  />
)

Icon.propTypes = {
  icon: PropTypes.oneOf([
    'arrowEndLeft',
    'caretRight',
    'caretUp',
    'warning',
    'collapse',
    'expand',
    'arrowLeft',
    'arrowEnd',
    'checks',
    'remove',
    'info',
    'tooltip',
    'downloadZip',
    'checkedBox',
    'moreDefault',
    'check',
    'resend',
    'caretLeft',
    'caretDown',
    'save',
    'delete',
    'link',
    'edit',
    'download',
    'preview',
    'remove1',
    'move',
    'bredcrumbs2',
  ]).isRequired,
}

export default Icon

// #region styles
const StyledIcon = styled.span`
  cursor: pointer;
  line-height: 1;

  font-weight: ${props => (props.bold ? 700 : 400)};
  font-size: ${props => (props.fontSize ? props.fontSize : '12px')};

  ${colorHelper};
  ${marginHelper};
  ${paddingHelper};
  ${positionHelper};
`
// #endregion
