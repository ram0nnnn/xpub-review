import { get } from 'lodash'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

import { marginHelper } from './styledHelpers'

const tagCSS = props => {
  if (get(props, 'oldStatus')) {
    return css`
      background-color: ${th('colorFurnitureHue')};
      height: calc(${th('gridUnit')} * 3)
      font-weight: ${th('tag.fontWeight')};
      padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')} 0px
        ${th('gridUnit')};
    `
  }

  if (get(props, `status`)) {
    return css`
      background-color: ${th('tag.statusBackgroundColor')};
      padding: calc(${th('gridUnit')} / 4) ${th('gridUnit')};
      height: calc(${th('gridUnit')} * 3);
      font-family: ${th('fontHeading')};
      display: flex;
      align-items: center;
    `
  }
  if (get(props, `pending`)) {
    return css`
      background-color: ${th('colorFurnitureHue')};
      height: calc(${th('gridUnit')} * 3);
      width: calc(${th('gridUnit')} * 10);
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: ${th('button.smallSize')};
      border-radius: ${th('borderRadius')};
    `
  }

  return css`
    background-color: ${th('tag.backgroundColor')};
    height: calc(${th('gridUnit')} * 2);
    font-family: ${th('fontInterface')};
  `
}

/** @component */
const Tag = styled.div`
  border-radius: ${th('tag.borderRadius')
    ? th('tag.borderRadius')
    : th('borderRadius')};
  color: ${th('tag.color')};
  display: initial;
  font-weight: ${th('tag.fontWeight')};
  font-size: ${th('tag.fontSize')};
  padding: 0 calc(${th('gridUnit')} / 2);
  text-align: center;
  white-space: nowrap;
  width: fit-content;

  ${tagCSS};
  ${marginHelper};
`

Tag.propTypes = {
  /** Old status of the corresponding user. */
  oldStatus: PropTypes.bool,
  /** New status of the corresponding user. */
  status: PropTypes.bool,
}

Tag.defaultProps = {
  oldStatus: false,
  status: false,
}

export default Tag
