import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { Action } from '@pubsweet/ui'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { withHandlers } from 'recompose'
import { th } from '@pubsweet/ui-toolkit'

import { displayHelper, paddingHelper, marginHelper } from './styledHelpers'

const ActionLink = ({
  to,
  onClick,
  disabled,
  children,
  renderLink,
  ...rest
}) => (
  <Root {...rest} to={to}>
    {renderLink(rest)}
  </Root>
)

ActionLink.propTypes = {
  /** Link/URL specifying where to navigate, outside or inside the app.
   * If present the component will behave like a navigation link. */
  to: PropTypes.string,
  /** Callback function fired when the component is clicked. */
  onClick: PropTypes.func,
  /** If true the component will be disabled (can't be interacted with). */
  disabled: PropTypes.bool,
}

ActionLink.defaultProps = {
  to: '',
  disabled: false,
  onClick: () => {},
}

export default withHandlers({
  renderLink: ({
    to,
    internal,
    disabled,
    onClick,
    children,
    fontSize,
    fontWeight,
    ...rest
  }) => () => {
    if (to && !internal) {
      return (
        <ExternalLink
          href={disabled ? undefined : to}
          target="_blank"
          {...rest}
        >
          {children}
        </ExternalLink>
      )
    }

    if (to && internal) {
      return <CustomLink to={to}>{children}</CustomLink>
    }
    return (
      <Action
        disabled={disabled}
        fontSize={fontSize}
        fontWeight={fontWeight}
        onClick={onClick}
      >
        {children}
      </Action>
    )
  },
})(ActionLink)

// #region styles
const ExternalLink = styled.a`
  color: ${th('colorSecondary')};
  cursor: pointer;
  font-family: ${th('defaultFont')};
  line-height: 1;
  text-decoration: underline;
  font-size: ${props => (props.fontSize ? props.fontSize : '')};
  opacity: ${props => (props.opacity ? props.opacity : '1')};
`
const CustomLink = ExternalLink.withComponent(Link)

const Root = styled.div`
  align-items: ${props => get(props, 'alignItems', 'center')};
  flex: ${props => props.flex || 'none'};
  justify-content: center;
  height: inherit;
  width: max-content;

  &:hover * {
    color: ${th('colorSecondary')};
  }

  ${displayHelper};
  ${marginHelper};
  ${paddingHelper};
`
// #endregion
