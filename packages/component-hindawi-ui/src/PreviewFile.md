Preview file in browser if possible

```js
const file = {
  name: 'cocojambo.pdf'
};

<PreviewFile file={file} />
```
