Shows the invited person and the possibility to cancel or resend the invite.

```js
const invitation = {
  id: '6dfd6286-3366-41fb-83e4-aed80955e579',
  isSubmitting: null,
  isCorresponding: null,
  status: 'pending',
  alias: {
    aff: 'Boko Haram',
    email: 'alexandru.munteanu+he1@thinslices.com',
    country: 'RO',
    name: {
      surname: 'MuntHE1',
      givenNames: 'AlexHE1',
    },
  },
}

;<PersonInvitation
  label="Handling Editor"
  invitation={invitation}
  onResend={id => console.log('resend invitation with id', id)}
  onRevoke={id => console.log('revoke invitation with id', id)}
/>
```
