import React from 'react'
import 'jest-styled-components'
import 'jest-dom/extend-expect'
import { cleanup, fireEvent } from 'react-testing-library'

import { render } from '../testUtils'
import ContextualBox from '../src/ContextualBox'

describe('ContextualBox', () => {
  afterEach(cleanup)

  it('should see text before and after collapsed contextual box .', () => {
    const { getByText } = render(
      <ContextualBox
        label="Reviewer reports"
        rightChildren={() => <div>1 invited, 0 accepted</div>}
      >
        <div>What is Lorem Ipsum?</div>
      </ContextualBox>,
    )
    fireEvent.click(getByText('Reviewer reports'))

    expect(getByText('Reviewer reports')).toBeInTheDocument()
    expect(getByText('1 invited, 0 accepted')).toBeInTheDocument()
    expect(getByText('What is Lorem Ipsum?')).toBeInTheDocument()
  })

  it('should not see text if contextual box is not colapsed .', () => {
    const { getByText, queryByText } = render(
      <ContextualBox label="Reviewer reports">
        <div>What is Lorem Ipsum?</div>
      </ContextualBox>,
    )

    expect(getByText('Reviewer reports')).toBeInTheDocument()
    expect(queryByText('What is Lorem Ipsum?')).toBeNull()
  })
})
