const config = require('config')
const convert = require('xml-js')

const { set, get, isEmpty } = require('lodash')

const manuscriptTypes = config.get('journalConfig.manuscriptTypes')

module.exports = {
  setMetadata: ({ meta, jsonTemplate, options, fileName }) => {
    const titleGroup = {
      'article-title': {
        _text: parseHtml(meta.title, options),
      },
    }
    const articleId = [
      {
        _attributes: {
          'pub-id-type': 'publisher-id',
        },
        _text: fileName,
      },
      {
        _attributes: {
          'pub-id-type': 'manuscript',
        },
        _text: fileName,
      },
    ]

    const articleType = {
      'subj-group': [
        {
          _attributes: {
            'subj-group-type': 'Article Type',
          },
          subject: {
            _text: get(
              manuscriptTypes.find(v => v.value === meta.articleType),
              'label',
              'Research Article',
            ),
          },
        },
      ],
    }
    set(jsonTemplate, 'article.front.article-meta.title-group', titleGroup)
    set(jsonTemplate, 'article.front.article-meta.article-id', articleId)
    set(
      jsonTemplate,
      'article.front.article-meta.article-categories',
      articleType,
    )
    set(
      jsonTemplate,
      'article.front.article-meta.abstract._text',
      meta.abstract,
    )

    return jsonTemplate
  },
  setHistory: (submitted, jsonTemplate) => {
    const date = new Date(submitted)
    const parsedDate = {
      date: {
        _attributes: {
          'date-type': 'received',
        },
        day: {
          _text: date.getDate(),
        },
        month: {
          _text: date.getMonth() + 1,
        },
        year: {
          _text: date.getFullYear(),
        },
      },
    }
    set(jsonTemplate, 'article.front.article-meta.history', parsedDate)

    return jsonTemplate
  },
  setFiles: (files, jsonTemplate) => {
    const jsonFiles = files.map(file => ({
      item_type: {
        _text: file.type,
      },
      item_description: {
        _text: file.originalName,
      },
      item_name: {
        _text: file.fileName,
      },
    }))

    set(jsonTemplate, 'article.front.files.file', jsonFiles)

    return jsonTemplate
  },
  setQuestions: (
    {
      hasConflicts = false,
      message = '',
      hasDataAvailability = false,
      dataAvailabilityMessage = '',
      hasFunding = false,
      fundingMessage = '',
    },
    jsonTemplate,
  ) => {
    const questions = []

    const getQuestionMessage = (selection, message, defaultMessage) => {
      if (selection) {
        return ''
      }
      return isEmpty(message) ? defaultMessage : message
    }

    questions.push({
      _attributes: {
        type: 'COI',
      },
      answer: {
        _text: hasConflicts ? 'Yes' : 'No',
      },
      statement: {
        _text: message,
      },
    })

    questions.push({
      _attributes: {
        type: 'DA',
      },
      answer: {
        _text: hasDataAvailability ? 'Yes' : 'No',
      },
      statement: {
        _text: getQuestionMessage(
          hasDataAvailability,
          dataAvailabilityMessage,
          'The authors for this paper did not provide a data availability statement',
        ),
      },
    })

    questions.push({
      _attributes: {
        type: 'Fund',
      },
      answer: {
        _text: hasFunding ? 'Yes' : 'No',
      },
      statement: {
        _text: getQuestionMessage(
          hasFunding,
          fundingMessage,
          'The authors for this paper did not provide a funding statement',
        ),
      },
    })

    set(jsonTemplate, 'article.front.questions.question', questions)
    return jsonTemplate
  },
  setContributors: (authors = [], jsonTemplate) => {
    const contrib = authors.map((author, i) => ({
      _attributes: {
        'contrib-type': 'author',
        corresp: author.isCorresponding ? 'Yes' : 'No',
      },
      role: {
        _attributes: {
          'content-type': '1',
        },
      },
      name: {
        surname: {
          _text: author.alias.surname,
        },
        'given-names': {
          _text: author.alias.givenNames,
        },
        prefix: {
          _text: author.alias.title || 'Dr.',
        },
      },
      email: {
        _text: author.alias.email,
      },
      xref: {
        _attributes: {
          'ref-type': 'aff',
          rid: `aff${i + 1}`,
        },
      },
    }))
    const aff = authors.map((a, i) => ({
      _attributes: {
        id: `aff${i + 1}`,
      },
      country: a.alias.country || 'UK',
      'addr-line': {
        _text: a.alias.aff || '',
      },
    }))

    set(
      jsonTemplate,
      'article.front.article-meta.contrib-group.contrib',
      contrib,
    )
    set(jsonTemplate, 'article.front.article-meta.contrib-group.aff', aff)

    return jsonTemplate
  },
  setEmptyReviewers: jsonTemplate => {
    set(jsonTemplate, 'article.front.rev-group', undefined)
    return jsonTemplate
  },
  setReviewers: (reviews = [], jsonTemplate) => {
    const xmlReviewers = reviews.map((review, i) => {
      const assigmentDate = new Date(review.member.responded)
      const submissionDate = new Date(review.submitted)
      const revType =
        review.member.team.role === 'reviewer' ? 'reviewer' : 'editor'
      const revObj = {
        _attributes: {
          'rev-type': revType,
        },
        name: {
          surname: {
            _text: review.member.alias.surname,
          },
          'given-names': {
            _text: review.member.alias.givenNames,
          },
          prefix: {
            _text: review.member.alias.title || 'Dr.',
          },
        },
        email: {
          _text: review.member.alias.email,
        },
        xref: {
          _attributes: {
            'ref-type': 'aff',
            rid: `aff${i + 1}`,
          },
        },
        date: [
          {
            _attributes: {
              'date-type': 'assignment',
            },
            day: {
              _text: assigmentDate.getDate(),
            },
            month: {
              _text: assigmentDate.getMonth() + 1,
            },
            year: {
              _text: assigmentDate.getFullYear(),
            },
          },
        ],
      }
      revObj.date.push({
        _attributes: {
          'date-type': 'submission',
        },
        day: {
          _text: submissionDate.getDate(),
        },
        month: {
          _text: submissionDate.getMonth() + 1,
        },
        year: {
          _text: submissionDate.getFullYear(),
        },
      })
      revObj.comment = review.comments.map(comm => ({
        _attributes: {
          'comment-type': comm.type === 'public' ? 'comment' : 'Confidential',
        },
        _text: comm.content,
      }))
      return revObj
    })
    const aff = reviews.map((review, i) => ({
      _attributes: {
        id: `aff${i + 1}`,
      },
      country: review.member.alias.country || 'UK',
      'addr-line': {
        _text: review.member.alias.aff || '',
      },
    }))

    set(jsonTemplate, 'article.front.rev-group.rev', xmlReviewers)
    set(jsonTemplate, 'article.front.rev-group.aff', aff)

    return jsonTemplate
  },
  createFileName: ({ id = Date.now(), prefix }) => `${prefix}${id}`,
}

const parseHtml = (content = '', options) => {
  if (/<\/?[^>]*>/.test(content)) {
    return convert.xml2js(content, options)
  }
  return content
}
