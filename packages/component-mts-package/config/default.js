const reviewDefaultConfig = require('hindawi-review/config/default')

const defaultParseXmlOptions = {
  compact: true,
  ignoreComment: true,
  spaces: 2,
  fullTagEmptyElement: true,
}

const defaultConfig = {
  doctype: 'article SYSTEM "JATS-archivearticle1-mathml3.dtd"',
  dtdVersion: '1.1d1',
  articleType: 'Research Article',
  journalIdPublisher: 'research',
  email: 'faraday@hindawi.com',
  journalTitle: 'Bioinorganic Chemistry and Applications',
  issn: '2474-7394',
  prefix: 'RESEARCH-F-',
}

const defaultS3Config = {
  secretAccessKey: process.env.AWS_S3_SECRET_KEY,
  accessKeyId: process.env.AWS_S3_ACCESS_KEY,
  region: process.env.AWS_S3_REGION,
  bucket: process.env.AWS_S3_BUCKET,
}

const defaultFTPConfig = {
  user: 'dlpuser@dlptest.com',
  password: '3D6XZV9MKdhM5fF',
  host: 'ftp.dlptest.com',
  port: 21,
  localRoot: `../files`,
  remoteRoot: '/',
  exclude: ['*.js'],
}

const journalConfig = {
  manuscriptTypes: [
    {
      label: 'Research Article',
      value: 'research',
      author: true,
      peerReview: true,
      abstractRequired: true,
    },
    {
      label: 'Review Article',
      value: 'review',
      author: true,
      peerReview: true,
      abstractRequired: true,
    },
    {
      label: 'Letter to the editor',
      value: 'letter-to-editor',
      author: true,
      peerReview: false,
      abstractRequired: false,
    },
  ],
}

module.exports = {
  ...reviewDefaultConfig,
  defaultConfig,
  defaultParseXmlOptions,
  defaultS3Config,
  defaultFTPConfig,
  journalConfig,
}
