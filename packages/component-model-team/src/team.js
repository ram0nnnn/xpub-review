const { HindawiBaseModel } = require('component-model')
const { model: TeamMember } = require('component-model-team-member')

class Team extends HindawiBaseModel {
  static get tableName() {
    return 'team'
  }

  static get schema() {
    return {
      properties: {
        role: {
          type: 'string',
          enum: Object.values(Team.Role),
          default: null,
        },
        manuscriptId: { type: ['string', 'null'] },
        journalId: { type: ['string', 'null'] },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-manuscript').model,
        join: {
          from: 'team.manuscriptId',
          to: 'manuscript.id',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-journal').model,
        join: {
          from: 'team.journalId',
          to: 'journal.id',
        },
      },
      members: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('component-model-team-member').model,
        join: {
          from: 'team.id',
          to: 'team_member.teamId',
        },
      },
    }
  }

  static get Role() {
    return {
      author: 'author',
      admin: 'admin',
      editorInChief: 'editorInChief',
      reviewer: 'reviewer',
      handlingEditor: 'handlingEditor',
    }
  }

  static get GlobalRoles() {
    return [Team.Role.admin, Team.Role.editorInChief, Team.Role.handlingEditor]
  }

  addMember(user, invitationOptions) {
    this.members = this.members || []
    const memberAlreadyExists = this.members.some(
      member => member.userId === user.id,
    )
    if (memberAlreadyExists) {
      const defaultIdentity = user.getDefaultIdentity()
      throw new Error(
        `User ${defaultIdentity.email} is already invited in the ${
          this.role
        } team`,
      )
    }

    const newMember = new TeamMember({
      ...invitationOptions,
      position: this.members.length,
    })
    newMember.linkUser(user)

    this.members.push(newMember)

    return newMember
  }

  toDTO() {
    const object = this.manuscript || this.journal

    return {
      ...this,
      object: object ? object.toDTO() : undefined,
      objectType: this.manuscriptId ? 'manuscript' : 'journal',
      members: this.members ? this.members.map(m => m.toDTO()) : undefined,
    }
  }

  removeMember(memberId) {
    this.members = this.members || []

    const member = this.members.find(member => member.id === memberId)
    if (!member) {
      throw new Error(`User does not exist in the ${this.role} team`)
    }
    if (member.isSubmitting) {
      throw new Error(`Submitting authors can't be deleted from the team`)
    }

    this.members = this.members.filter(member => member.id !== memberId)
  }
}

module.exports = Team
