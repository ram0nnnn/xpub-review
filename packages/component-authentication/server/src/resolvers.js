const logger = require('@pubsweet/logger')

const models = require('@pubsweet/models')

const { getCurrentUser, withAuthsomeMiddleware } = require('helper-service')
const {
  token: tokenService,
} = require('@pubsweet/model-user/src/authentication')

const useCases = require('./use-cases')
const notification = require('./notifications/notification')

const resolvers = {
  Query: {
    async currentUser(_, { input }, ctx) {
      return useCases.currentUserUseCase
        .initialize({ loggedUser: getCurrentUser, models })
        .execute({ user: ctx.user })
    },
  },
  Mutation: {
    async signUp(_, { input }, ctx) {
      return useCases.signUpUseCase
        .initialize(notification, tokenService, models)
        .execute(input)
    },
    async signUpFromInvitation(_, { input }, ctx) {
      return useCases.signUpFromInvitationUseCase
        .initialize(tokenService, models)
        .execute(input)
    },
    async requestPasswordReset(_, { email }, ctx) {
      return useCases.requestPasswordResetUseCase
        .initialize(notification, models)
        .execute(email)
    },
    async resetPassword(_, { input }, ctx) {
      return useCases.resetPasswordUseCase
        .initialize(logger, models)
        .execute(input)
    },
    async confirmAccount(_, { input }, ctx) {
      return useCases.confirmAccountUseCase
        .initialize({ tokenService, models })
        .execute(input)
    },
    async localLogin(_, { input }, ctx) {
      return useCases.localLoginUseCase
        .initialize(tokenService, models)
        .execute(input)
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
