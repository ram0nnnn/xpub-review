process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const logger = require('@pubsweet/logger')
const fixturesService = require('fixture-service')

const resetPasswordUseCase = require('../src/use-cases/resetPassword')

jest.mock('@pubsweet/logger', () => ({
  error: jest.fn(),
}))

const chance = new Chance()
const { fixtures, models } = fixturesService

describe('Reset password use case', () => {
  it('should return success when the body is correct', async () => {
    const user = fixtures.generateUser({
      passwordResetToken: chance.guid(),
    })

    const mockedModels = models.build(fixtures)

    await resetPasswordUseCase.initialize(logger, mockedModels).execute({
      email: user.identities[0].email,
      password: 'Password!23',
      token: user.passwordResetToken,
    })

    expect(user.passwordResetTimestamp).toBeNull()
    expect(user.passwordResetToken).toBeNull()
  })

  it('should return an error when the password is weak', async () => {
    const result = resetPasswordUseCase
      .initialize(logger, models.build(fixtures))
      .execute({
        email: chance.email(),
        password: 'weak-password',
        token: chance.guid(),
      })

    expect(result).rejects.toThrow(
      'Password is too weak. Please check password requirements.',
    )
  })

  it('should not return an error when the user is not found', async () => {
    const result = resetPasswordUseCase
      .initialize(logger, models.build(fixtures))
      .execute({
        email: chance.email(),
        password: 'Password!23',
        token: chance.guid(),
      })

    expect(result).resolves.not.toThrow()
  })

  it('should return an error when tokens do not match', async () => {
    const user = fixtures.generateUser({
      passwordResetToken: chance.guid(),
    })

    const mockedModels = models.build(fixtures)

    const result = resetPasswordUseCase
      .initialize(logger, mockedModels)
      .execute({
        email: user.identities[0].email,
        password: 'Password!23',
        token: chance.hash(),
      })

    expect(result).rejects.toThrow('Invalid request.')
  })
})
