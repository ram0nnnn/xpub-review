import { get } from 'lodash'
import { validators } from 'component-hindawi-ui'

export const parseSearchParams = url => {
  const params = new URLSearchParams(url)
  const parsedObject = {}
  /* eslint-disable */
  for ([key, value] of params) {
    parsedObject[key] = value
  }
  /* eslint-enable */
  return parsedObject
}

export const setToken = token => {
  window.localStorage.setItem('token', token)
}

export const removeToken = () => {
  window.localStorage.removeItem('token')
}

export const getRedirectTo = location =>
  `${get(location, 'state.from.pathname', '/')}${get(
    location,
    'state.from.search',
    '',
  )}`

export const loginValidate = values => {
  const errors = {}

  if (values.email !== 'admin' && !validators.emailRegex.test(values.email)) {
    errors.email = 'Invalid email'
  }

  if (!values.email) {
    errors.email = 'Required'
  }

  if (!values.password) {
    errors.password = 'Required'
  }

  return errors
}
