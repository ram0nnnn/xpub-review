import React from 'react'
import 'jest-styled-components'
import { cleanup, fireEvent } from 'react-testing-library'

import SignUpForm from '../components/SignUpForm'
import { render } from './testUtils'

const titles = [
  {
    label: 'Mr',
    value: 'mr',
  },
  {
    label: 'Mrs',
    value: 'mrs',
  },
  {
    label: 'Miss',
    value: 'miss',
  },
  {
    label: 'Ms',
    value: 'ms',
  },
  {
    label: 'Dr',
    value: 'dr',
  },
  {
    label: 'Professor',
    value: 'prof',
  },
]
const journal = {
  titles,
}

describe('SignUpForm component', () => {
  afterEach(cleanup)

  it('should set initial values', async done => {
    const initialValues = {
      aff: 'Boko Haram',
      agreeTc: true,
      country: 'Romania',
      givenNames: 'Firstnameovici',
      surname: 'Lastnameovici',
      title: 'miss',
    }

    const onSubmitMock = jest.fn()
    const { getByText } = render(
      <SignUpForm
        initialValues={initialValues}
        journal={journal}
        onSubmit={onSubmitMock}
        step={0}
      />,
    )

    fireEvent.click(getByText(/PROCEED TO SET EMAIL AND PASSWORD/i))
    setTimeout(() => {
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      expect(onSubmitMock).toHaveBeenCalledWith(
        initialValues,
        expect.anything(),
      )

      done()
    })
  })

  it('should move to the next step', async done => {
    const onSubmitMock = jest.fn()

    const initialValues = {
      aff: 'Boko Haram',
      agreeTc: false,
      country: 'Romania',
      givenNames: 'Firstnameovici',
      surname: 'Lastnameovici',
      title: 'miss',
    }

    const { clickCheckbox, getByTestId, getByText, selectOption } = render(
      <SignUpForm
        initialValues={initialValues}
        journal={journal}
        onSubmit={onSubmitMock}
        step={0}
      />,
    )

    fireEvent.change(getByTestId('first-name'), {
      target: {
        value: 'Firstnameovici',
      },
    })

    fireEvent.change(getByTestId('last-name'), {
      target: {
        value: 'Lastnameovici',
      },
    })

    selectOption('Miss')

    fireEvent.change(getByTestId('affiliation'), {
      target: {
        value: 'Boko Haram',
      },
    })

    clickCheckbox()
    fireEvent.click(getByText(/PROCEED TO SET EMAIL AND PASSWORD/i))

    setTimeout(() => {
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      expect(onSubmitMock).toHaveBeenCalledWith(
        {
          aff: 'Boko Haram',
          agreeTc: true,
          country: 'Romania',
          givenNames: 'Firstnameovici',
          surname: 'Lastnameovici',
          title: 'miss',
        },
        expect.anything(),
      )

      done()
    })
  })

  it('should set email and password', async done => {
    const initialValues = {
      aff: 'Boko Haram',
      agreeTc: true,
      country: 'Romania',
      givenNames: 'Firstnameovici',
      surname: 'Lastnameovici',
      title: 'miss',
    }

    const onSubmitMock = jest.fn()
    const { getByText, getByTestId } = render(
      <SignUpForm
        initialValues={initialValues}
        journal={journal}
        onSubmit={onSubmitMock}
        step={1}
      />,
    )

    fireEvent.change(getByTestId('sign-up-email'), {
      target: { value: 'nigerian.prince@gmail.com' },
    })

    fireEvent.change(getByTestId('password'), {
      target: { value: '1Password!' },
    })

    fireEvent.change(getByTestId('confirm-password'), {
      target: { value: '1Password!' },
    })

    fireEvent.click(getByText(/CREATE ACCOUNT/i))

    setTimeout(() => {
      expect(onSubmitMock).toHaveBeenCalledTimes(1)
      expect(onSubmitMock).toHaveBeenCalledWith(
        {
          aff: 'Boko Haram',
          agreeTc: true,
          confirmPassword: '1Password!',
          country: 'Romania',
          email: 'nigerian.prince@gmail.com',
          givenNames: 'Firstnameovici',
          password: '1Password!',
          surname: 'Lastnameovici',
          title: 'miss',
        },
        expect.anything(),
      )

      done()
    })
  })
})
