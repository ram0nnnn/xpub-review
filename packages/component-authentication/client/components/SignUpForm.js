import React, { Fragment } from 'react'
import { get } from 'lodash'
import { Formik } from 'formik'
import { withProps } from 'recompose'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { required } from 'xpub-validators'
import { Button, Checkbox, H2, Spinner, TextField } from '@pubsweet/ui'
import {
  Row,
  Item,
  Text,
  Label,
  Menu,
  ActionLink,
  MenuCountry,
  ShadowedBox,
  ValidatedFormField,
  PasswordValidation,
  validators,
  marginHelper,
  passwordValidator,
} from 'component-hindawi-ui'

import { BottomRow } from './'

const containerPaddings = {
  pt: 0,
  pr: 0,
  pb: 0,
  pl: 0,
}
const validateCheckbox = value => (!value ? 'Required' : undefined)
const SignUpForm = ({
  step,
  onSubmit,
  prevStep,
  isInvited,
  buttonText,
  isFetching,
  fetchingError,
  initialValues,
  journal: { titles = [] },
}) => (
  <Formik
    initialValues={initialValues}
    onSubmit={onSubmit}
    validate={step === 1 ? passwordValidator : null}
  >
    {({ handleSubmit, errors, touched, values }) => (
      <ShadowedBox center mt={5} {...containerPaddings}>
        <H2 mt={5}>{isInvited ? 'Add new account details' : 'Sign up'}</H2>
        {isInvited && <Text align="center">{get(values, 'email', '')}</Text>}

        {step === 0 && (
          <Fragment>
            <Row height={6} mt={3} pl={4} pr={4}>
              <Item mr={1} vertical>
                <Label required>First Name</Label>
                <ValidatedFormField
                  component={TextField}
                  data-test-id="first-name"
                  inline
                  name="givenNames"
                  validate={[required]}
                />
              </Item>

              <Item ml={1} vertical>
                <Label required>Last Name</Label>
                <ValidatedFormField
                  component={TextField}
                  data-test-id="last-name"
                  inline
                  name="surname"
                  validate={[required]}
                />
              </Item>
            </Row>

            <Row height={6} mt={3} pl={4} pr={4}>
              <Item mr={1} vertical>
                <Label required>Title</Label>
                <ValidatedFormField
                  component={Menu}
                  data-test-id="title-menu"
                  name="title"
                  options={titles}
                  validate={[required]}
                />
              </Item>

              <Item ml={1} vertical>
                <Label required>Country</Label>
                <ValidatedFormField
                  component={MenuCountry}
                  data-test-id="country-menu"
                  name="country"
                  validate={[required]}
                />
              </Item>
            </Row>

            <Row height={6} mt={3} pl={4} pr={4}>
              <Item vertical>
                <Label required>Affiliation</Label>
                <ValidatedFormField
                  component={TextField}
                  data-test-id="affiliation"
                  inline
                  name="aff"
                  validate={[required]}
                />
              </Item>
            </Row>

            <CheckboxRow mb={1} mt={2}>
              <ValidatedFormField
                component={CustomCheckbox}
                name="agreeTc"
                validate={[validateCheckbox]}
              />
            </CheckboxRow>

            <Row height={6} pl={4} pr={4}>
              <Text display="inline" secondary small>
                This account information will be processed by us in accordance
                with our Privacy Policy for the purpose of registering your
                account and allowing you to use the services available via the
                platform. Please read our
                <ActionLink
                  display="inline"
                  ml={1 / 2}
                  mr={1 / 2}
                  to="https://www.hindawi.com/privacy/"
                >
                  Privacy Policy
                </ActionLink>
                for further information.
              </Text>
            </Row>
          </Fragment>
        )}

        {step === 1 && (
          <Fragment>
            {!isInvited && (
              <Row height={6} mt={3} pl={4} pr={4}>
                <Item vertical>
                  <Label required>Email</Label>
                  <ValidatedFormField
                    component={TextField}
                    data-test-id="sign-up-email"
                    inline
                    name="email"
                    validate={[required, validators.emailValidator]}
                  />
                </Item>
              </Row>
            )}

            <PasswordValidation
              errors={errors}
              formValues={values}
              pl={4}
              pr={4}
              touched={touched}
            />
          </Fragment>
        )}

        <Row height={5} mt={3} pl={4} pr={4}>
          {isFetching ? (
            <Spinner />
          ) : (
            <Fragment>
              {step === 1 && (
                <Button fullWidth mr={2} onClick={prevStep} secondary>
                  BACK
                </Button>
              )}

              <Button
                fullWidth
                ml={step === 1 ? 2 : 0}
                onClick={handleSubmit}
                primary
                type="submit"
              >
                {buttonText}
              </Button>
            </Fragment>
          )}
        </Row>

        {fetchingError && (
          <Row justify="flex-start" mt={2} pl={4} pr={4}>
            <Text error>{fetchingError}</Text>
          </Row>
        )}

        <BottomRow alignItems="center" height={8} mt={3}>
          <Text display="flex" secondary>
            {`Already have an account? `}
            <ActionLink internal pl={1 / 2} to="/login">
              Sign in
            </ActionLink>
          </Text>
        </BottomRow>
      </ShadowedBox>
    )}
  </Formik>
)

export default withProps(({ isInvited, step }) => {
  let buttonText = ''

  if (step === 0) {
    buttonText = isInvited
      ? 'PROCEED TO SET PASSWORD'
      : 'PROCEED TO SET EMAIL AND PASSWORD'
  } else {
    buttonText = isInvited ? 'CONFIRM' : 'CREATE ACCOUNT'
  }

  return { buttonText }
})(SignUpForm)

const CustomCheckbox = input => (
  <RootCheckbox>
    <Checkbox checked={input.value} data-test-id="agree-checkbox" {...input} />
    <Text display="inline-flex">
      I agree with the
      <ActionLink
        display="inline"
        ml={1 / 2}
        to="https://www.hindawi.com/terms/"
      >
        Terms of Service
      </ActionLink>
    </Text>
  </RootCheckbox>
)

// #region styled-components
const RootCheckbox = styled.div.attrs(props => ({
  className: 'custom-checkbox',
}))`
  display: flex;

  + div[role='alert'] {
    margin-top: 0;
  }

  & label {
    margin-bottom: calc(${th('gridUnit')} / 2);
    & span {
      color: ${th('colorText')};
      font-family: ${th('fontReading')};
      font-size: ${th('fontSizeBase')};
    }
  }
`

const CheckboxRow = styled.div`
  padding-left: calc(${th('gridUnit')} * 4);
  width: 100%;

  ${marginHelper};
`
// #endregion
