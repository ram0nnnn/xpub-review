import React, { Fragment } from 'react'
import { Formik } from 'formik'
import { required } from 'xpub-validators'
import { Button, H2, Spinner, TextField } from '@pubsweet/ui'

import {
  Row,
  Item,
  Text,
  Label,
  ShadowedBox,
  ValidatedFormField,
  validators,
} from 'component-hindawi-ui'

const ResetPasswordForm = ({
  history,
  isFetching,
  fetchingError,
  requestResetPassword,
}) => (
  <Formik onSubmit={requestResetPassword}>
    {({ handleSubmit }) => (
      <ShadowedBox center mt={5}>
        <H2>Reset password</H2>
        <Row mt={3}>
          <Item vertical>
            <Label required>Email</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="email-reset-password"
              inline
              name="email"
              validate={[required, validators.emailValidator]}
            />
          </Item>
        </Row>

        {fetchingError && (
          <Row justify="flex-start" mt={2}>
            <Text error>{fetchingError}</Text>
          </Row>
        )}

        <Row mt={3}>
          {isFetching ? (
            <Spinner />
          ) : (
            <Fragment>
              <Button fullWidth mr={3 / 2} onClick={() => history.goBack()}>
                BACK
              </Button>
              <Button fullWidth ml={3 / 2} onClick={handleSubmit} primary>
                SEND EMAIL
              </Button>
            </Fragment>
          )}
        </Row>
      </ShadowedBox>
    )}
  </Formik>
)

export default ResetPasswordForm
