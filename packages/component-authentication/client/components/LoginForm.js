import React from 'react'
import { Formik } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Button, H2, Spinner, TextField } from '@pubsweet/ui'
import {
  Row,
  Text,
  Item,
  Label,
  ActionLink,
  PasswordField,
  ValidatedFormField,
} from 'component-hindawi-ui'
import { withHandlers, compose } from 'recompose'

import { BottomRow } from './'
import { loginValidate } from '../utils'

const LoginForm = ({ loginUser, fetchingError, isFetching, onEnter }) => (
  <Formik onSubmit={loginUser} validate={loginValidate}>
    {({ handleSubmit }) => (
      <Root>
        <H2>Login</H2>

        <Row mt={3} pl={5} pr={5}>
          <Item vertical>
            <Label required>Email</Label>
            <ValidatedFormField
              component={TextField}
              data-test-id="login-email"
              inline
              name="email"
              onKeyPress={onEnter(handleSubmit)}
            />
          </Item>
        </Row>

        <Row mt={2} pl={5} pr={5}>
          <Item vertical>
            <Label required>Password</Label>
            <ValidatedFormField
              component={PasswordField}
              data-test-id="login-password"
              inline
              name="password"
              onKeyPress={onEnter(handleSubmit)}
            />
          </Item>
        </Row>

        <Row height={1} justify="flex-end" pl={5} pr={5}>
          <ActionLink alignItems="center" internal to="/password-reset">
            Forgot your password?
          </ActionLink>
        </Row>

        <Row mt={3}>
          {isFetching ? (
            <Spinner />
          ) : (
            <Button ml={5} mr={5} onClick={handleSubmit} primary>
              LOG IN
            </Button>
          )}
        </Row>

        {fetchingError && (
          <Row height={2} justify="flex-start" mt={1} pl={5} pr={5}>
            <Text error>{fetchingError}</Text>
          </Row>
        )}

        <BottomRow alignItems="center" bgColor="#f6f6f6" height={8} mt={3}>
          <Text display="flex" secondary>
            {`Don't have an account?`}
            <ActionLink alignItems="center" internal pl={1 / 2} to="/signup">
              Sign up
            </ActionLink>
          </Text>
        </BottomRow>
      </Root>
    )}
  </Formik>
)

export default compose(
  withHandlers({
    onEnter: () => handleSubmit => event => {
      if (event.which === 13) {
        event.preventDefault()
        handleSubmit()
      }
    },
  }),
)(LoginForm)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue')};
  border-radius: ${th('gridUnit')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  margin-top: calc(${th('gridUnit')} * 10);
  padding-top: calc(${th('gridUnit')} * 5);
  width: calc(${th('gridUnit')} * 46);
`
// #endregion
