import React from 'react'
import { Formik } from 'formik'
import { Button, H2, Spinner } from '@pubsweet/ui'
import {
  Row,
  Text,
  ShadowedBox,
  PasswordValidation,
  passwordValidator,
} from 'component-hindawi-ui'

const SetNewPasswordForm = ({ isFetching, fetchingError, setPassword }) => (
  <Formik onSubmit={setPassword} validate={passwordValidator}>
    {({ handleSubmit, values, errors, touched }) => (
      <ShadowedBox center mt={5}>
        <H2>Set new password</H2>

        <PasswordValidation
          errors={errors}
          formValues={values}
          touched={touched}
        />

        {fetchingError && (
          <Row justify="flex-start" mt={1}>
            <Text error>{fetchingError}</Text>
          </Row>
        )}

        <Row mt={4}>
          {isFetching ? (
            <Spinner />
          ) : (
            <Button onClick={handleSubmit} primary>
              Confirm
            </Button>
          )}
        </Row>
      </ShadowedBox>
    )}
  </Formik>
)

export default SetNewPasswordForm
