import { compose, withHandlers } from 'recompose'
import { withFetching } from 'component-hindawi-ui'

import { LoginForm } from '../components'
import withAuthenticationGQL from '../graphql'
import { removeToken, setToken, getRedirectTo } from '../utils'

export default compose(
  withFetching,
  withAuthenticationGQL,
  withHandlers({
    loginUser: ({ history, location, setError, loginUser, setFetching }) => (
      { email, password },
      formProps,
    ) => {
      setFetching(true)
      removeToken()
      loginUser({
        variables: {
          input: {
            email,
            password,
          },
        },
      })
        .then(r => {
          setFetching(false)
          setToken(r.data.localLogin.token)
          history.push(getRedirectTo(location))
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(LoginForm)
