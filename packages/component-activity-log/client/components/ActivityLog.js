import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { isEmpty, filter, get, orderBy } from 'lodash'
import { DateParser, Menu, Icon } from '@pubsweet/ui'
import { compose, withProps, withHandlers, withState } from 'recompose'
import {
  Row,
  Item,
  Text,
  Label,
  Loader,
  ContextualBox,
} from 'component-hindawi-ui'

const filterOptions = [
  { label: 'All', value: 'ALL' },
  { label: 'Editor In Chief', value: 'editorInChief' },
  { label: 'Handling Editor', value: 'handlingEditor' },
  { label: 'Author', value: 'author' },
  { label: 'Reviewer', value: 'reviewer' },
  { label: 'Admin', value: 'admin' },
]

const ActivityLog = ({
  value,
  error,
  events,
  toggle,
  loading,
  expanded,
  eventsTime,
  errorMessage,
  changeFilterValue,
}) => (
  <ContextualBox
    expanded={expanded}
    label="Activity Log"
    mt={2}
    scrollIntoView
    toggle={toggle}
  >
    <Root>
      {error ? (
        <Row mb={1} mt={3}>
          <Item alignItems="center" justify="center">
            <Icon error size={2}>
              alert-triangle
            </Icon>
            <ErrorMessage>{errorMessage}</ErrorMessage>
          </Item>
        </Row>
      ) : (
        <Fragment>
          <Item mb={1} vertical>
            <Label>Role</Label>
            <StyledMenu
              inline
              onChange={changeFilterValue}
              options={filterOptions}
              placeholder="All"
              value={value}
            />
          </Item>
          <EnhancedActivityLogEvents
            events={events}
            loading={loading}
            value={value}
          />
        </Fragment>
      )}
    </Root>
  </ContextualBox>
)

export default compose(
  withProps(({ events }) => ({
    events: orderBy(events, 'version', 'desc'),
  })),
  withProps(({ events }) => ({
    events: events.map(item => ({
      logs: orderBy(item.logs, ['created'], ['desc']),
      version: item.version,
    })),
  })),
  withState('value', 'setFilterValues', 'ALL'),
  withHandlers({
    changeFilterValue: ({ value, setFilterValues }) => value => {
      setFilterValues(value)
    },
  }),
)(ActivityLog)

const ActivityLogEvents = ({
  events = [],
  error,
  value,
  getTag,
  loading,
  getRole,
  emptyStateMessage = 'There are no activity logs',
}) => (
  <Wrapper>
    {loading ? (
      <Loader />
    ) : (
      isEmpty(events) && (
        <Item alignItems="center" justify="center">
          <ErrorMessage>{emptyStateMessage}</ErrorMessage>
        </Item>
      )
    )}
    {events.map(({ version, logs = [] }) => (
      <Events key={version}>
        <TextVersion height="auto" labelLine>
          VERSION {version}
        </TextVersion>

        {logs.map(log => (
          <Row alignItems="center" key={log.id} pl={1}>
            <Item display="inline" mb={1} mr={2} mt={1}>
              <Text fontWeight={600} mr={1 / 2}>
                {get(log.user, 'email') || 'System'}
              </Text>
              {getRole(log.user)}
              <Text medium mr={1 / 2}>
                {log.action}
              </Text>
              <Text fontWeight={600} mr={1 / 2}>
                {get(log.target, 'email')}
              </Text>
              {getRole(log.target)}
            </Item>
            <Item flex={0} justify="flex-end" pr={2}>
              <DateParser
                dateFormat="YYYY-MM-DD hh:mm [(UTC)]"
                timestamp={log.created}
              >
                {timestamp => <Date>{timestamp}</Date>}
              </DateParser>
              <DateParser humanizeThreshold={356} timestamp={log.created}>
                {timestamp => <Date>{`\u00A0${timestamp}`}</Date>}
              </DateParser>
            </Item>
          </Row>
        ))}
      </Events>
    ))}
  </Wrapper>
)

const EnhancedActivityLogEvents = compose(
  withProps(({ events, value }) => ({
    getTag: (userRole = 'handlingEditor') => {
      const usersRoles = [
        { tag: 'SA', role: 'author' },
        { tag: 'EIC', role: 'editorInChief' },
        { tag: 'HE', role: 'handlingEditor' },
        { tag: 'Admin', role: 'admin' },
        { tag: 'Reviewer', role: 'reviewer' },
      ]
      return usersRoles.find(item => userRole.includes(item.role)).tag
    },
  })),
  withProps(({ events, value, getTag }) => ({
    getRole: item =>
      get(item, 'role') && (
        <Text mr={1 / 2}>
          (
          <Text customId fontWeight={600}>
            {getTag(item.role)}
            {` ${item.reviewerNumber || ''}`}
          </Text>
          )
        </Text>
      ),
    events: events.map(item => ({
      logs: item.logs.filter(log =>
        value === 'ALL' ? log : (get(log.user, 'role') || '').includes(value),
      ),
      version: item.version,
    })),
  })),
  withProps(({ events, value }) => ({
    events: filter(events, item => !isEmpty(item.logs)),
  })),
)(ActivityLogEvents)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  padding-left: calc(${th('gridUnit')} * 1);
  padding-top: calc(${th('gridUnit')} * 1);
  height: calc(${th('gridUnit')} * 65);
`
const Wrapper = styled.div`
  display: block;
  height: calc(${th('gridUnit')} * 56);
  overflow-x: auto;
`

const Date = styled(Text)`
  color: ${th('colorSecondary')};
  white-space: nowrap;
  font-size: ${th('fontSizeBaseMedium')};
`
const Events = styled.div`
  margin-top: calc(${th('gridUnit')} * 1);
  margin-bottom: calc(${th('gridUnit')} * 2);
`

const ErrorMessage = styled.span`
  color: ${th('colorFurnitureHue')};
  font-weight: 600;
`

const StyledMenu = styled(Menu)`
  max-width: calc(${th('gridUnit')} * 17);
  min-width: calc(${th('gridUnit')} * 17);
`
const TextVersion = styled(Text)`
  font-size: 12px;
`
// #endregion

ActivityLog.propTypes = {
  /** Array of users events. */
  events: PropTypes.arrayOf(PropTypes.object).isRequired,
  /** The state of the contextual box. If passed from a parent then the component
   * is controlled and can be expanded/collapsed remotely.
   */
  expanded: PropTypes.bool, // eslint-disable-line
  /** Callback function used to control the state of the component.
   * To be used together with the `expanded` prop.
   */
  toggle: PropTypes.func, // eslint-disable-line
  /** Error from graphQL where cannot retrive activity log events. */
  errorMessage: PropTypes.string,
}

ActivityLog.defaultProps = {
  errorMessage: 'Cannot retrive activity log events',
}
