process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { getAuditLogEventsUseCase } = require('../src/use-cases')

describe('get all audit logs for all versions of a manuscript', () => {
  it('should return the existing audit logs in the correct form', async () => {
    const mockedModels = models.build(fixtures)
    const manuscript = fixtures.generateManuscript({ status: 'submitted' })
    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'author',
    })
    const auditLog = fixtures.generateAuditLog({
      userId: teamMember.userId,
      manuscriptId: manuscript.id,
      action: 'manuscript_submitted',
      objectType: 'manuscript',
      objectId: manuscript.id,
    })
    manuscript.linkAuditLog(auditLog)
    const user = await mockedModels.User.find(teamMember.userId)
    await user.linkTeamMemberships(teamMember)
    await auditLog.linkUser(user)

    const result = await getAuditLogEventsUseCase
      .initialize(mockedModels)
      .execute(manuscript.id)

    expect(result.length).toEqual(1)
    expect(result[0]).toEqual(
      expect.objectContaining({
        logs: [
          expect.objectContaining({
            created: expect.anything(),
            id: expect.anything(),
            target: expect.any(Object),
            user: expect.objectContaining({
              email: user.identities[0].email,
              role: teamMember.team.role,
            }),
          }),
        ],
        version: 1,
      }),
    )
  })
  it('should return the target user data if the objectType is user', async () => {
    const mockedModels = models.build(fixtures)
    const manuscript = fixtures.generateManuscript({ status: 'submitted' })
    const invitedUser = fixtures.generateUser({})
    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'author',
    })
    const auditLog = fixtures.generateAuditLog({
      userId: teamMember.userId,
      manuscriptId: manuscript.id,
      action: 'reviewer_invited',
      objectType: 'user',
      objectId: invitedUser.id,
    })
    manuscript.linkAuditLog(auditLog)
    const user = await mockedModels.User.find(teamMember.userId)
    await user.linkTeamMemberships(teamMember)
    await auditLog.linkUser(user)

    const result = await getAuditLogEventsUseCase
      .initialize(mockedModels)
      .execute(manuscript.id)
    expect(result.length).toEqual(1)
    expect(result[0]).toEqual(
      expect.objectContaining({
        logs: [
          expect.objectContaining({
            created: expect.anything(),
            id: expect.anything(),
            target: expect.objectContaining({
              email: invitedUser.identities[0].email,
            }),
          }),
        ],
      }),
    )
  })
  it('should correctly map logs to versions', async () => {
    const mockedModels = models.build(fixtures)
    const manuscriptV1 = fixtures.generateManuscript({
      status: 'submitted',
      version: 1,
    })
    const manuscriptV2 = fixtures.generateManuscript({
      status: 'submitted',
      submissionId: manuscriptV1.submissionId,
      version: 2,
    })

    const teamMember1 = await dataService.createUserOnManuscript({
      manuscript: manuscriptV1,
      fixtures,
      role: 'author',
    })
    const teamMember2 = await dataService.createUserOnManuscript({
      manuscript: manuscriptV2,
      fixtures,
      role: 'author',
    })

    const auditLog1 = fixtures.generateAuditLog({
      userId: teamMember1.userId,
      manuscriptId: manuscriptV1.id,
      action: 'manuscript_submitted',
      objectType: 'manuscript',
      objectId: manuscriptV1.id,
    })

    const auditLog2 = fixtures.generateAuditLog({
      userId: teamMember2.userId,
      manuscriptId: manuscriptV2.id,
      action: 'manuscript_submitted',
      objectType: 'manuscript',
      objectId: manuscriptV2.id,
    })

    manuscriptV1.linkAuditLog(auditLog1)
    manuscriptV2.linkAuditLog(auditLog2)

    const user1 = await mockedModels.User.find(teamMember1.userId)
    await user1.linkTeamMemberships(teamMember1)
    await auditLog1.linkUser(user1)

    const user2 = await mockedModels.User.find(teamMember2.userId)
    await user2.linkTeamMemberships(teamMember2)
    await auditLog2.linkUser(user2)

    const result = await getAuditLogEventsUseCase
      .initialize(mockedModels)
      .execute(manuscriptV1.id)

    expect(result.length).toEqual(2)
    expect(result[0].version).toEqual(1)
    expect(result[0].logs).toBeDefined()
    expect(result[1].version).toEqual(2)
    expect(result[1].logs).toBeDefined()
  })
})
