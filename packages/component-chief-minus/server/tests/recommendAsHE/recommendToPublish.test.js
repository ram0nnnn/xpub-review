process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  recommendation_accept: 'recommended to Publish manuscript',
}
logEvent.objectType = { manuscript: 'manuscript' }

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  recommendToPublishAsHEUseCase,
} = require('../../src/use-cases/recommendAsHE')

const notification = {
  notifyEiCWhenHEMakesRecommendation: jest.fn(),
  notifyReviewersWhenHEMakesRecommendation: jest.fn(),
}

const jobsService = {
  deletePendingReviewersFromQueue: jest.fn(),
}
const chance = new Chance()
describe('Make recommendation to publish as HE', () => {
  it('changes the manuscript status to pending approval', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })

    const heMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'handlingEditor',
    })
    const reviewer = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'reviewer',
    })
    const reviewerTeam = fixtures.generateTeam({ role: 'reviewer' })
    reviewerTeam.members = [reviewer]

    const mockedModels = models.build(fixtures)

    await recommendToPublishAsHEUseCase
      .initialize({ notification, models: mockedModels, jobsService, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: heMember.userId,
        input: { forAuthor: chance.sentence(), forEiC: chance.sentence() },
      })

    expect(manuscript.status).toEqual(
      mockedModels.Manuscript.Statuses.pendingApproval,
    )
    expect(
      notification.notifyEiCWhenHEMakesRecommendation,
    ).toHaveBeenCalledTimes(1)
    expect(
      notification.notifyReviewersWhenHEMakesRecommendation,
    ).toHaveBeenCalledTimes(1)
  })
})
