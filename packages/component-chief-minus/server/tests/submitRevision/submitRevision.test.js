process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')

const chance = new Chance()
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  revision_submitted: 'submitted manuscript revision',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notification = {
  notificationsEditorInChief: {
    notifyEiCWhenRevisionSubmitted: jest.fn(),
  },
  invitationsReviewer: {
    notifyReviewersWhenAuthorSubmitsMajorRevision: jest.fn(),
  },
  notificationsHandlingEditor: {
    notifyHeWhenRevisionSubmitted: jest.fn(),
  },
}

const { submitRevisionUseCase } = require('../../src/use-cases/submitRevision')

describe('Submit Revision Use Case', () => {
  it('updates the status when EiC has requested revision', async () => {
    const journal = fixtures.journals[0]
    const eic = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const manuscript = await fixtures.generateManuscript({
      version: 1,
      status: 'submitted',
      submissionId: chance.guid(),
      journalId: journal.id,
    })

    const author = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'author',
    })

    await dataService.createReviewOnManuscript({
      manuscript,
      recommendation: 'revision',
      fixtures,
      teamMember: eic,
    })

    const revisionManuscript = await fixtures.generateManuscript({
      version: 2,
      status: 'draft',
      submissionId: manuscript.submissionId,
      journalId: journal.id,
    })

    await dataService.addUserOnManuscript({
      manuscript: revisionManuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'author',
      user: fixtures.users.find(u => u.id === author.userId),
    })

    await dataService.createReviewOnManuscript({
      manuscript,
      recommendation: 'responseToRevision',
      fixtures,
      teamMember: author,
    })

    const mockedModels = models.build(fixtures)

    await submitRevisionUseCase
      .initialize({
        models: mockedModels,
        notification,
        logEvent,
      })
      .execute({ submissionId: manuscript.submissionId })

    expect(revisionManuscript.status).toEqual('submitted')
  })
  it('updates the status when HE has requested revision without reviewers', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const manuscript = await fixtures.generateManuscript({
      version: 1,
      status: 'submitted',
      submissionId: chance.guid(),
      journalId: journal.id,
    })

    const author = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'author',
    })

    const handlingEditor = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'handlingEditor',
    })

    await dataService.createReviewOnManuscript({
      manuscript,
      recommendation: chance.pickone(['minor', 'major']),
      fixtures,
      teamMember: handlingEditor,
    })

    const revisionManuscript = await fixtures.generateManuscript({
      version: 2,
      status: 'draft',
      submissionId: manuscript.submissionId,
      journalId: journal.id,
    })

    await dataService.addUserOnManuscript({
      manuscript: revisionManuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'author',
      user: fixtures.users.find(u => u.id === author.userId),
    })

    await dataService.createReviewOnManuscript({
      manuscript,
      recommendation: 'responseToRevision',
      fixtures,
      teamMember: author,
    })

    const mockedModels = models.build(fixtures)

    await submitRevisionUseCase
      .initialize({
        models: mockedModels,
        notification,
        logEvent,
      })
      .execute({ submissionId: manuscript.submissionId })

    expect(revisionManuscript.status).toEqual('heAssigned')
  })
  it('updates the status when HE has requested a minor revision with reviewers', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const manuscript = await fixtures.generateManuscript({
      version: 1,
      status: 'submitted',
      submissionId: chance.guid(),
      journalId: journal.id,
    })

    const author = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'author',
    })

    const handlingEditor = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'handlingEditor',
    })

    await dataService.createReviewOnManuscript({
      manuscript,
      recommendation: 'minor',
      fixtures,
      teamMember: handlingEditor,
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        status: 'submitted',
      },
      role: 'reviewer',
    })

    const revisionManuscript = await fixtures.generateManuscript({
      version: 2,
      status: 'draft',
      submissionId: manuscript.submissionId,
      journalId: journal.id,
    })

    await dataService.addUserOnManuscript({
      manuscript: revisionManuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'author',
      user: fixtures.users.find(u => u.id === author.userId),
    })

    await dataService.createReviewOnManuscript({
      manuscript,
      recommendation: 'responseToRevision',
      fixtures,
      teamMember: author,
    })

    const mockedModels = models.build(fixtures)

    await submitRevisionUseCase
      .initialize({
        models: mockedModels,
        notification,
        logEvent,
      })
      .execute({ submissionId: manuscript.submissionId })

    expect(revisionManuscript.status).toEqual('reviewCompleted')
  })
  it('updates the status when HE has requested a major revision with reviewers', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    const manuscript = await fixtures.generateManuscript({
      version: 1,
      status: 'submitted',
      submissionId: chance.guid(),
      journalId: journal.id,
    })

    const author = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'author',
    })

    const handlingEditor = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'handlingEditor',
    })

    await dataService.createReviewOnManuscript({
      manuscript,
      recommendation: 'major',
      fixtures,
      teamMember: handlingEditor,
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        status: 'submitted',
        reviewerNumber: 1,
      },
      role: 'reviewer',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        status: 'accepted',
      },
      role: 'reviewer',
    })

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: {
        status: 'pending',
      },
      role: 'reviewer',
    })

    const revisionManuscript = await fixtures.generateManuscript({
      version: 2,
      status: 'draft',
      submissionId: manuscript.submissionId,
      journalId: journal.id,
    })

    await dataService.addUserOnManuscript({
      manuscript: revisionManuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: 'author',
      user: fixtures.users.find(u => u.id === author.userId),
    })

    await dataService.createReviewOnManuscript({
      manuscript,
      recommendation: 'responseToRevision',
      fixtures,
      teamMember: author,
    })

    const mockedModels = models.build(fixtures)

    await submitRevisionUseCase
      .initialize({
        models: mockedModels,
        notification,
        logEvent,
      })
      .execute({ submissionId: manuscript.submissionId })

    expect(revisionManuscript.status).toEqual('reviewersInvited')
    expect(manuscript.status).toEqual('olderVersion')

    const revTeam = fixtures.teams.find(
      t => t.manuscriptId === manuscript.id && t.role === 'reviewer',
    )

    expect(revTeam.members).toHaveLength(2)
  })
})
