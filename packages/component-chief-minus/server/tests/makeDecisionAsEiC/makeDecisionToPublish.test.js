process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Promise = require('bluebird')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  makeDecisionToPublishAsEiCUseCase,
} = require('../../src/use-cases/makeDecisionAsEiC')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  manuscript_accepted: 'decision is to Publish manuscript',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notification = {
  notifyEQAWhenEiCMakesDecisionToPublish: jest.fn(),
}
const sendPackage = jest.fn()
const chance = new Chance()

describe('Make decision to publish as EiC', () => {
  let journal
  let manuscript
  let editorInChiefMember
  let mockedModels

  beforeEach(async () => {
    // eslint-disable-next-line prefer-destructuring
    journal = fixtures.journals[0]
    editorInChiefMember = await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })

    manuscript = fixtures.generateManuscript({
      journalId: journal.id,
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: 'author',
    })
    mockedModels = models.build(fixtures)
  })
  it('create a new review', async () => {
    manuscript.status = mockedModels.Manuscript.Statuses.pendingApproval
    await makeDecisionToPublishAsEiCUseCase
      .initialize({ notification, models: mockedModels, sendPackage, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(manuscript.status).toEqual(mockedModels.Manuscript.Statuses.inQA)
    expect(manuscript.technicalCheckToken).toBeDefined()
    expect(
      notification.notifyEQAWhenEiCMakesDecisionToPublish,
    ).toHaveBeenCalled()
  })
  it('should send email to EQA', async () => {
    manuscript.status = mockedModels.Manuscript.Statuses.pendingApproval
    await makeDecisionToPublishAsEiCUseCase
      .initialize({ notification, models: mockedModels, sendPackage, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(
      notification.notifyEQAWhenEiCMakesDecisionToPublish,
    ).toHaveBeenCalled()
  })
  it('should send the manuscript in approved if it already passed EQA', async () => {
    manuscript.status = mockedModels.Manuscript.Statuses.pendingApproval
    manuscript.hasPassedEqa = true
    await makeDecisionToPublishAsEiCUseCase
      .initialize({ notification, models: mockedModels, sendPackage, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorInChiefMember.userId,
        content: chance.sentence(),
      })

    expect(manuscript.status).toEqual(mockedModels.Manuscript.Statuses.accepted)
  })
  it('should throw error if the manuscript is in a wrong status', async () => {
    const statuses = mockedModels.Manuscript.Statuses
    const wrongStatuses = [
      statuses.draft,
      statuses.technicalChecks,
      statuses.submitted,
      statuses.heInvited,
      statuses.heAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
      statuses.reviewCompleted,
      statuses.revisionRequested,
      statuses.accepted,
      statuses.rejected,
      statuses.inQA,
      statuses.deleted,
      statuses.published,
      statuses.withdrawn,
      statuses.withdrawalRequested,
      statuses.olderVersion,
    ]
    await Promise.each(wrongStatuses, async wrongStatus => {
      const manuscript = await fixtures.generateManuscript({
        journalId: journal.id,
        status: wrongStatus,
      })
      dataService.createUserOnManuscript({
        manuscript,
        fixtures,
        input: { isSubmitting: true, isCorresponding: false },
        role: 'author',
      })
      try {
        await makeDecisionToPublishAsEiCUseCase
          .initialize({
            notification,
            models: mockedModels,
            sendPackage,
            logEvent,
          })
          .execute({
            manuscriptId: manuscript.id,
            userId: editorInChiefMember.userId,
            content: chance.sentence(),
          })
        throw new Error(`Use-case should fail for status ${wrongStatus}`)
      } catch (err) {
        expect(err.message).toEqual(
          `Cannot publish a manuscript in the current status.`,
        )
      }
    })
  })
})
