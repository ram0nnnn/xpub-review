process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  reviewer_agreed: 'accepted invitation to review',
}
logEvent.objectType = { manuscript: 'manuscript' }
const { acceptReviewerInvitationUseCase } = require('../../src/use-cases')

const notification = {
  sendReviewerEmail: jest.fn(),
  sendHandlingEditorEmail: jest.fn(),
}

const Job = {
  cancelQueue: jest.fn(),
}

describe('Accept invitation as a reviewer', () => {
  it('accept the invitation', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'accepted' },
      role: 'editorInChief',
    })

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    manuscript.journal = journal

    const teamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending' },
      role: 'reviewer',
    })
    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const handlingEditorMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })

    const mockedModels = models.build(fixtures)
    const teamMemberId = teamMember.id
    await acceptReviewerInvitationUseCase
      .initialize({ notification, models: mockedModels, Job, logEvent })
      .execute({ teamMemberId, userId: handlingEditorMember.userId })

    expect(teamMember.status).toEqual('accepted')
    expect(teamMember.reviewerNumber).toEqual(1)
    expect(notification.sendReviewerEmail).toHaveBeenCalledTimes(1)
    expect(notification.sendHandlingEditorEmail).toHaveBeenCalledTimes(1)
  })
})
