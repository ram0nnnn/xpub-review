process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_revoked: 'revoked invitation sent to',
}
logEvent.objectType = { user: 'user' }

const { cancelReviewerInvitationUseCase } = require('../../src/use-cases')

const notification = {
  sendReviewerEmail: jest.fn(),
}

const Job = {
  cancelQueue: jest.fn(),
}

describe('Cancel reviewer invitation', () => {
  it('cancel the invitation to user', async () => {
    const journal = fixtures.journals[0]
    await dataService.createUserOnJournal({
      journal,
      fixtures,
      input: { status: 'pending' },
      role: 'editorInChief',
    })
    const reviewer = fixtures.generateUser({})
    const identity = fixtures.generateIdentity({ userId: reviewer.id })
    reviewer.assignIdentity(identity)

    const manuscript = fixtures.generateManuscript({ journalId: journal.id })
    const manuscriptTeam = fixtures.generateTeam({
      role: 'reviewer',
      manuscriptId: manuscript.id,
    })
    manuscript.teams.push(manuscriptTeam)
    manuscriptTeam.manuscript = manuscript

    const teamMember = fixtures.generateTeamMember({
      userId: reviewer.id,
      teamId: manuscriptTeam.id,
      status: 'pending',
    })
    teamMember.user = reviewer
    teamMember.team = manuscriptTeam
    teamMember.linkUser(reviewer)
    manuscriptTeam.members.push(teamMember)

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'pending', isSubmitting: true },
      role: 'author',
    })

    const handlingEditorMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      input: { status: 'accepted' },
      role: 'handlingEditor',
    })

    const mockedModels = models.build(fixtures)

    const input = {
      teamMemberId: teamMember.id,
      manuscriptId: manuscript.id,
    }

    await cancelReviewerInvitationUseCase
      .initialize({ notification, models: mockedModels, Job, logEvent })
      .execute({ input, userId: handlingEditorMember.userId })

    expect(notification.sendReviewerEmail).toHaveBeenCalledTimes(1)
    expect(manuscriptTeam.members).toHaveLength(0)
  })
})
