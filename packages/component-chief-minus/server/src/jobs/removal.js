const moment = require('moment')
const logger = require('@pubsweet/logger')
const { logEvent } = require('component-activity-log/server')
const { TeamMember, ReviewerSuggestion } = require('@pubsweet/models')

const Job = require('component-jobs')

const scheduleRemovalJob = async ({
  days,
  timeUnit,
  invitationId,
  manuscriptId,
}) => {
  const executionDate = moment()
    .add(days, timeUnit)
    .toISOString()

  const queue = `removal-${invitationId}`

  const params = {
    days,
    timeUnit,
    invitationId,
    manuscriptId,
  }

  await Job.schedule({ queue, executionDate, jobHandler, params })
}

const jobHandler = async job => {
  const { days, timeUnit, invitationId, manuscriptId } = job.data

  const teamMember = await TeamMember.find(invitationId, 'user.identities')
  const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
    queryObject: {
      email: teamMember.alias.email,
      manuscriptId,
    },
  })
  reviewerSuggestion.isInvited = false
  await reviewerSuggestion.save()

  await teamMember.updateProperties({ status: TeamMember.Statuses.expired })
  await teamMember.save()

  logEvent({
    userId: null,
    manuscriptId,
    action: logEvent.actions.reviewer_invitation_removed,
    objectType: logEvent.objectType.user,
    objectId: teamMember.user.id,
  })

  const message = `Job ${
    job.name
  }: the ${days} ${timeUnit} removal has been executed for invitation ${invitationId}`

  logger.info(message)
}
module.exports = { scheduleRemovalJob }
