const config = require('config')
const { get, forOwn } = require('lodash')
const { services } = require('helper-service')
const Email = require('@pubsweet/component-email-templating')

const unsubscribeSlug = config.get('unsubscribe.url')
const { staffEmail, name: journalName } = config.get('journal')

const acceptReviewPath = config.get('accept-review.url')
const acceptReviewNewUserPath = config.get('accept-review-new-user.url')
const declineReviewPath = config.get('decline-review.url')

const baseUrl = config.get('pubsweet-client.baseUrl')
const { getEmailCopy } = require('./emailCopy')
const { scheduleReminderJob } = require('../../jobs/reminders')
const { scheduleRemovalJob } = require('../../jobs/removal')

const daysList = config.get('reminders.reviewer.days')
const timeUnit = config.get('reminders.reviewer.timeUnit')
const removalDays = config.get('reminders.reviewer.remove')

module.exports = {
  async sendReviewInvitations({
    manuscript,
    submittingAuthor,
    invitation,
    user,
  }) {
    const { id: invitationId, created } = invitation

    const { title, abstract } = manuscript
    const authorTeam = get(manuscript, 'teams').find(t => t.role === 'author')
    const authorTeamMembers = get(authorTeam, 'members', [])

    const subjectText = `${get(manuscript, 'customId', '')}: Review invitation`

    const declineLink = services.createUrl(baseUrl, declineReviewPath, {
      invitationId,
      manuscriptId: manuscript.id,
    })

    let agreeLink = services.createUrl(baseUrl, acceptReviewPath, {
      invitationId,
      submissionId: manuscript.submissionId,
      manuscriptId: manuscript.id,
    })

    if (!get(user, 'user.identities[0].isConfirmed')) {
      agreeLink = services.createUrl(baseUrl, acceptReviewNewUserPath, {
        invitationId,
        submissionId: manuscript.submissionId,
        manuscriptId: manuscript.id,
        email: get(user, 'alias.email', ''),
        givenNames: get(user, 'alias.givenNames', ''),
        surname: get(user, 'alias.surname', ''),
        aff: get(user, 'alias.aff', ''),
        country: get(user, 'alias.country', ''),
        token: get(user, 'user.passwordResetToken'),
      })
    }

    const authorsList = authorTeamMembers.map(author => author.getName())

    const handlingEditor = manuscript.getHandlingEditor()
    const handlingEditorName = handlingEditor.getName()

    const submittingAuthorName = submittingAuthor.getName()

    const emailType = 'reviewer-invitation'
    const titleText = `A manuscript titled <strong>"${title}"</strong> by <strong>${submittingAuthorName}</strong> et al.`

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      titleText,
      expectedDate: services.getExpectedDate({
        timestamp: new Date(),
        daysExpected: 14,
      }),
    })

    const email = new Email({
      type: 'user',
      templateType: 'invitation',
      fromEmail: `${handlingEditorName} <${staffEmail}>`,
      toUser: {
        email: get(user, 'alias.email'),
        name: get(user, 'alias.surname'),
      },
      content: {
        title,
        abstract,
        agreeLink,
        declineLink,
        signatureJournal: journalName,
        signatureName: handlingEditorName,
        authorsList: authorsList.join(', '),
        subject: subjectText,
        paragraph,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: get(user, 'user.unsubscribeToken', ''),
        }),
      },
      bodyProps,
    })

    await email.sendEmail()

    forOwn(daysList, (days, order) =>
      scheduleReminderJob({
        days,
        order,
        email,
        timeUnit,
        invitationId: invitation.id,
        subject: `${get(manuscript, 'customId', '')}: Review reminder`,
        titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
        expectedDate: services.getExpectedDate({
          timestamp: new Date(created).getTime(),
          daysExpected: 0,
        }),
        manuscriptId: manuscript.id,
        userId: user.user.id,
      }),
    )

    scheduleRemovalJob({
      timeUnit,
      invitationId: invitation.id,
      days: removalDays,
      manuscriptId: manuscript.id,
    })
  },
  async notifyReviewersWhenAuthorSubmitsMajorRevision({
    submittedReviewers,
    manuscript,
    submittingAuthor,
  }) {
    const handlingEditor = manuscript.getHandlingEditor()
    const handlingEditorName = handlingEditor.getName()

    const submittingAuthorName = submittingAuthor.getName()

    const { title, abstract } = manuscript

    const authorTeam = get(manuscript, 'teams').find(t => t.role === 'author')
    const authorTeamMembers = get(authorTeam, 'members', [])
    const authorsList = authorTeamMembers.map(author => author.getName())

    submittedReviewers.forEach(reviewerMember => {
      const declineLink = services.createUrl(baseUrl, declineReviewPath, {
        invitationId: reviewerMember.id,
        manuscriptId: manuscript.id,
      })

      const agreeLink = services.createUrl(baseUrl, acceptReviewPath, {
        invitationId: reviewerMember.id,
        manuscriptId: manuscript.id,
      })

      const { paragraph, ...bodyProps } = getEmailCopy({
        emailType: 'reviewer-invitation-after-revision',
        titleText: `the manuscript titled <strong>"${title}"</strong> by <strong>${submittingAuthorName}</strong> et al.`,
        expectedDate: services.getExpectedDate({ daysExpected: 14 }),
      })

      const email = new Email({
        type: 'user',
        templateType: 'invitation',
        fromEmail: `${handlingEditorName} <${staffEmail}>`,
        toUser: {
          email: get(reviewerMember, 'alias.email'),
          name: get(reviewerMember, 'alias.surname'),
        },
        content: {
          subject: `${manuscript.customId}: Review invitation: New Version`,
          title,
          paragraph,
          authorsList: authorsList.join(', '),
          signatureName: handlingEditorName,
          unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
            id: reviewerMember.user.id,
            token: get(reviewerMember.user, 'user.unsubscribeToken', ''),
          }),
          signatureJournal: journalName,
          agreeLink,
          declineLink,
          abstract,
        },
        bodyProps,
      })

      return email.sendEmail()
    })
  },
}
