const config = require('config')

const journalName = config.get('journal.name')

const getEmailCopy = ({
  emailType,
  titleText,
  expectedDate,
  targetUserName,
}) => {
  let upperContent, manuscriptText, subText, lowerContent, paragraph
  let hasLink = true
  let resend = false
  switch (emailType) {
    case 'reviewer-invitation':
      upperContent = `${titleText}, has been submitted to ${journalName} for consideration.<div>&nbsp;</div>
        As the Academic Editor handling the manuscript, I would be delighted if you would agree to review it and let me know whether you feel it is suitable for publication.`
      manuscriptText = `If you are able to review the manuscript, I would be grateful if you could <strong>submit your
        report by ${expectedDate}</strong>.`
      subText = `The manuscript's abstract and author information is below to help you decide. Once you have agreed to review, you will be able to download the full article PDF.`
      lowerContent = `If a potential conflict of interest exists between yourself and either the authors or
        the subject of the manuscript, please decline to handle the manuscript. If a conflict
        becomes apparent during the review process, please let me know at the earliest possible
        opportunity. For more information about our conflicts of interest policies, please
        see:
        <a style="color:#007e92; text-decoration: none;" href="https://www.hindawi.com/ethics/#coi">https://www.hindawi.com/ethics/#coi</a>.`
      break
    case 'reviewer-resend-invitation':
      resend = true
      upperContent = `On ${expectedDate} I invited you to review ${titleText}, submitted to ${journalName} for consideration.<br/><br/>
        I would be grateful if you would agree to review the manuscript and let me know whether you feel
        it is suitable for publication. If you are unable to review this manuscript please decline to review. More details are available by clicking the link below.`
      lowerContent = `Thank you in advance for taking the time to consider this invitation, as it would not be possible for us to run the journal without the help of our reviewers.<br/><br/>
        I am looking forward to hearing from you.`
      break
    case 'reviewer-resend-invitation-first-reminder':
      resend = true
      upperContent = `On ${expectedDate} I invited you to review ${titleText}, submitted for possible publication in ${journalName}.<br/><br/>
        We'd be grateful if you could submit a decision on whether or not you will be able to review this manuscript using the link below.`
      lowerContent = `Thank you in advance for your help with the evaluation of this manuscript.<br/><br/>
        We look forward to hearing from you.`
      break
    case 'reviewer-resend-invitation-second-reminder':
    case 'reviewer-resend-invitation-third-reminder':
      resend = true
      upperContent = `We sent you a request to review ${titleText}; however we have not yet received your decision.
        We would appreciate it if you could visit the following link to let us know whether or not you will be able to review this manuscript:`
      lowerContent = `Please do not hesitate to contact me if you have any problems with the system.`
      break
    case 'reviewer-accepted':
      paragraph = `We are pleased to inform you that Dr. ${targetUserName} has agreed to review ${titleText}.<br/><br/>
        You should receive the report before ${expectedDate}.<br/><br/>
        If the review is delayed and you would like to send a reminder to Dr. ${targetUserName}, please visit the manuscript details page.`
      break
    case 'reviewer-declined':
      paragraph = `We regret to inform you that Dr. ${targetUserName} has declined to review ${titleText}.<br/><br/>
        Please visit the manuscript details to invite additional reviewers in order to reach a decision on the manuscript. `
      break
    case 'reviewer-thank-you':
      paragraph = `Thank you for agreeing to review ${titleText}.<br/><br/>
        You can view the full PDF file of the manuscript and post your review report using the following link.<br/>
        You will be asked to log in before being able to complete your review.`
      break
    case 'reviewer-cancel-invitation':
      paragraph = `We recently invited you to review ${titleText} for ${journalName}.<br/>
        This is to confirm that we no longer require your review.<br/><br/>
        If you have comments on the manuscript you believe I should see, please email them to ${config.get(
          'journal.staffEmail',
        )} as soon as possible.<br/><br/>
        Thank you for your time and I hope you will consider reviewing for ${journalName} in the future.`
      hasLink = false
      break
    case 'reviewer-invitation-after-revision':
      upperContent = `A new version of ${titleText}, has been submitted to ${journalName} for consideration.<div>&nbsp;</div>
      As you reviewed the previous version of this manuscript, I would be delighted if you would agree to review the new version and let me know whether you feel it is suitable for publication.`
      subText = `The manuscript's abstract and author information is below to help you decide. Once you have agreed to review, you will be able to download the full article PDF.`
      lowerContent = `If a potential conflict of interest exists between yourself and either the authors or
        the subject of the manuscript, please decline to handle the manuscript. If a conflict
        becomes apparent during the review process, please let me know at the earliest possible
        opportunity. For more information about our conflicts of interest policies, please
        see:
        <a style="color:#007e92; text-decoration: none;" href="https://www.hindawi.com/ethics/#coi">https://www.hindawi.com/ethics/#coi</a>.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    resend,
    hasLink,
    paragraph,
    subText,
    upperContent,
    lowerContent,
    manuscriptText,
    hasIntro: true,
    hasSignature: true,
  }
}

module.exports = {
  getEmailCopy,
}
