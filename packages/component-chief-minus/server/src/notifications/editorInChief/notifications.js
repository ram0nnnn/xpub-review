const config = require('config')
const { get } = require('lodash')
const { services } = require('helper-service')

const Email = require('@pubsweet/component-email-templating')

const baseUrl = config.get('pubsweet-client.baseUrl')

const { name: journalName, staffEmail } = config.get('journal')
const unsubscribeSlug = config.get('unsubscribe.url')

const { getEmailCopy } = require('./emailCopy')

module.exports = {
  async notifySAWhenEiCRequestsRevision({
    manuscript: { id, submissionId, title, customId },
    submittingAuthor,
    review,
    editorInChief,
  }) {
    const eicName = editorInChief.getName()
    const submittingAuthorName = submittingAuthor.getName()

    const authorNote = review.comments.find(comm => comm.type === 'public')
    const content = get(authorNote, 'content')

    const authorNoteText = content ? `Reason & Details: "${content}"` : ''

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'author-request-to-revision-from-eic',
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      comments: authorNoteText,
    })

    const email = new Email({
      type: 'user',
      toUser: {
        email: submittingAuthor.alias.email,
        name: submittingAuthor.alias.surname,
      },
      fromEmail: `${eicName} <${staffEmail}>`,
      content: {
        subject: `${customId}: Revision requested`,
        paragraph,
        signatureName: eicName,
        ctaText: 'MANUSCRIPT DETAILS',
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.user.id,
          token: submittingAuthor.user.unsubscribeToken,
        }),
        ctaLink: services.createUrl(baseUrl, `/details/${submissionId}/${id}`),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async notifyEiCWhenRevisionSubmitted({
    draft,
    submittingAuthor,
    editorInChief,
    submissionId,
  }) {
    const targetUserName = ` ${get(
      submittingAuthor,
      'alias.surname',
      '',
    )} ${get(submittingAuthor, 'alias.givenNames', '')}`
    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: get(draft, 'title', ''),
      emailType: 'revision-submitted',
      targetUserName,
    })
    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: get(editorInChief, 'alias.email', ''),
      },
      content: {
        subject: `${draft.customId}: Revision submitted`,
        paragraph,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${submissionId}/${draft.id}`,
        ),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async notifyEQAWhenEiCMakesDecisionToPublish({
    editorInChief,
    manuscript: { id, customId, title, submissionId },
    submittingAuthor,
  }) {
    const submittingAuthorName = submittingAuthor.getName()
    const eicName = editorInChief.getName()
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'EQA-manuscript-accepted-by-eic',
      titleText: `The manuscript titled "${title}" by ${submittingAuthorName}`,
      eicName,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${staffEmail}>`,
      toUser: {
        email: staffEmail,
        name: 'Editorial Assistant',
      },
      content: {
        subject: `${customId}: Manuscript decision finalised`,
        paragraph,
        signatureName: eicName,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: services.createUrl(baseUrl, `/details/${submissionId}/${id}`),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async notifyAuthorsWhenEiCMakesDecisionToRejectBeforePeerReview({
    authors,
    comment: { content },
    editorInChief,
    manuscript: { id, customId, title, submissionId },
  }) {
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'authors-manuscript-rejected-before-peer-review',
      comments: content,
    })
    const eicName = editorInChief.getName()

    authors.forEach(async author => {
      const email = new Email({
        type: 'user',
        toUser: {
          email: get(author, 'alias.email', ''),
          name: get(author, 'alias.surname', ''),
        },
        fromEmail: `${eicName} <${staffEmail}>`,
        content: {
          subject: `${customId}: Manuscript rejected`,
          paragraph,
          signatureName: eicName,
          signatureJournal: journalName,
          unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
            id: get(author, 'user.id', ''),
            token: get(author, 'user.unsubscribeToken', ''),
          }),
        },
        bodyProps,
      })

      return email.sendEmail()
    })
  },

  async notifyAuthorsWhenEiCMakesDecisionToRejectAfterPeerReview({
    authors,
    comment: { content },
    editorInChief,
    manuscript: { id, customId, title, submissionId },
  }) {
    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: title,
      emailType: 'authors-manuscript-rejected-after-peer-review',
      comments: content,
    })
    const eicName = editorInChief.getName()

    authors.forEach(async author => {
      const email = new Email({
        type: 'user',
        toUser: {
          email: get(author, 'alias.email', ''),
          name: get(author, 'alias.surname', ''),
        },
        fromEmail: `${eicName} <${staffEmail}>`,
        content: {
          subject: `${customId}: Manuscript rejected`,
          paragraph,
          signatureName: eicName,
          signatureJournal: journalName,
          unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
            id: get(author, 'user.id', ''),
            token: get(author, 'user.unsubscribeToken', ''),
          }),
        },
        bodyProps,
      })

      return email.sendEmail()
    })
  },

  async notifyHEWhenEICRejectsManuscript({
    manuscript: { id, title, customId, submissionId },
    editorInChief,
    handlingEditor,
    submittingAuthor,
  }) {
    const eicName = editorInChief.getName()
    const submittingAuthorName = submittingAuthor.getName()
    const { paragraph, ...bodyProps } = getEmailCopy({
      targetUserName: eicName,
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'he-manuscript-rejected-after-he-recommendation',
    })

    const email = new Email({
      type: 'user',
      toUser: {
        email: get(handlingEditor, 'alias.email', ''),
        name: get(handlingEditor, 'alias.surname', ''),
      },
      fromEmail: `${eicName} <${staffEmail}>`,
      content: {
        subject: `${customId}: Editorial decision confirmed`,
        paragraph,
        signatureName: eicName,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: services.createUrl(baseUrl, `/details/${submissionId}/${id}`),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: get(handlingEditor, 'user.id', ''),
          token: get(handlingEditor, 'user.unsubscribeToken', ''),
        }),
        signatureJournal: journalName,
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async notifyReviewersWhenEICRejectsManuscript({
    reviewers,
    editorInChief,
    manuscript: { id, customId, title, submissionId },
    submittingAuthor,
  }) {
    const submittingAuthorName = submittingAuthor.getName()
    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'reviewer-manuscript-rejected-after-eic-decision',
    })
    const eicName = editorInChief.getName()

    reviewers.forEach(async reviewer => {
      const email = new Email({
        type: 'user',
        toUser: {
          email: get(reviewer, 'alias.email', ''),
          name: get(reviewer, 'alias.surname', ''),
        },
        fromEmail: `${eicName} <${staffEmail}>`,
        content: {
          subject: `${customId}: A manuscript reviewed has been rejected.`,
          paragraph,
          signatureName: eicName,
          ctaText: 'MANUSCRIPT DETAILS',
          ctaLink: services.createUrl(
            baseUrl,
            `/details/${submissionId}/${id}`,
          ),
          unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
            id: get(reviewer, 'user.id', ''),
            token: get(reviewer, 'user.unsubscribeToken', ''),
          }),
          signatureJournal: journalName,
        },
        bodyProps,
      })

      return email.sendEmail()
    })
  },

  async notifyHEWhenEICReturnsManuscript({
    submittingAuthor,
    handlingEditor,
    editorInChief,
    manuscript: { id, customId, title, submissionId },
    comment: { content },
  }) {
    const submittingAuthorName = submittingAuthor.getName()
    const eicName = editorInChief.getName()

    const { paragraph, ...bodyProps } = getEmailCopy({
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'he-manuscript-returned-with-comments',
      comments: content,
      targetUserName: eicName,
    })

    const email = new Email({
      type: 'user',
      toUser: {
        email: get(handlingEditor, 'alias.email', ''),
        name: get(handlingEditor, 'alias.surname', ''),
      },
      fromEmail: `${eicName} <${staffEmail}>`,
      content: {
        subject: `${customId}: Editorial decision returned with comments`,
        paragraph,
        signatureName: eicName,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: services.createUrl(baseUrl, `/details/${submissionId}/${id}`),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: get(handlingEditor, 'user.id', ''),
          token: get(handlingEditor, 'user.unsubscribeToken', ''),
        }),
        signatureJournal: journalName,
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
