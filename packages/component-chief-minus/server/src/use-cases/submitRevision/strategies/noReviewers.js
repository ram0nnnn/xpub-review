module.exports = {
  execute: async ({
    draft,
    Manuscript,
    notification,
    submissionId,
    submittingAuthor,
    handlingEditor,
  }) => {
    await draft.updateStatus(Manuscript.Statuses.heAssigned)
    await draft.save()

    notification.notificationsHandlingEditor.notifyHeWhenRevisionSubmitted({
      submittingAuthor,
      draft,
      handlingEditor,
      submissionId,
    })
  },
}
