module.exports = {
  execute: async ({
    draft,
    Manuscript,
    notification,
    submittingAuthor,
    handlingEditor,
    submissionId,
    TeamMember,
    beforeDraft,
  }) => {
    const pendingReviewers = beforeDraft.getReviewersByStatus(
      TeamMember.Statuses.pending,
    )
    await Promise.all(pendingReviewers.map(async pr => pr.delete()))

    await draft.updateStatus(Manuscript.Statuses.reviewCompleted)
    await draft.saveRecursively()

    notification.notificationsHandlingEditor.notifyHeWhenRevisionSubmitted({
      submittingAuthor,
      draft,
      handlingEditor,
      submissionId,
    })
  },
}
