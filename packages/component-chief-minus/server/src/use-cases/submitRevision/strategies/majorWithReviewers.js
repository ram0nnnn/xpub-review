module.exports = {
  execute: async ({
    beforeDraft,
    Manuscript,
    TeamMember,
    draft,
    handlingEditor,
    notification,
    submittingAuthor,
    submissionId,
  }) => {
    const reviewers = beforeDraft.getReviewers()

    const pendingReviewers = beforeDraft.getReviewersByStatus(
      TeamMember.Statuses.pending,
    )
    await Promise.all(pendingReviewers.map(async pr => pr.delete()))

    const submittedReviewers = reviewers.filter(
      r => r.status === TeamMember.Statuses.submitted,
    )
    draft.addReviewers(submittedReviewers)

    await draft.updateStatus(Manuscript.Statuses.reviewersInvited)
    await draft.saveRecursively()

    notification.invitationsReviewer.notifyReviewersWhenAuthorSubmitsMajorRevision(
      {
        submittedReviewers,
        manuscript: draft,
        submittingAuthor,
      },
    )

    notification.notificationsHandlingEditor.notifyHeWhenRevisionSubmitted({
      submittingAuthor,
      draft,
      handlingEditor,
      submissionId,
    })
  },
}
