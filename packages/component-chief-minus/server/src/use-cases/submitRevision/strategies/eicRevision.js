module.exports = {
  execute: async ({
    Journal,
    Manuscript,
    draft,
    notification,
    submittingAuthor,
    submissionId,
  }) => {
    const journal = await Journal.find(draft.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()

    await draft.updateStatus(Manuscript.Statuses.submitted)
    await draft.save()

    notification.notificationsEditorInChief.notifyEiCWhenRevisionSubmitted({
      submittingAuthor,
      draft,
      editorInChief,
      submissionId,
    })
  },
}
