const { last } = require('lodash')

const noReviewers = require('./strategies/noReviewers')
const minorWithReviewers = require('./strategies/minorWithReviewers')
const majorWithReviewers = require('./strategies/majorWithReviewers')
const eicRevision = require('./strategies/eicRevision')

const initialize = ({
  notification,
  models: { Manuscript, Journal, Review, TeamMember },
  logEvent,
}) => ({
  async execute({ submissionId }) {
    const manuscripts = await Manuscript.findOrderedBy({
      queryObject: { submissionId },
      orderByField: 'version',
      order: 'asc',
      eagerLoadRelations: [
        'reviews.member.team',
        'teams.members.user.identities',
      ],
    })

    const latestManuscriptVersion = manuscripts.find(
      manuscript => manuscript.status !== Manuscript.Statuses.olderVersion,
    )

    const draft = last(manuscripts)
    const beforeDraft = manuscripts[manuscripts.length - 2]

    await beforeDraft.updateStatus(Manuscript.Statuses.olderVersion)
    await beforeDraft.save()

    const editorReview = beforeDraft.getLatestEditorReview()

    const submittingAuthor = draft.getSubmittingAuthor()

    const responseToRevision = draft.reviews.find(
      review =>
        review.recommendation === Review.Recommendations.responseToRevision,
    )

    if (responseToRevision) {
      responseToRevision.setSubmitted(new Date().toISOString())
      await responseToRevision.save()
    }

    const strategies = {
      noReviewers,
      minor: minorWithReviewers,
      major: majorWithReviewers,
      revision: eicRevision,
    }

    let strategy = editorReview.recommendation

    const reviewers = beforeDraft.getReviewers()
    if (
      reviewers.length === 0 &&
      editorReview.recommendation !== Review.Recommendations.revision
    ) {
      strategy = 'noReviewers'
    }

    const handlingEditor = beforeDraft.getHandlingEditor()

    await strategies[strategy].execute({
      draft,
      Journal,
      Manuscript,
      TeamMember,
      beforeDraft,
      notification,
      submissionId,
      handlingEditor,
      submittingAuthor,
    })

    logEvent({
      userId: submittingAuthor.userId,
      manuscriptId: latestManuscriptVersion.id,
      action: logEvent.actions.revision_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: submissionId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
