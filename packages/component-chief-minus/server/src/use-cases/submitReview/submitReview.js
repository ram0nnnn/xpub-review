const { AuthorizationError } = require('@pubsweet/errors')

const initialize = ({
  notification,
  models: { Review, Manuscript, Journal },
  logEvent,
}) => ({
  async execute({ reviewId, userId }) {
    const review = await Review.find(
      reviewId,
      '[member, manuscript.teams.members.user, comments.files]',
    )

    if (review.member.userId !== userId)
      throw new AuthorizationError('User can not update selected review')
    const hasPublicComment = review.hasPublicComment()
    if (!hasPublicComment) throw new Error('Cannot submit an empty review')

    review.updateProperties({
      submitted: new Date(),
      open: false,
    })

    review.manuscript.updateProperties({
      status: Manuscript.Statuses.reviewCompleted,
    })

    review.member.updateProperties({
      status: 'submitted',
    })

    review.stripEmptyPrivateComments()

    await review.saveRecursively()

    const reviewer = review.member
    const { manuscript } = review
    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const handlingEditor = manuscript.getHandlingEditor()
    notification.notifyHEWhenReviewerSubmitsReview({
      reviewer,
      manuscript,
      editorInChief,
      handlingEditor,
      submittingAuthor,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.review_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
