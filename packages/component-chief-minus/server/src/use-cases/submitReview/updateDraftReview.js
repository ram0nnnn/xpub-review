const { AuthorizationError } = require('@pubsweet/errors')

const initialize = ({ models: { Review } }) => ({
  async execute({ reviewId, input, userId }) {
    const review = await Review.find(reviewId, '[member, comments]')
    if (review.member.userId !== userId) {
      throw new AuthorizationError('User can not update selected review')
    }

    review.updateProperties({ recommendation: input.recommendation })

    await Promise.all(
      input.comments.map(async inputComm => {
        const comment = review.comments.find(com => com.id === inputComm.id)
        comment.updateProperties({ content: inputComm.content })
        await comment.save()
      }),
    )

    await review.saveRecursively()

    return review.toDTO()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
