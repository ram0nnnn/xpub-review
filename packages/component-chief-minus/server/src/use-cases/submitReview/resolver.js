const useCases = require('./index')
const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const notificationsHandlingEditor = require('../../notifications/handlingEditor/notifications')

const resolver = {
  Query: {},
  Mutation: {
    async updateDraftReview(_, { reviewId, input }, ctx) {
      return useCases.updateDraftReviewUseCase
        .initialize({ models })
        .execute({ reviewId, input, userId: ctx.user })
    },
    async submitReview(_, { reviewId }, ctx) {
      return useCases.submitReviewUseCase
        .initialize({
          notification: notificationsHandlingEditor,
          models,
          logEvent,
        })
        .execute({ reviewId, userId: ctx.user })
    },
  },
}

module.exports = resolver
