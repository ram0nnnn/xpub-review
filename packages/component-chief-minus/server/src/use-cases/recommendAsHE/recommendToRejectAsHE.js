const initialize = ({
  notification,
  models: { Review, Manuscript, Journal, Comment, User, Team, TeamMember },
  jobsService,
  logEvent,
}) => ({
  async execute({ manuscriptId, input, userId }) {
    const manuscript = await Manuscript.find(manuscriptId, 'teams.[members]')

    const user = await User.find(userId, 'teamMemberships.team')

    const heMember = user.teamMemberships.find(
      member =>
        member.team.role === Team.Role.handlingEditor &&
        member.team.manuscriptId === manuscriptId,
    )

    const review = new Review({
      recommendation: Review.Recommendations.reject,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: heMember.id,
    })
    await review.save()

    const authorComment = new Comment({
      content: input.forAuthor,
      reviewId: review.id,
      type: 'public',
    })
    await authorComment.save()

    if (input.forEiC) {
      const eicComment = new Comment({
        content: input.forEiC,
        reviewId: review.id,
        type: 'private',
      })
      await eicComment.save()
    }

    await manuscript.updateStatus(Manuscript.Statuses.pendingApproval)
    await manuscript.save()

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    const reviewerTeam = manuscript.teams.find(
      team => team.role === Team.Role.reviewer,
    )
    jobsService.deletePendingReviewersFromQueue({ reviewerTeam })
    let hasSubmittedReviews
    if (reviewerTeam) {
      hasSubmittedReviews = reviewerTeam.members.some(
        member => member.status === TeamMember.Statuses.submitted,
      )
    }

    if (hasSubmittedReviews) {
      notification.notifyReviewersWhenHEMakesRecommendation({
        submittingAuthor,
        handlingEditor: heMember,
        manuscript,
        reviewers: reviewerTeam.members,
      })
    }

    notification.notifyEiCWhenHEMakesRecommendation({
      review,
      editorInChief,
      manuscript,
      commentForEiC: input.forEiC,
      submittingAuthor,
      handlingEditor: heMember,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.recommendation_reject,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'handlingEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
