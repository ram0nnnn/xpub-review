const { createVersion } = require('../createVersion/createVersion')

const initialize = ({
  notification,
  models: {
    Review,
    Manuscript,
    Journal,
    Comment,
    User,
    File,
    Team,
    TeamMember,
  },
  logEvent,
  jobsService,
}) => ({
  async execute({ manuscriptId, content, type, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[teams.members.user, files, reviews]',
    )

    const user = await User.find(userId, 'teamMemberships.team')

    const heMember = user.teamMemberships.find(
      member =>
        member.team.role === Team.Role.handlingEditor &&
        member.team.manuscriptId === manuscriptId,
    )

    const review = new Review({
      recommendation: Review.Recommendations[type],
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: heMember.id,
    })
    await review.save()

    const authorComment = new Comment({
      content,
      reviewId: review.id,
      type: Comment.Types.public,
    })
    await authorComment.save()

    await manuscript.updateStatus(Manuscript.Statuses.revisionRequested)
    await manuscript.save()
    await createVersion({
      Manuscript,
      Review,
      Comment,
      File,
      Team,
      TeamMember,
      manuscript,
    })

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const reviewerTeam = manuscript.teams.find(
      t => t.role === Team.Role.reviewer,
    )
    jobsService.deletePendingReviewersFromQueue({ reviewerTeam })
    let reviewerTeamMembers = []
    if (reviewerTeam) {
      reviewerTeamMembers = reviewerTeam.members
    }
    notification.notifyEiCWhenHEMakesRecommendation({
      review,
      editorInChief,
      manuscript,
      submittingAuthor,
      handlingEditor: heMember,
    })

    notification.notifySAWhenHERequestsRevision({
      submittingAuthor,
      manuscript,
      commentForAuthor: content,
      handlingEditor: heMember,
    })

    if (review.recommendation === Review.Recommendations.minor) {
      logEvent({
        userId,
        manuscriptId,
        action: logEvent.actions.revision_requested_minor,
        objectType: logEvent.objectType.manuscript,
        objectId: manuscriptId,
      })
    }

    if (review.recommendation === Review.Recommendations.major) {
      logEvent({
        userId,
        manuscriptId,
        action: logEvent.actions.revision_requested_major,
        objectType: logEvent.objectType.manuscript,
        objectId: manuscriptId,
      })
    }

    notification.notifyReviewersWhenHEMakesRecommendation({
      submittingAuthor,
      handlingEditor: heMember,
      manuscript,
      reviewers: reviewerTeamMembers,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'handlingEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
