const initialize = ({
  notification,
  models: { Review, Manuscript, Journal, Comment, User, Team },
  logEvent,
  jobsService,
}) => ({
  async execute({ manuscriptId, input, userId }) {
    const manuscript = await Manuscript.find(manuscriptId, 'teams.[members]')
    const user = await User.find(userId, 'teamMemberships.team')

    const handlingEditor = user.teamMemberships.find(
      member =>
        member.team.role === Team.Role.handlingEditor &&
        member.team.manuscriptId === manuscriptId,
    )

    const review = new Review({
      recommendation: Review.Recommendations.publish,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: handlingEditor.id,
    })
    await review.save()

    const authorComment = new Comment({
      content: input.forAuthor,
      reviewId: review.id,
      type: 'public',
    })
    await authorComment.save()

    if (input.forEiC) {
      const eicComment = new Comment({
        content: input.forEiC,
        reviewId: review.id,
        type: 'private',
      })
      await eicComment.save()
    }

    await manuscript.updateStatus(Manuscript.Statuses.pendingApproval)
    await manuscript.save()

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const reviewerTeam = manuscript.teams.find(
      t => t.role === Team.Role.reviewer,
    )

    jobsService.deletePendingReviewersFromQueue({ reviewerTeam })

    notification.notifyEiCWhenHEMakesRecommendation({
      review,
      editorInChief,
      manuscript,
      commentForEiC: input.forEiC,
      submittingAuthor,
      handlingEditor,
    })

    notification.notifyReviewersWhenHEMakesRecommendation({
      submittingAuthor,
      handlingEditor,
      manuscript,
      reviewers: reviewerTeam.members,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.recommendation_accept,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'handlingEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
