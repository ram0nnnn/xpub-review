const uuid = require('uuid')

const initialize = ({
  notification,
  models: { Manuscript, User, Team, Review, Journal },
  sendPackage,
  logEvent,
}) => ({
  async execute({ manuscriptId, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[files,teams.members,reviews.[comments,member.team]]',
    )

    if (manuscript.status !== Manuscript.Statuses.pendingApproval) {
      throw new Error(`Cannot publish a manuscript in the current status.`)
    }

    const user = await User.find(userId, 'teamMemberships.team')
    const adminOrEiCMember = user.teamMemberships.find(member =>
      [Team.Role.editorInChief, Team.Role.admin].includes(member.team.role),
    )

    const review = new Review({
      recommendation: Review.Recommendations.publish,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: adminOrEiCMember.id,
    })
    review.assignMember(user.teamMemberships[0])
    manuscript.assignReview(review)
    if (manuscript.hasPassedEqa) {
      manuscript.updateStatus(Manuscript.Statuses.accepted)
    } else {
      manuscript.updateProperties({
        status: Manuscript.Statuses.inQA,
        technicalCheckToken: uuid.v4(),
      })
    }

    await sendPackage({ manuscript, isEQA: true })

    await manuscript.saveRecursively()

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    notification.notifyEQAWhenEiCMakesDecisionToPublish({
      editorInChief,
      submittingAuthor,
      manuscript,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_accepted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorInChief']

module.exports = {
  initialize,
  authsomePolicies,
}
