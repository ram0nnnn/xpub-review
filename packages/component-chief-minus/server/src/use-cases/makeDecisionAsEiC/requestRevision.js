const { createVersion } = require('../createVersion/createVersion')

const initialize = ({
  notification,
  fileUploadService,
  models: {
    Review,
    Manuscript,
    User,
    Team,
    Comment,
    File,
    Journal,
    TeamMember,
  },
  logEvent,
}) => ({
  async execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[teams.members.user, files, reviews]',
    )

    if (manuscript.hasHandlingEditor()) {
      throw new Error(
        'Cannot make request a revision after a Handling Editor has been assigned.',
      )
    }

    await createVersion({
      Manuscript,
      Review,
      Comment,
      File,
      Team,
      TeamMember,
      manuscript,
    })
    await manuscript.updateStatus(Manuscript.Statuses.revisionRequested)

    await manuscript.save()

    const user = await User.find(userId, 'teamMemberships.team')
    const eicTeamMember = user.teamMemberships.find(member =>
      [Team.Role.editorInChief, Team.Role.admin].includes(member.team.role),
    )

    const review = new Review({
      recommendation: Review.Recommendations.revision,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: eicTeamMember.id,
    })

    review.addComment({
      content,
      type: Comment.Types.public,
    })

    await review.saveRecursively()

    const submittingAuthor = manuscript.getSubmittingAuthor()
    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()

    notification.notifySAWhenEiCRequestsRevision({
      manuscript,
      submittingAuthor,
      review,
      editorInChief,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.revision_requested,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorInChief']

module.exports = {
  initialize,
  authsomePolicies,
}
