const initialize = ({
  notification,
  models: { Review, Manuscript, Journal, Comment, User, Team },
  logEvent,
}) => ({
  async execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[reviews.comments.files, teams.members]',
    )

    const allowedStatuses = [Manuscript.Statuses.pendingApproval]

    if (!allowedStatuses.includes(manuscript.status)) {
      throw new Error(`Cannot return a manuscript in the current status.`)
    }

    const user = await User.find(userId, 'teamMemberships.team')
    const adminOrEiCMember = user.teamMemberships.find(member =>
      [Team.Role.editorInChief, Team.Role.admin].includes(member.team.role),
    )

    const review = new Review({
      recommendation: Review.Recommendations.returnToHE,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: adminOrEiCMember.id,
    })

    const comment = review.addComment({
      content,
      type: 'public',
    })

    manuscript.assignReview(review)
    manuscript.updateStatus(Manuscript.Statuses.reviewCompleted)
    await manuscript.saveRecursively()

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = await journal.getEditorInChief()
    const handlingEditor = await manuscript.getHandlingEditor()
    const submittingAuthor = await manuscript.getSubmittingAuthor()

    notification.notifyHEWhenEICReturnsManuscript({
      submittingAuthor,
      handlingEditor,
      editorInChief,
      manuscript,
      comment,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_returned,
      objectType: logEvent.objectType.user,
      objectId: handlingEditor.userId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorInChief']

module.exports = {
  initialize,
  authsomePolicies,
}
