const { isEmpty } = require('lodash')

const initialize = ({
  notification,
  models: { Review, Manuscript, Journal, Comment, User, Team },
  logEvent,
}) => ({
  async execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[files, reviews.[comments.files,member.team], teams.members]',
    )

    const statuses = Manuscript.Statuses
    const notAllowedStatuses = [
      statuses.draft,
      statuses.deleted,
      statuses.rejected,
      statuses.inQA,
      statuses.accepted,
      statuses.published,
      statuses.withdrawn,
      statuses.withdrawalRequested,
      statuses.olderVersion,
    ]

    if (notAllowedStatuses.includes(manuscript.status)) {
      throw new Error(`Cannot reject a manuscript in the current status.`)
    }

    const user = await User.find(userId, 'teamMemberships.team')
    const adminOrEiCMember = user.teamMemberships.find(member =>
      [Team.Role.editorInChief, Team.Role.admin].includes(member.team.role),
    )

    const review = new Review({
      recommendation: Review.Recommendations.reject,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: adminOrEiCMember.id,
    })

    const comment = review.addComment({
      content,
      type: 'public',
    })
    manuscript.assignReview(review)
    manuscript.updateStatus(Manuscript.Statuses.rejected)
    await manuscript.saveRecursively()

    const authors = manuscript.getAuthors()
    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const handlingEditor = await manuscript.getHandlingEditor()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    const reviewers = await manuscript.getReviewers()
    const submittingReviewers = reviewers.filter(r => r.status === 'submitted')
    const HERecommendation = manuscript.getLatestHERecommendation()

    if (!isEmpty(submittingReviewers)) {
      notification.notifyReviewersWhenEICRejectsManuscript({
        reviewers: submittingReviewers,
        editorInChief,
        manuscript,
        submittingAuthor,
      })
      notification.notifyAuthorsWhenEiCMakesDecisionToRejectAfterPeerReview({
        authors,
        comment,
        editorInChief,
        manuscript,
      })
    } else {
      notification.notifyAuthorsWhenEiCMakesDecisionToRejectBeforePeerReview({
        authors,
        comment,
        editorInChief,
        manuscript,
      })
    }
    if (HERecommendation) {
      notification.notifyHEWhenEICRejectsManuscript({
        manuscript,
        editorInChief,
        handlingEditor,
        submittingAuthor,
      })
    }

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_rejected,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorInChief']

module.exports = {
  initialize,
  authsomePolicies,
}
