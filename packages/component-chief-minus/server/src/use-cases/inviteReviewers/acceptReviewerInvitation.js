const { get } = require('lodash')

const initialize = ({
  Job,
  notification,
  models: { TeamMember, Manuscript, Review, Comment, User },
  logEvent,
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      'team.[manuscript.[teams.[members.[user.[identities]]],journal.[teams.[members]]]]',
    )

    if (get(teamMember, 'status') === TeamMember.Statuses.expired) {
      throw new Error('Your invitation has expired')
    }

    if (get(teamMember, 'status') !== TeamMember.Statuses.pending) {
      throw new Error('User already responded to the invitation.')
    }

    // TODO check logged in user to be the one associated with the teamMember

    teamMember.updateProperties({ status: TeamMember.Statuses.accepted })

    const manuscript = get(teamMember, 'team.manuscript', {})
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const journal = get(manuscript, 'journal', {})
    const editorInChief = journal.getEditorInChief()

    const manuscriptTeams = get(teamMember, 'team.manuscript.teams')
    const reviewersTeam = manuscriptTeams.find(t => t.role === 'reviewer')
    const acceptedReviewers = reviewersTeam.members
      .filter(m => ['accepted', 'submitted'].includes(m.status))
      .sort((a, b) => (a.reviewerNumber > b.reviewerNumber ? 1 : -1))
    const responded = new Date()

    if (acceptedReviewers.length === 0) {
      teamMember.updateProperties({ reviewerNumber: 1, responded })
      manuscript.updateStatus(Manuscript.Statuses.underReview)
      await manuscript.save()
    } else {
      const reviewerNumber =
        acceptedReviewers[acceptedReviewers.length - 1].reviewerNumber + 1
      teamMember.updateProperties({ reviewerNumber, responded })
    }
    await teamMember.save()

    const user = await User.find(teamMember.userId, 'identities')
    const localIdentity = user.identities.find(i => i.type === 'local')
    localIdentity.updateProperties({
      isConfirmed: true,
    })
    await localIdentity.save()

    const draftReview = new Review({
      teamMemberId: teamMember.id,
      manuscriptId: manuscript.id,
      open: true,
    })

    draftReview.addComment({
      type: Comment.Types.public,
    })

    draftReview.addComment({
      type: Comment.Types.private,
    })

    await draftReview.saveRecursively()
    await Job.cancelQueue(`reminders-${teamMemberId}`)
    await Job.cancelQueue(`removal-${teamMemberId}`)

    notification.sendReviewerEmail({
      user: teamMember,
      manuscript,
      submittingAuthor,
    })
    notification.sendHandlingEditorEmail({
      manuscript,
      user: teamMember,
      editorInChief,
      submittingAuthor,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.reviewer_agreed,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
