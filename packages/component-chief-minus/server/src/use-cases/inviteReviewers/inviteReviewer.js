const uuid = require('uuid')

const initialize = ({
  invitation,
  models: { Team, TeamMember, Manuscript, Identity, User, ReviewerSuggestion },
  logEvent,
}) => ({
  async execute({ manuscriptId, input, reqUserId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      'teams.[members.[user.[identities]]]',
    )
    const submittingAuthor = manuscript.getSubmittingAuthor()

    const userIdentity = await Identity.findOneByField(
      'email',
      input.email,
      'user.[identities]',
    )
    let user = userIdentity ? userIdentity.user : undefined
    let identity

    if (!user) {
      user = new User({
        defaultIdentity: 'local',
        isActive: true,
        isSubscribedToEmails: true,
        confirmationToken: uuid.v4(),
        unsubscribeToken: uuid.v4(),
        passwordResetToken: uuid.v4(),
        agreeTc: true,
      })

      identity = new Identity({
        type: 'local',
        isConfirmed: false,
        passwordHash: null,
        givenNames: input.givenNames,
        surname: input.surname,
        email: input.email,
        aff: input.aff || '',
        userId: user.id,
        country: input.country || '',
      })
      await user.assignIdentity(identity)
      await user.saveRecursively()
    }

    let manuscriptReviewersTeam = manuscript.teams.find(
      team => team.role === 'reviewer',
    )

    let teamMember
    if (!manuscriptReviewersTeam) {
      manuscriptReviewersTeam = new Team({
        manuscriptId,
        role: 'reviewer',
      })
    } else {
      teamMember = manuscriptReviewersTeam.members.find(
        reviewer => reviewer.userId === user.id,
      )
    }
    await manuscript.updateStatus(Manuscript.Statuses.reviewersInvited)
    await manuscript.save()

    if (teamMember && teamMember.status === TeamMember.Statuses.expired) {
      await teamMember.updateProperties({ status: TeamMember.Statuses.pending })
      await teamMember.save()
    } else {
      teamMember = manuscriptReviewersTeam.addMember(user, {
        alias: input,
      })
      await manuscriptReviewersTeam.saveRecursively()
    }

    const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
      queryObject: {
        email: input.email,
        manuscriptId,
      },
    })
    if (reviewerSuggestion) {
      reviewerSuggestion.isInvited = true
      await reviewerSuggestion.save()
    }

    logEvent({
      userId: reqUserId,
      manuscriptId,
      action: logEvent.actions.reviewer_invited,
      objectType: logEvent.objectType.user,
      objectId: teamMember.user.id,
    })

    invitation.sendReviewInvitations({
      manuscript,
      invitation: teamMember,
      submittingAuthor,
      user: teamMember,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'handlingEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
