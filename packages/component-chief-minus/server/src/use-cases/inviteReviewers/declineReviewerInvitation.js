const { get } = require('lodash')

const initialize = ({
  Job,
  notification,
  models: { TeamMember, Team, Manuscript, Journal },
  logEvent,
}) => ({
  async execute({ teamMemberId, userId }) {
    let teamMember
    try {
      teamMember = await TeamMember.find(
        teamMemberId,
        'team.[manuscript.[teams.[members.[user.[identities]]]]]',
      )
    } catch (e) {
      throw new Error('Something went wrong.')
    }

    if (get(teamMember, 'status') !== 'pending') {
      throw new Error('User already responded to the invitation.')
    }
    teamMember.updateProperties({ status: 'declined' })
    await teamMember.save()

    const manuscript = get(teamMember, 'team.manuscript')
    if (!manuscript) {
      throw new Error('Manuscript does not exists.')
    }
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()

    const manuscriptReviewersTeam = await Team.findOneBy({
      queryObject: { manuscriptId: manuscript.id, role: 'reviewer' },
      eagerLoadRelations: 'members',
    })
    const acceptedReviewersTeam = manuscriptReviewersTeam.members.filter(
      member => member.status === 'accepted',
    )
    if (!acceptedReviewersTeam) {
      await manuscript.updateStatus(Manuscript.Statuses.reviewersInvited)
      await manuscript.save()
    }

    await Job.cancelQueue(`reminders-${teamMemberId}`)
    await Job.cancelQueue(`removal-${teamMemberId}`)

    notification.sendHandlingEditorEmail({
      manuscript,
      user: teamMember,
      isAccepted: false,
      editorInChief,
      submittingAuthor,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.reviewer_declined,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
