const { get } = require('lodash')

const initialize = ({
  notification,
  models: { TeamMember, Journal, User, Manuscript, Team },
  logEvent,
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      'team.[members,manuscript.[teams.[members.[user.[identities]]]]]',
    )

    const manuscript = get(teamMember, 'team.manuscript')
    const submissionId = get(teamMember, 'team.manuscript.submissionId')

    const manuscripts = await Manuscript.findBy(
      { submissionId },
      'teams.members',
    )

    await Promise.all(
      manuscripts.map(async manuscript => {
        const heTeam = manuscript.teams.find(
          team => team.role === Team.Role.handlingEditor,
        )
        await heTeam.delete()
      }),
    )
    if (!manuscript) {
      throw new Error('Manuscript does not exists.')
    }

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()

    const submittingAuthor = manuscript.getSubmittingAuthor()

    await manuscript.updateStatus(Manuscript.Statuses.submitted)
    await manuscript.save()

    notification.sendInvitedHEEmail({
      user: teamMember,
      manuscript,
      submittingAuthor,
      editorInChief,
      isCanceled: true,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_revoked,
      objectType: logEvent.objectType.user,
      objectId: teamMember.userId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorInChief']

module.exports = {
  initialize,
  authsomePolicies,
}
