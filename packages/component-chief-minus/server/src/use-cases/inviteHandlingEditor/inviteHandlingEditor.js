const { isEmpty } = require('lodash')

const initialize = ({
  notification,
  models: { Team, Manuscript, User, Journal },
  logEvent,
}) => ({
  async execute({ submissionId, userId, reqUserId }) {
    const manuscripts = await Manuscript.findBy(
      { submissionId },
      'teams.[members.[user.[identities]]]',
    )

    if (isEmpty(manuscripts)) {
      throw new Error('No manuscript has been found.')
    }

    const user = await User.find(userId, 'identities')
    const submittingAuthor = manuscripts[0].getSubmittingAuthor()

    const journal = await Journal.find(
      manuscripts[0].journalId,
      'teams.members',
    )
    const editorInChief = journal.getEditorInChief()

    let teamMember
    await Promise.all(
      manuscripts.map(async manuscript => {
        let heTeam = manuscript.teams.find(
          team => team.role === Team.Role.handlingEditor,
        )

        if (!heTeam) {
          heTeam = new Team({
            manuscriptId: manuscript.id,
            role: Team.Role.handlingEditor,
          })
          manuscript.teams.push(heTeam)
        }

        teamMember = heTeam.addMember(user)
        await manuscript.saveRecursively()
      }),
    )

    const manuscriptLastVersion = manuscripts.find(
      m => m.version === manuscripts.length,
    )
    await manuscriptLastVersion.updateStatus(Manuscript.Statuses.heInvited)
    await manuscriptLastVersion.save()

    notification.sendInvitedHEEmail({
      user: teamMember,
      manuscript: manuscriptLastVersion,
      editorInChief,
      submittingAuthor,
    })

    logEvent({
      userId: reqUserId,
      manuscriptId: manuscriptLastVersion.id,
      action: logEvent.actions.invitation_sent,
      objectType: logEvent.objectType.user,
      objectId: userId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorInChief']

module.exports = {
  initialize,
  authsomePolicies,
}
