const { get } = require('lodash')
const Promise = require('bluebird')

const initialize = ({
  notification,
  models: { TeamMember, Journal, Manuscript, Team },
  logEvent,
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      'team.[members, manuscript.teams.members]',
    )

    const manuscript = get(teamMember, 'team.manuscript')
    if (!manuscript) {
      throw new Error('Manuscript does not exists.')
    }

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()
    await manuscript.updateStatus(Manuscript.Statuses.submitted)
    await manuscript.save()

    const reviewersTeam = manuscript.teams.find(
      rt => rt.role === Team.Role.reviewer,
    )

    let reviewers = []
    if (reviewersTeam) {
      reviewers = reviewersTeam.members
      await reviewersTeam.delete()
    }

    const allVersionsOfManuscript = await Manuscript.findBy(
      { submissionId: manuscript.submissionId },
      'teams.members',
    )

    const allHEteams = allVersionsOfManuscript.map(manuscript =>
      manuscript.teams.find(t => t.role === Team.Role.handlingEditor),
    )

    await Promise.each(allHEteams, team => team.delete())

    const draftManuscript = allVersionsOfManuscript.find(
      m => m.status === Manuscript.Statuses.draft,
    )

    if (draftManuscript) {
      await draftManuscript.delete()
    }

    notification.notifyAuthorWhenHERemoved({
      manuscript,
      submittingAuthor,
      editorInChief,
    })

    notification.notifyInvitedHEWhenRemoved({
      user: teamMember,
      manuscript,
      editorInChief,
    })

    notification.notifyReviewersWhenHERemoved({
      reviewers,
      manuscript,
      editorInChief,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.he_removed,
      objectType: logEvent.objectType.user,
      objectId: teamMember.userId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorInChief']

module.exports = {
  initialize,
  authsomePolicies,
}
