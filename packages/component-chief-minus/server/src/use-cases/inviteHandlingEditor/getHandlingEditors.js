const initialize = ({ Team }) => ({
  async execute(manuscriptId) {
    const globalHeTeam = await Team.findOneBy({
      queryObject: {
        role: 'handlingEditor',
        manuscriptId: null,
      },
      eagerLoadRelations: 'members.[user.[identities]]',
    })

    if (!globalHeTeam) {
      return []
    }

    const globalMembers = globalHeTeam.members
    const manuscriptHeTeam = await Team.findOneBy({
      queryObject: {
        role: 'handlingEditor',
        manuscriptId,
      },
      eagerLoadRelations: 'members',
    })

    if (!manuscriptHeTeam) {
      return globalMembers.map(m => m.toDTO())
    }

    const declinedMemberIds = manuscriptHeTeam.members
      .filter(member => member.status === 'declined')
      .map(m => m.userId)

    if (declinedMemberIds.length > 0) {
      return globalMembers
        .filter(gmember => !declinedMemberIds.includes(gmember.userId))
        .map(m => m.toDTO())
    }

    return globalMembers.map(m => m.toDTO())
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorInChief']

module.exports = {
  initialize,
  authsomePolicies,
}
