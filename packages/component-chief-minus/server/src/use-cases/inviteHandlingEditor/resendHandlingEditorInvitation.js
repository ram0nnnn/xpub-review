const initialize = ({
  notification,
  models: { User, TeamMember, Journal },
  logEvent,
}) => ({
  async execute({ teamMemberId, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      'team.[manuscript.[teams.[members.[user.[identities]]]]]',
    )

    const { manuscript } = teamMember.team

    const submittingAuthor = manuscript.getSubmittingAuthor()

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()

    notification.sendInvitedHEEmail({
      user: teamMember,
      manuscript,
      submittingAuthor,
      editorInChief,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_resent,
      objectType: logEvent.objectType.user,
      objectId: teamMember.userId,
    })
  },
})

const authsomePolicies = ['authenticatedUser', 'adminOrEditorInChief']

module.exports = {
  initialize,
  authsomePolicies,
}
