const { get } = require('lodash')

const initialize = ({
  notification,
  models: { TeamMember, Journal, Manuscript, Team },
  logEvent,
}) => ({
  async execute({ input: { teamMemberId, reason }, userId }) {
    const teamMember = await TeamMember.find(
      teamMemberId,
      'team.[manuscript.[teams.[members.[user.[identities]]]]]',
    )
    teamMember.updateProperties({ status: 'declined' })

    const manuscript = get(teamMember, 'team.manuscript')
    const submissionId = get(teamMember, 'team.manuscript.submissionId')

    const manuscripts = await Manuscript.findBy(
      { submissionId },
      'teams.members',
    )

    await Promise.all(
      manuscripts.map(async manuscript => {
        const heTeam = manuscript.teams.find(
          team => team.role === Team.Role.handlingEditor,
        )
        const heMember = heTeam.members[0]
        heMember.updateProperties({ status: 'declined' })
        await heMember.save()
      }),
    )
    if (!manuscript) {
      throw new Error('Manuscript does not exists.')
    }

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorInChief = journal.getEditorInChief()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    await manuscript.updateStatus(Manuscript.Statuses.submitted)
    await manuscript.save()

    notification.notifyEiCAboutHEDecisionEmail({
      reason,
      user: teamMember,
      manuscript,
      submittingAuthor,
      editorInChief,
      isAccepted: false,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_declined,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
