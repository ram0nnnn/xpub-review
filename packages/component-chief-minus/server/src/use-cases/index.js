const inviteHandlingEditorUseCases = require('./inviteHandlingEditor')

const makeDecisionAsEiCUseCases = require('./makeDecisionAsEiC')
const recommendAsHEUseCases = require('./recommendAsHE')

const inviteReviewersUseCases = require('./inviteReviewers')
const submitRevisionUseCase = require('./submitRevision')
const submitReviewUseCases = require('./submitReview')

module.exports = {
  ...inviteHandlingEditorUseCases,
  ...submitRevisionUseCase,
  ...inviteReviewersUseCases,
  ...makeDecisionAsEiCUseCases,
  ...recommendAsHEUseCases,
  ...submitReviewUseCases,
}
