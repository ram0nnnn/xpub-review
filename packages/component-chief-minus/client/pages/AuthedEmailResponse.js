import { withFetching } from 'component-hindawi-ui'
import { compose, lifecycle, withHandlers } from 'recompose'

import { parseError } from '../utils'
import { EmailResponseLayout } from '../components'
import { withReviewerGQL } from '../graphql/withGQL'

export default compose(
  withFetching,
  withReviewerGQL,
  withHandlers({
    acceptReviewerInvitation: ({
      history,
      setError,
      setFetching,
      acceptReviewerInvitation,
      params: { invitationId, manuscriptId, submissionId },
    }) => () => {
      setFetching(true)
      acceptReviewerInvitation({
        variables: {
          teamMemberId: invitationId,
        },
      })
        .then(r => {
          setFetching(false)
          history.replace(`/details/${submissionId}/${manuscriptId}`)
        })
        .catch(e => {
          setError(parseError(e))
          setFetching(false)
        })
    },
  }),
  lifecycle({
    componentDidMount() {
      const { action, acceptReviewerInvitation } = this.props
      switch (action) {
        case 'accept-review':
          acceptReviewerInvitation()
          break
        default:
          break
      }
    },
  }),
)(EmailResponseLayout)
