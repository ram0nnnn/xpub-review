import React, { Fragment } from 'react'
import { get } from 'lodash'
import { compose, withHandlers, withProps } from 'recompose'
import { withJournal } from 'xpub-journal'
import { CommentWithFile } from 'component-hindawi-ui'
import { withAuthorGQL } from '../graphql'
import { EditorialComments, SubmitRevision, AuthorReviews } from '../components'
import useVisibleStatuses from '../useVisibleStatuses'

const ManuscriptPageAuthor = ({
  match,
  journal,
  decisions,
  editAuthor,
  manuscript,
  currentUser,
  removeAuthor,
  revisionDraft,
  updateAutosave,
  isLatestVersion,
  reviewerReports,
  editorialReviews,
  getFileSignedUrl,
  authorResponseProps,
  updateDraftRevision,
  addFileToManuscript,
  updateManuscriptFile,
  addAuthorToManuscript,
  submitRevisionAsAuthor,
  removeFileFromManuscript,
  invitationsWithReviewers,
  editorialCommentsDecisions,
}) => {
  const {
    AuthorReviewsBox,
    SubmitRevisionBox,
    CommentWithFileBox,
    EditorialCommentsBox,
  } = useVisibleStatuses({
    manuscript,
    revisionDraft,
    reviewerReports,
    isLatestVersion,
    editorialReviews,
  })
  return (
    <Fragment>
      <EditorialComments
        decisions={editorialCommentsDecisions}
        editorialReviews={editorialReviews}
        isVisible={EditorialCommentsBox.isVisible}
        mt={2}
        startExpanded={EditorialCommentsBox.startExpanded}
      />
      <AuthorReviews
        currentUser={currentUser}
        invitations={invitationsWithReviewers}
        isVisible={AuthorReviewsBox.isVisible}
        journal={journal}
        reviewerReports={reviewerReports}
      />
      <SubmitRevision
        addAuthorToManuscript={addAuthorToManuscript}
        addFileToManuscript={addFileToManuscript}
        editAuthor={editAuthor}
        getFileSignedUrl={getFileSignedUrl}
        highlight={SubmitRevisionBox.isHighlighed}
        isVisible={SubmitRevisionBox.isVisible}
        match={match}
        mt={2}
        onSubmit={submitRevisionAsAuthor}
        removeAuthor={removeAuthor}
        removeFileFromManuscript={removeFileFromManuscript}
        revisionDraft={revisionDraft}
        updateAutosave={updateAutosave}
        updateDraftRevision={updateDraftRevision}
        updateManuscriptFile={updateManuscriptFile}
      />
      <CommentWithFile
        {...authorResponseProps}
        commentLabel="Your Reply"
        isVisible={CommentWithFileBox.isVisible}
        mt={2}
        startExpanded={CommentWithFileBox.startExpanded}
      />
    </Fragment>
  )
}

export default compose(
  withAuthorGQL,
  withJournal,
  withProps(({ data }) => ({
    revisionDraft: get(data, 'getDraftRevision'),
  })),

  withHandlers({
    submitRevisionAsAuthor: ({
      match,
      history,
      submitRevision,
      revisionDraft,
    }) => (values, modalProps) => {
      modalProps.setFetching(true)

      submitRevision({
        variables: {
          submissionId: get(match, 'params.submissionId', ''),
        },
      })
        .then(() => {
          const path = `/details/${get(match, 'params.submissionId')}/${get(
            revisionDraft,
            'id',
          )}`

          modalProps.setFetching(false)
          modalProps.hideModal()
          history.push(path)
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
  }),
)(ManuscriptPageAuthor)
