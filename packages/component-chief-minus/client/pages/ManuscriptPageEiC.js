import React, { Fragment } from 'react'
import { get } from 'lodash'
import { EICDecision, CommentWithFile } from 'component-hindawi-ui'
import { compose, withHandlers, withProps } from 'recompose'

import {
  EditorInChiefAssignHE,
  EditorialComments,
  ReviewerDetailsEIC,
} from '../components'
import { withEditorInChiefGQL } from '../graphql/withGQL'
import useVisibleStatuses from '../useVisibleStatuses'

const ManuscriptPageEiC = ({
  status,
  decisions,
  reviewers,
  manuscript,
  toggleAssignHE,
  recommendations,
  reviewerReports,
  editorialReviews,
  assignHEExpanded,
  authorResponseProps,
  inviteHandlingEditor,
  handleEditorialDecision,
  editorialCommentsDecisions,
  ...rest
}) => {
  const {
    EICDecisionBox,
    CommentWithFileBox,
    EditorialCommentsBox,
    ReviewerDetailsEICBox,
    EditorInChiefAssignHEBox,
  } = useVisibleStatuses({
    manuscript,
    reviewerReports,
    editorialReviews,
  })

  return (
    <Fragment>
      <CommentWithFile
        {...authorResponseProps}
        commentLabel="Author Reply"
        isVisible={CommentWithFileBox.isVisible}
        mt={2}
        startExpanded={CommentWithFileBox.startExpanded}
      />
      <EditorialComments
        decisions={editorialCommentsDecisions}
        editorialReviews={editorialReviews}
        isVisible={EditorialCommentsBox.isVisible}
        mt={2}
        startExpanded={EditorialCommentsBox.startExpanded}
      />
      <EditorInChiefAssignHE
        expanded={assignHEExpanded}
        inviteHandlingEditor={inviteHandlingEditor}
        isVisible={EditorInChiefAssignHEBox.isVisible}
        manuscript={manuscript}
        toggle={toggleAssignHE}
      />
      <ReviewerDetailsEIC
        isVisible={ReviewerDetailsEICBox.isVisible}
        manuscript={manuscript}
        mt={2}
        options={recommendations}
        reviewerReports={reviewerReports}
        reviewers={reviewers}
        startExpanded={ReviewerDetailsEICBox.startExpanded}
      />
      <EICDecision
        highlight={EICDecisionBox.isHighlighed}
        isVisible={EICDecisionBox.isVisible}
        mt={2}
        onSubmit={handleEditorialDecision}
        options={decisions}
        status={status}
      />
    </Fragment>
  )
}

export default compose(
  withEditorInChiefGQL,

  withProps(({ manuscript }) => ({
    status: get(manuscript, 'status'),
  })),

  withHandlers({
    inviteHandlingEditor: ({ manuscript, inviteHandlingEditor }) => (
      he,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      inviteHandlingEditor({
        variables: {
          submissionId: manuscript.submissionId,
          userId: he.user.id,
        },
      })
        .then(() => {
          modalProps.setFetching(true)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    publishAsEiC: ({ manuscript, makeDecisionToPublishAsEiC }) => (
      content,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      makeDecisionToPublishAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    rejectAsEiC: ({ manuscript, makeDecisionToRejectAsEiC }) => (
      content,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      makeDecisionToRejectAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    returnToHEAsEiC: ({ manuscript, makeDecisionToReturnAsEiC }) => (
      content,
      modalProps,
    ) => {
      modalProps.setFetching(true)
      makeDecisionToReturnAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
    requestRevisionAsEiC: ({
      manuscript,
      requestRevisionAsEiC,
      collapseEicDecision,
    }) => (content, modalProps, formikBag) => {
      modalProps.setFetching(true)

      requestRevisionAsEiC({
        variables: {
          manuscriptId: manuscript.id,
          content,
        },
      })
        .then(() => {
          formikBag.resetForm({ decision: '', message: '' })
          modalProps.setFetching(false)
          modalProps.hideModal()
          collapseEicDecision()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
  }),
  withHandlers({
    handleEditorialDecision: ({
      rejectAsEiC,
      publishAsEiC,
      returnToHEAsEiC,
      requestRevisionAsEiC,
    }) => ({ decision, message }, modalProps, formikBag) => {
      switch (decision) {
        case 'publish':
          publishAsEiC(message, modalProps, formikBag)
          break
        case 'return-to-handling-editor':
          returnToHEAsEiC(message, modalProps, formikBag)
          break
        case 'revision':
          requestRevisionAsEiC(message, modalProps, formikBag)
          break
        case 'reject':
          rejectAsEiC(message, modalProps, formikBag)
          break
        default:
          break
      }
    },
  }),
)(ManuscriptPageEiC)
