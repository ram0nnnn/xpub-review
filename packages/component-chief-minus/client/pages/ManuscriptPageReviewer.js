import React, { Fragment } from 'react'
import { chain, get, isEmpty } from 'lodash'
import { withRouter } from 'react-router'
import { compose, withProps, withHandlers } from 'recompose'
import {
  RespondToReviewerInvitation,
  CommentWithFile,
  ReviewerReportBox,
} from 'component-hindawi-ui'

import { EditorialComments, ReviewerSubmitReport } from '../components'
import { withReviewerGQL } from '../graphql'
import { autosaveReviewForm } from '../utils'
import useVisibleStatuses from '../useVisibleStatuses'

const ManuscriptPageReviewer = ({
  manuscript,
  currentUser,
  submitReview,
  reviewerReport,
  isLatestVersion,
  reviewerReports,
  recommendations,
  editorialReviews,
  updateDraftReview,
  autosaveReviewForm,
  respondToInvitation,
  authorResponseProps,
  editorialCommentsDecisions,
}) => {
  const {
    CommentWithFileBox,
    EditorialCommentsBox,
    ReviewerSubmitReportBox,
    ReviewerReportContextualBox,
    RespondToReviewerInvitationBox,
  } = useVisibleStatuses({
    manuscript,
    currentUser,
    isLatestVersion,
    reviewerReports,
    editorialReviews,
  })
  return (
    <Fragment>
      <CommentWithFile
        {...authorResponseProps}
        commentLabel="Author Reply"
        isVisible={CommentWithFileBox.isVisible}
        mt={2}
        startExpanded={CommentWithFileBox.startExpanded}
      />

      <EditorialComments
        decisions={editorialCommentsDecisions}
        editorialReviews={editorialReviews}
        isVisible={EditorialCommentsBox.isVisible}
        mt={2}
        startExpanded={EditorialCommentsBox.startExpanded}
      />

      <RespondToReviewerInvitation
        highlight={RespondToReviewerInvitationBox.isHighlighed}
        isVisible={RespondToReviewerInvitationBox.isVisible}
        mt={2}
        onSubmit={respondToInvitation}
        startExpanded={RespondToReviewerInvitationBox.startExpanded}
      />

      <ReviewerSubmitReport
        autosaveReviewForm={autosaveReviewForm}
        highlight={ReviewerSubmitReportBox.isHighlighed}
        isVisible={ReviewerSubmitReportBox.isVisible}
        onSubmit={submitReview}
        options={recommendations}
        reviewerReport={reviewerReport}
        updateDraftReview={updateDraftReview}
      />
      <ReviewerReportBox
        isLatestVersion={isLatestVersion}
        isVisible={ReviewerReportContextualBox.isVisible}
        options={recommendations}
        reviewerReports={reviewerReports}
        startExpanded={ReviewerReportContextualBox.startExpanded}
      />
    </Fragment>
  )
}

const parseError = e => e.message.split(':')[1].trim()
export default compose(
  withRouter,
  withReviewerGQL,
  withProps(({ reviewerReports, currentUser }) => ({
    reviewerReport: chain(reviewerReports)
      .filter(review => get(review, 'member.user.id') === currentUser.id)
      .map(review => !isEmpty(review) && { ...review, currentUserReview: true })
      .first()
      .value(),
  })),
  withHandlers({
    respondToInvitation: ({
      history,
      manuscript,
      currentUser,
      acceptReviewerInvitation,
      declineReviewerInvitation,
      ...rest
    }) => ({ reviewerDecision }, modalProps) => {
      const reviewerInvitation = manuscript.reviewers.find(
        r => r.user.id === currentUser.id,
      )
      const teamMemberId = get(reviewerInvitation, 'id', '')
      modalProps.setFetching(true)
      if (reviewerDecision === 'yes') {
        acceptReviewerInvitation({
          variables: {
            teamMemberId,
          },
        })
          .then(() => {
            modalProps.setFetching(false)
            modalProps.hideModal()
          })
          .catch(e => {
            modalProps.setFetching(false)
            modalProps.setError(parseError(e))
          })
      } else {
        declineReviewerInvitation({
          variables: {
            teamMemberId,
          },
        })
          .then(() => {
            modalProps.setFetching(false)
            modalProps.hideModal()
            history.replace('/')
          })
          .catch(e => {
            modalProps.setFetching(false)
            modalProps.setError(e.message)
          })
      }
    },
    submitReview: ({ submitReview }) => (values, modalProps) => {
      modalProps.setFetching(true)
      submitReview({
        variables: {
          reviewId: values.reviewId,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(parseError(e.message))
        })
    },
    autosaveReviewForm: ({
      updateDraftReview,
      updateAutosaveReview,
    }) => values => {
      autosaveReviewForm({
        values,
        updateDraftReview,
        updateAutosaveReview,
      })
    },
  }),
)(ManuscriptPageReviewer)
