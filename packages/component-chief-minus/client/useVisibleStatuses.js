import { get, chain, isEmpty } from 'lodash'
import { useJournal } from 'component-journal-info'

const useVisibleStatuses = ({
  currentUser,
  manuscript = {},
  revisionDraft = false,
  editorialReviews = [],
  isLatestVersion,
  reviewerReports = [],
}) => {
  const { parsedStatuses: statuses } = useJournal()
  const status = get(manuscript, 'status')
  const role = get(manuscript, 'role')
  const reviewers = get(manuscript, 'reviewers') || []
  const reviews = get(manuscript, 'reviews') || []
  const handlingEditor = get(manuscript, 'handlingEditor')
  const currentUserId = get(currentUser, 'id')
  const version = get(manuscript, 'version')
  const authorResponse = get(manuscript, 'reviews', []).find(
    r => get(r, 'recommendation') === 'responseToRevision',
  )

  const AuthorReviews = {
    author: {
      isVisible: status === statuses.revisionRequested || !isLatestVersion,
    },
  }
  const CommentWithFile = {
    author: {
      isVisible: authorResponse && status !== statuses.revisionRequested,
      startExpanded: !isEmpty(authorResponse),
    },
    admin: {
      isVisible: authorResponse && status !== statuses.revisionRequested,
      startExpanded: !isEmpty(authorResponse),
    },
    editorInChief: {
      isVisible: authorResponse && status !== statuses.revisionRequested,
      startExpanded: !isEmpty(authorResponse),
    },
    handlingEditor: {
      isVisible: version > 1 && get(handlingEditor, 'status') === 'accepted',
      startExpanded: !isEmpty(authorResponse),
    },
    reviewer: {
      isVisible: authorResponse,
      startExpanded: !isEmpty(authorResponse),
    },
  }

  const EditorialComments = {
    author: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
    admin: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
    editorInChief: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
    handlingEditor: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
    reviewer: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
  }

  const EditorInChiefAssignHE = {
    admin: {
      isVisible: status === statuses.submitted,
    },
    editorInChief: {
      isVisible: status === statuses.submitted,
    },
  }

  const EICDecision = {
    admin: {
      isVisible: ![
        statuses.technicalChecks,
        statuses.rejected,
        statuses.inQA,
        statuses.accepted,
        statuses.olderVersion,
      ].includes(status),
      isHighlighed: false,
    },
    editorInChief: {
      isVisible: ![
        statuses.rejected,
        statuses.inQA,
        statuses.accepted,
        statuses.olderVersion,
      ].includes(status),
      isHighlighed: [statuses.pendingApproval].includes(status),
    },
  }

  const HERecommendation = {
    handlingEditor: {
      isVisible: [statuses.heAssigned, statuses.reviewCompleted].includes(
        status,
      ),
      highlight: [statuses.reviewCompleted].includes(status),
    },
  }

  const RespondToEditorialInvitation = {
    handlingEditor: {
      isVisible: status === statuses.heInvited,
      isHighlighed: true,
      startExpanded: true,
    },
  }

  const RespondToReviewerInvitation = {
    reviewer: {
      isVisible: !!chain(reviewers)
        .find(
          reviewer =>
            reviewer.user.id === currentUserId &&
            get(reviewer, 'status') === 'pending',
        )
        .value(),
      isHighlighed: true,
      startExpanded: true,
    },
  }

  const ReviewerDetailsEIC = {
    admin: {
      isVisible: get(handlingEditor, 'status') === 'accepted',
      startExpanded: !!reviewerReports.length,
    },
    editorInChief: {
      isVisible: get(handlingEditor, 'status') === 'accepted',
      startExpanded: !!reviewerReports.length,
    },
  }

  const ReviewerDetailsHE = {
    handlingEditor: {
      isVisible:
        ![statuses.heInvited, statuses.rejected].includes(status) &&
        get(handlingEditor, 'status') === 'accepted',

      startExpanded: status === statuses.heAssigned || !!reviewerReports.length,
      isHighlighed: status === statuses.heAssigned,
      canInviteReviewers: [
        statuses.heAssigned,
        statuses.reviewersInvited,
        statuses.underReview,
        statuses.reviewCompleted,
      ].includes(status),

      canInvitePublons: [
        statuses.heAssigned,
        statuses.reviewersInvited,
        statuses.underReview,
        statuses.reviewCompleted,
      ].includes(status),
    },
  }

  const reviewerCanSeeHisReport = !!chain(reviews)
    .find(review => get(review, 'member.user.id') === currentUserId)
    .get('submitted')
    .value()
  const reviewerCanSubmitReport = !!chain(reviewers)
    .find(
      reviewer =>
        reviewer.user.id === currentUserId &&
        get(reviewer, 'status') === 'accepted',
    )
    .value()

  const ReviewerReportBox = {
    reviewer: {
      isVisible: reviewerCanSeeHisReport,
      startExpanded: true,
    },
  }

  const ReviewerSubmitReport = {
    reviewer: {
      isVisible: reviewerCanSubmitReport && !reviewerCanSeeHisReport,
      isHighlighed: true,
    },
  }

  const SubmitRevision = {
    author: {
      isVisible: revisionDraft && status === statuses.revisionRequested,
      isHighlighed: true,
    },
  }

  return {
    AuthorReviewsBox: AuthorReviews[role],
    CommentWithFileBox: CommentWithFile[role],
    EditorialCommentsBox: EditorialComments[role],
    EditorInChiefAssignHEBox: EditorInChiefAssignHE[role],
    EICDecisionBox: EICDecision[role],
    HERecommendationBox: HERecommendation[role],
    RespondToEditorialInvitationBox: RespondToEditorialInvitation[role],
    RespondToReviewerInvitationBox: RespondToReviewerInvitation[role],
    ReviewerDetailsEICBox: ReviewerDetailsEIC[role],
    ReviewerDetailsHEBox: ReviewerDetailsHE[role],
    ReviewerReportContextualBox: ReviewerReportBox[role],
    ReviewerSubmitReportBox: ReviewerSubmitReport[role],
    SubmitRevisionBox: SubmitRevision[role],
  }
}

export default useVisibleStatuses
