import moize from 'moize'
import {
  chain,
  cloneDeep,
  get,
  omit,
  omitBy,
  isUndefined,
  isEqual,
  debounce,
  set,
  tail,
} from 'lodash'

export const parseAuthor = a =>
  omit({ ...a, ...get(a, 'alias'), ...get(a, 'alias.name') }, [
    '__typename',
    'id',
    'name',
    'alias',
    'user',
    'status',
    'invited',
    'reviewerNumber',
    'responded',
  ])

const booleanKeys = ['hasConflicts', 'hasDataAvailability', 'hasFunding']
export const convertKeysToBooleans = (obj, keys = booleanKeys) =>
  Object.keys(obj).reduce((acc, k) => {
    if (booleanKeys.includes(k)) {
      return { ...acc, [k]: obj[k] === 'yes' }
    }
    return { ...acc, [k]: obj[k] || '' }
  }, {})

export const convertBooleansToStrings = (obj, keys = booleanKeys) =>
  Object.keys(obj).reduce((acc, k) => {
    if (booleanKeys.includes(k)) {
      return { ...acc, [k]: obj[k] ? 'yes' : 'no' }
    }
    return { ...acc, [k]: obj[k] }
  }, {})

export const parseConflicts = conflicts => {
  const message = conflicts.hasConflicts ? conflicts.message : undefined
  const dataAvailabilityMessage = conflicts.hasDataAvailability
    ? undefined
    : conflicts.dataAvailabilityMessage
  const fundingMessage = conflicts.hasFunding
    ? undefined
    : conflicts.fundingMessage

  return omitBy(
    {
      ...conflicts,
      message,
      dataAvailabilityMessage,
      fundingMessage,
    },
    isUndefined,
  )
}

export const parseFormValues = values => {
  const manuscriptId = get(values, 'id', '')
  const meta = omit(get(values, 'meta', {}), '__typename')
  const conflicts = get(values, 'meta.conflicts', {}) || {}
  const content = get(values, 'responseToReviewers.content')

  const authors = chain(values)
    .get('authors', [])
    .filter(a => a.id !== 'unsaved-author')
    .map(parseAuthor)
    .value()

  const files = chain(values)
    .get('files', {})
    .flatMap()
    .map(({ mimeType, originalName, __typename, ...f }) => ({
      ...f,
      name: originalName,
    }))
    .value()

  return {
    manuscriptId,
    autosaveInput: {
      authors,
      files,
      meta: {
        ...meta,
        conflicts: parseConflicts(
          omit(convertKeysToBooleans(conflicts), '__typename'),
        ),
      },
      content,
    },
  }
}

export const autosaveRequest = ({
  values,
  updateDraftRevision,
  updateAutosave,
}) => {
  const variables = parseFormValues(values)

  updateAutosave({
    variables: {
      params: {
        error: null,
        inProgress: true,
        updatedAt: null,
      },
    },
  })

  updateDraftRevision({
    variables,
  }).then(r => {
    updateAutosave({
      variables: {
        params: {
          error: null,
          inProgress: false,
          updatedAt: Date.now(),
        },
      },
    })
  })
}

const memoizedAutosaveRequest = moize(autosaveRequest, {
  equals: ({ values: valuesOne }, { values: valuesTwo }) =>
    isEqual(valuesOne, valuesTwo),
})

export const autosaveForm = debounce(memoizedAutosaveRequest, 1000)

export const validateRevision = values => {
  const errors = {}
  if (values.isEditing) {
    errors.isEditing = 'An author is being edited.'
  }

  if (values.authors.length === 0) {
    errors.authorError = 'At least one author is required.'
  }

  if (get(values, 'files.manuscript').length === 0) {
    set(errors, 'fileError', 'At least one manuscript is required.')
  }

  if (
    !get(values, 'responseToReviewers.content') &&
    !get(values, 'responseToReviewers.file')
  ) {
    set(
      errors,
      'responseToReviewers.content',
      'A reply or a report file is needed.',
    )
  }

  return errors
}

export const parseManuscriptFiles = (files = []) =>
  files.reduce(
    (acc, file) => ({
      ...acc,
      [file.type]: [...acc[file.type], file],
    }),
    {
      manuscript: [],
      coverLetter: [],
      supplementary: [],
      responseToReviewers: [],
    },
  )

export const removeTypename = (inputObject = {}) => {
  const o = cloneDeep(inputObject)
  Object.keys(inputObject).forEach(key => {
    if (key === '__typename') delete o[key]
    if (o[key] !== null && typeof o[key] === 'object') {
      o[key] = removeTypename(o[key])
    }
  })
  return o
}

export const setInitialValues = values => {
  const id = get(values, 'id')
  const meta = omit(get(values, 'meta', {}), '__typename')
  const conflicts = convertBooleansToStrings(
    omit(get(values, 'meta.conflicts', {}), '__typename'),
  )
  const responseToReviewers = get(values, 'comment.content') || ''

  const authors = chain(values)
    .get('authors', [])
    .map(removeTypename)
    .value()
  const responseToReviewersFile = chain(values)
    .get('comment.files', [])
    .map(removeTypename)
    .value()

  const files = chain(values)
    .get('files', [])
    .map(removeTypename)
    .value()

  return {
    id,
    authors,
    files: parseManuscriptFiles(files),
    meta: {
      ...meta,
      conflicts,
    },
    responseToReviewers: {
      content: responseToReviewers,
      file: responseToReviewersFile[0],
    },
  }
}

export const parseSearchParams = url => {
  const params = new URLSearchParams(url)
  const parsedObject = {}
  /* eslint-disable */
  for ([key, value] of params) {
    parsedObject[key] = value
  }
  /* eslint-enable */
  return parsedObject
}

export const parseError = e => tail(e.message.split(':'))

const parseCommentToNotHaveFiles = comment => {
  const comm = cloneDeep(comment)
  Object.keys(comment).forEach(key => {
    if (key === 'files') delete comm[key]
  })
  return comm
}

export const parseReviewValues = values => {
  const reviewId = get(values, 'reviewId')
  const recommendation = get(values, 'recommendation')
  const publicComment = parseCommentToNotHaveFiles(
    removeTypename(get(values, 'public', {})),
  )
  const privateComment = parseCommentToNotHaveFiles(
    removeTypename(get(values, 'private', {})),
  )
  const comments = [publicComment, privateComment]
  const input = recommendation
    ? {
        recommendation,
        comments,
      }
    : {
        comments,
      }
  return {
    reviewId,
    input,
  }
}

export const autosaveReviewRequest = ({
  values,
  updateDraftReview,
  updateAutosaveReview,
}) => {
  const variables = parseReviewValues(values)

  updateAutosaveReview({
    variables: {
      params: {
        error: null,
        inProgress: true,
        updatedAt: null,
      },
    },
  })

  updateDraftReview({
    variables,
  }).then(() => {
    updateAutosaveReview({
      variables: {
        params: {
          error: null,
          inProgress: false,
          updatedAt: Date.now(),
        },
      },
    })
  })
}

const memoizedAutosaveReviewRequest = moize(autosaveReviewRequest, {
  equals: ({ values: valuesOne }, { values: valuesTwo }) =>
    isEqual(valuesOne, valuesTwo),
})

export const autosaveReviewForm = debounce(memoizedAutosaveReviewRequest, 1000)
