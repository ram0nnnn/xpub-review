import React from 'react'

import styled from 'styled-components'
import { get } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withHandlers } from 'recompose'

import { WizardFiles } from 'component-submission/client/components'
import {
  Row,
  Text,
  Icon,
  marginHelper,
  paddingHelper,
  ContextualBox,
} from 'component-hindawi-ui'

const ManuscriptFiles = ({
  formValues,
  formErrors,
  onChangeList,
  onDeleteFile,
  onUploadFile,
  startExpanded,
}) => (
  <ContextualBox
    label="Manuscript Files"
    startExpanded={startExpanded}
    transparent
  >
    <SectionRoot ml={1} pl={1}>
      <Row justify="flex-start" mb={2}>
        <Text display="inline" secondary>
          Drag & Drop files in the specific section or click{' '}
          <Icon
            bold
            color="colorSecondary"
            fontSize="10px"
            icon="expand"
            mr={1 / 2}
          />
          to upload. Use the{' '}
          <Icon color="colorSecondary" fontSize="16px" icon="move" mr={1 / 2} />
          icon to reorder or move files to a different type.
        </Text>
      </Row>

      <WizardFiles
        compact
        files={get(formValues, 'files', [])}
        onChangeList={onChangeList}
        onDeleteFile={onDeleteFile}
        onUploadFile={onUploadFile}
      />

      {get(formErrors, 'fileError', false) && (
        <Row justify="flex-start" mt={1}>
          <Text error>{get(formErrors, 'fileError', '')}</Text>
        </Row>
      )}
    </SectionRoot>
  </ContextualBox>
)

export default compose(
  withHandlers({
    onChangeList: ({ updateManuscriptFile }) => ({
      fileId,
      sourceProps,
      toListName: type,
      destinationProps,
    }) => {
      updateManuscriptFile({
        variables: {
          fileId,
          type,
        },
      })
        .then(r => {
          const file = r.data.updateManuscriptFile
          sourceProps.remove(sourceProps.index)
          destinationProps.push(file)
        })
        .catch(e => {
          destinationProps.setError(e.message)
        })
    },
    onDeleteFile: ({ handleDelete }) => (
      file,
      { index, remove, setError, setFetching },
    ) => {
      handleDelete(file, {
        setError,
        setFetching,
        index,
        remove,
      })
    },
    onUploadFile: ({ revisionManuscriptId, handleUpload }) => (
      file,
      { type, push, setFetching, ...rest },
    ) => {
      handleUpload(file, {
        type,
        entityId: revisionManuscriptId,
        setFetching,
        push,
      })
    },
  }),
)(ManuscriptFiles)

const SectionRoot = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-left: 2px solid ${th('colorBorder')};

  ${marginHelper}
  ${paddingHelper}
`
