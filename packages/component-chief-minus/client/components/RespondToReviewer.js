import React from 'react'
import styled from 'styled-components'
import { compose, setDisplayName, withProps, withHandlers } from 'recompose'
import { FilePicker, Spinner } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { get } from 'lodash'
import { File } from 'component-files/client'
// import { required } from 'xpub-validators'
import {
  Row,
  Icon,
  Item,
  Label,
  Textarea,
  ActionLink,
  withFetching,
  marginHelper,
  ContextualBox,
  paddingHelper,
  ValidatedFormField,
} from 'component-hindawi-ui'

const allowedFileExtensions = ['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']

const RespondToReviewer = ({
  maxFiles = 1,
  onUpload,
  onDelete,
  isFetching,
  responseFile,
  startExpanded,
}) => (
  <ContextualBox
    label="Response to Reviewer Comments"
    startExpanded={startExpanded}
    transparent
  >
    <SectionRoot ml={1} pl={1}>
      <Row>
        <Item vertical>
          <Item alignItems="center" height={3}>
            <Label mr={1} required>
              Your Reply
            </Label>
            {isFetching ? (
              <Spinner />
            ) : (
              <FilePicker
                allowedFileExtensions={allowedFileExtensions}
                disabled={!!responseFile}
                onUpload={onUpload}
              >
                <ActionLink
                  data-test-id="add-file"
                  disabled={!!responseFile}
                  fontSize="12px"
                  fontWeight="bold"
                >
                  <Icon
                    bold
                    color="colorWarning"
                    fontSize="10px"
                    icon="expand"
                    mr={1 / 2}
                  />
                  UPLOAD FILE
                </ActionLink>
              </FilePicker>
            )}
          </Item>
          <ValidatedFormField
            component={Textarea}
            minHeight={11}
            name="responseToReviewers.content"
          />
        </Item>
      </Row>
      <Row justify="flex-start" mb={2}>
        {responseFile && (
          <File hasDelete item={responseFile} onDelete={onDelete} />
        )}
      </Row>
    </SectionRoot>
  </ContextualBox>
)

export default compose(
  withFetching,
  setDisplayName('RespondToReviewer'),
  withProps(({ formValues, revisionDraft }) => ({
    responseCommentId: get(revisionDraft, 'comment.id'),
    responseFile: get(formValues, 'responseToReviewers.file'),
  })),
  withHandlers({
    addFileToField: ({ setFieldValue }) => field => file =>
      setFieldValue(field, file),
    removeFileFromField: ({ setFieldValue }) => field => () =>
      setFieldValue(field, ''),
  }),
  withHandlers({
    onUpload: ({
      handleUpload,
      setFetching,
      responseCommentId,
      addFileToField,
      setFieldError,
      ...rest
    }) => file => {
      handleUpload(file, {
        type: 'responseToReviewers',
        entityId: responseCommentId,
        setFetching,
        setFileField: addFileToField('responseToReviewers.file'),
        ...rest,
      })
    },
    onDelete: ({
      handleDelete,
      clearError,
      setError,
      setFetching,
      removeFileFromField,
    }) => file => {
      clearError()

      handleDelete(file, {
        setError,
        setFetching,
        setFileField: removeFileFromField('responseToReviewers.file'),
      })
    },
  }),
)(RespondToReviewer)

const SectionRoot = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-left: 2px solid ${th('colorBorder')};

  ${marginHelper}
  ${paddingHelper}
`
