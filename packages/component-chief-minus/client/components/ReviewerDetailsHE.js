import React from 'react'
import { ReviewerDetails, ReviewerReports } from 'component-hindawi-ui'
import { PublonsTable } from 'component-reviewer-suggestions/client'
import { InviteReviewers } from '.'

const ReviewerDetailsHE = ({
  mt,
  options,
  isVisible,
  manuscript,
  highlight,
  startExpanded,
  reviewers = [],
  inviteReviewer,
  reviewerReports,
  canInviteReviewers,
  canInvitePublons,
  reviewerSuggestions,
  cancelReviewerInvitation,
  resendReviewerInvitation,
}) => (
  <ReviewerDetails
    highlight={highlight}
    isVisible={isVisible}
    mt={mt}
    reviewers={reviewers}
    startExpanded={startExpanded}
    tabButtons={[
      'Reviewer Details',
      'Reviewer Suggestions',
      'Reviewer Reports',
    ]}
  >
    <InviteReviewers
      cancelReviewerInvitation={cancelReviewerInvitation}
      canInviteReviewers={canInviteReviewers}
      onInvite={inviteReviewer}
      resendReviewerInvitation={resendReviewerInvitation}
      reviewers={reviewers}
    />
    <PublonsTable
      canInvitePublons={canInvitePublons}
      onInvite={inviteReviewer}
      reviewers={reviewerSuggestions}
    />
    <ReviewerReports options={options} reviewerReports={reviewerReports} />
  </ReviewerDetails>
)

export default ReviewerDetailsHE
