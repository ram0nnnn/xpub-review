import React from 'react'
import styled from 'styled-components'
import { compose, withHandlers, withProps } from 'recompose'
import { get } from 'lodash'

import { required } from 'xpub-validators'
import { th } from '@pubsweet/ui-toolkit'
import { TextField } from '@pubsweet/ui'
import { WizardAuthors } from 'component-submission/client/components'

import {
  Row,
  Menu,
  Item,
  Label,
  Textarea,
  marginHelper,
  paddingHelper,
  ContextualBox,
  ValidatedFormField,
} from 'component-hindawi-ui'

const DetailsAndAuthors = ({
  journal,
  formErrors,
  formValues,
  onSaveAuthor,
  onEditAuthor,
  onDeleteAuthor,
  startExpanded,
  setFieldValue,
  manuscriptTypes,
  setWizardEditMode,
}) => (
  <ContextualBox
    label="Details & Authors"
    startExpanded={startExpanded}
    transparent
  >
    <SectionRoot ml={1} pl={1}>
      <Row mb={2}>
        <Item data-test-id="submission-title" flex={3} mr={2} vertical>
          <Label required>Manuscript Title</Label>
          <ValidatedFormField
            component={TextField}
            inline
            name="meta.title"
            validate={[required]}
          />
        </Item>
        <Item data-test-id="submission-type" vertical>
          <Label required>Manuscript Type</Label>
          <ValidatedFormField
            component={Menu}
            name="meta.articleType"
            options={manuscriptTypes}
            placeholder="Please select"
            validate={[required]}
          />
        </Item>
      </Row>
      <Row mb={3}>
        <Item data-test-id="submission-abstract" vertical>
          <Label required>Abstract</Label>
          <ValidatedFormField
            component={Textarea}
            minHeight={15}
            name="meta.abstract"
            validate={[required]}
          />
        </Item>
      </Row>

      <WizardAuthors
        formValues={formValues}
        journal={journal}
        onDeleteAuthor={onDeleteAuthor({ formValues, setFieldValue })}
        onEditAuthor={onEditAuthor({ formValues, setFieldValue })}
        onSaveAuthor={onSaveAuthor({ formValues, setFieldValue })}
        setWizardEditMode={setWizardEditMode}
        wizardErrors={formErrors}
      />
    </SectionRoot>
  </ContextualBox>
)

export default compose(
  withProps(({ journal }) => ({
    manuscriptTypes: get(journal, 'manuscriptTypes', []),
  })),
  withHandlers({
    setWizardEditMode: ({ setFieldValue }) => value =>
      setFieldValue('isEditing', value),
    onDeleteAuthor: ({ revisionManuscriptId, removeAuthor }) => ({
      values,
      setFieldValue,
    }) => (
      { id: authorTeamMemberId },
      { setError, clearError, setEditMode, setFetching, setWizardEditMode },
    ) => {
      clearError()
      setFetching(true)
      removeAuthor({
        variables: { manuscriptId: revisionManuscriptId, authorTeamMemberId },
      })
        .then(r => {
          setFetching(false)
          setEditMode(false)
          setFieldValue('authors', r.data.removeAuthorFromManuscript)
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    onEditAuthor: ({ revisionManuscriptId, editAuthor }) => ({
      values,
      setFieldValue,
    }) => (
      { id: authorTeamMemberId, ...authorInput },
      { setError, clearError, setEditMode, setFetching, setWizardEditMode },
    ) => {
      clearError()
      setFetching(true)
      editAuthor({
        variables: {
          manuscriptId: revisionManuscriptId,
          authorInput,
          authorTeamMemberId,
        },
      })
        .then(r => {
          setFetching(false)
          setWizardEditMode(false)
          setFieldValue('authors', r.data.editAuthorFromManuscript)
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
    onSaveAuthor: ({ addAuthorToManuscript, revisionManuscriptId }) => ({
      values,
      setFieldValue,
    }) => (
      { id, ...authorInput },
      {
        index,
        formFns,
        setError,
        clearError,
        setEditMode,
        setFetching,
        setWizardEditMode,
      },
    ) => {
      clearError()
      setFetching(true)
      addAuthorToManuscript({
        variables: { manuscriptId: revisionManuscriptId, authorInput },
      })
        .then(r => {
          setFetching(false)
          setWizardEditMode(false)
          setFieldValue('authors', r.data.addAuthorToManuscript)
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(DetailsAndAuthors)

const SectionRoot = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-left: 2px solid ${th('colorBorder')};

  ${marginHelper}
  ${paddingHelper}
`
