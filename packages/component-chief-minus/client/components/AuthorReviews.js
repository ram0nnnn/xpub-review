import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { withProps, compose } from 'recompose'
import { th } from '@pubsweet/ui-toolkit'
import styled from 'styled-components'

import { Row, Text, ContextualBox } from 'component-hindawi-ui'
import { ReviewerReportAuthor } from './'

const SubmittedReportsNumberForAuthorReviews = ({ reports }) => (
  <Row fitContent justify="flex-end">
    <Text customId mr={1 / 2}>
      {reports}
    </Text>
    <Text mr={1 / 2} pr={1 / 2} secondary>
      {' '}
      submitted
    </Text>
  </Row>
)

const AuthorReviews = ({
  token,
  toggle,
  journal,
  isVisible,
  reviewerReports,
}) =>
  reviewerReports.length > 0 &&
  isVisible && (
    <ContextualBox
      label="Reviewer Reports"
      mb={2}
      mt={2}
      rightChildren={() => (
        <SubmittedReportsNumberForAuthorReviews
          reports={reviewerReports.length}
        />
      )}
      toggle={toggle}
    >
      <Root>
        {reviewerReports.map(r => (
          <ReviewerReportAuthor
            journal={journal}
            key={r.id}
            report={r}
            token={token}
          />
        ))}
      </Root>
    </ContextualBox>
  )

AuthorReviews.propTypes = {
  /** The list of available reports. */
  reports: PropTypes.arrayOf(PropTypes.object),
  /** Object containing the list of recommendations. */
  journal: PropTypes.object, //eslint-disable-line
  /** Contains the token of the currently logged user. */
  token: PropTypes.string,
}

AuthorReviews.defaultProps = {
  reports: [],
  journal: {},
  token: '',
}

export default compose(
  withProps(({ currentUser }) => ({
    token: get(currentUser, 'token', ''),
  })),
)(AuthorReviews)

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
`
