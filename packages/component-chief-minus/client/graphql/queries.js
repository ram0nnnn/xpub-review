import gql from 'graphql-tag'

import {
  manuscriptFragment,
  userFragment,
  fileFragment,
  teamMember,
} from './fragments'

export const getManuscript = gql`
  query getManuscript($manuscriptId: String!) {
    getManuscript(manuscriptId: $manuscriptId) {
      ...manuscriptDetails
    }
  }
  ${manuscriptFragment}
`

export const getManuscriptVersions = gql`
  query getManuscriptVersions($submissionId: String!) {
    getManuscriptVersions(submissionId: $submissionId) {
      ...manuscriptDetails
    }
    getDraftRevision(submissionId: $submissionId) {
      ...manuscriptDetails
      comment {
        id
        type
        content
        files {
          ...fileDetails
        }
      }
    }
  }
  ${manuscriptFragment}
  ${fileFragment}
`

export const getCurrentUser = gql`
  query {
    getCurrentUser {
      ...userDetails
    }
  }
  ${userFragment}
`

export const getHandlingEditors = gql`
  query getHandlingEditors($manuscriptId: String!) {
    getHandlingEditors(manuscriptId: $manuscriptId) {
      ...teamMember
    }
  }
  ${teamMember}
`
