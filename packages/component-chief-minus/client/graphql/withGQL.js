import { graphql } from 'react-apollo'
import { compose } from 'recompose'
import { queries as activityLogQueries } from 'component-activity-log/client'

import * as queries from './queries'
import * as mutations from './mutations'

const refetchGetManuscriptVersions = ({ params: { submissionId = '' } }) => ({
  query: queries.getManuscriptVersions,
  variables: { submissionId },
})

const refetchGetAuditLogEvents = ({ params: { manuscriptId = '' } }) => ({
  query: activityLogQueries.getAuditLogEvents,
  variables: { manuscriptId },
})

export default compose(
  graphql(mutations.getFileSignedUrl, {
    name: 'getFileSignedUrl',
  }),
)

export const withEditorInChiefGQL = compose(
  graphql(mutations.inviteHandlingEditor, {
    name: 'inviteHandlingEditor',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.resendHandlingEditorInvitation, {
    name: 'resendHandlingEditorInvitation',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.cancelHandlingEditorInvitation, {
    name: 'cancelHandlingEditorInvitation',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.removeHandlingEditor, {
    name: 'removeHandlingEditor',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.makeDecisionToPublishAsEiC, {
    name: 'makeDecisionToPublishAsEiC',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.makeDecisionToRejectAsEiC, {
    name: 'makeDecisionToRejectAsEiC',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),

  graphql(mutations.makeDecisionToReturnAsEiC, {
    name: 'makeDecisionToReturnAsEiC',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),

  graphql(mutations.requestRevisionAsEiC, {
    name: 'requestRevisionAsEiC',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.resendHandlingEditorInvitation, {
    name: 'resendHandlingEditorInvitation',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
)

export const withHandlingEditorGQL = compose(
  graphql(mutations.acceptHandlingEditorInvitation, {
    name: 'acceptHandlingEditorInvitation',
    options: ({ match }) => ({
      refetchQueries: [refetchGetManuscriptVersions(match)],
    }),
  }),
  graphql(mutations.declineHandlingEditorInvitation, {
    name: 'declineHandlingEditorInvitation',
    options: {
      refetchQueries: ['getManuscripts'],
    },
  }),
  graphql(mutations.requestRevisionAsHE, {
    name: 'requestRevisionAsHE',
    options: {
      refetchQueries: ['getManuscriptVersions'],
    },
  }),
  graphql(mutations.recommendToRejectAsHE, {
    name: 'recommendToRejectAsHE',
    options: {
      refetchQueries: ['getManuscriptVersions'],
    },
  }),
  graphql(mutations.recommendToPublishAsHE, {
    name: 'recommendToPublishAsHE',
    options: {
      refetchQueries: ['getManuscriptVersions'],
    },
  }),
  graphql(mutations.inviteReviewer, {
    name: 'inviteReviewer',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.resendReviewerInvitation, {
    name: 'resendReviewerInvitation',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
  graphql(mutations.cancelReviewerInvitation, {
    name: 'cancelReviewerInvitation',
    options: ({ match, role }) => {
      const refetchQueries = [refetchGetManuscriptVersions(match)]
      if (role === 'admin') refetchQueries.push(refetchGetAuditLogEvents(match))
      return { refetchQueries }
    },
  }),
)

export { default as withReviewerGQL } from './withReviewerGQL'
export { default as withAuthorGQL } from './withAuthorGQL'
