import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as mutations from './mutations'

export default compose(
  graphql(mutations.acceptReviewerInvitation, {
    name: 'acceptReviewerInvitation',
    options: {
      refetchQueries: ['getManuscriptVersions'],
    },
  }),
  graphql(mutations.declineReviewerInvitation, {
    name: 'declineReviewerInvitation',
    options: ({ currentUser }) =>
      currentUser
        ? {
            refetchQueries: ['getManuscripts'],
          }
        : {},
  }),
  graphql(mutations.updateDraftReview, {
    name: 'updateDraftReview',
  }),
  graphql(mutations.updateAutosaveReview, {
    name: 'updateAutosaveReview',
  }),
  graphql(mutations.submitReview, {
    name: 'submitReview',
    options: {
      refetchQueries: ['getManuscriptVersions'],
    },
  }),
)
