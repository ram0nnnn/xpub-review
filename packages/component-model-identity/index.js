const Identity = require('./src/identity')
const resolvers = require('./src/resolvers')

module.exports = {
  model: Identity,
  modelName: 'Identity',
  resolvers,
}
