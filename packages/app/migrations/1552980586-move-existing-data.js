const mimetype = require('mimetype')
const BaseModel = require('@pubsweet/base-model')
const logger = require('@pubsweet/logger')
const Promise = require('bluebird')
const { v4 } = require('uuid')
const { HindawiBaseModel } = require('component-model')

BaseModel.prototype.$beforeInsert = function $beforeInsert() {
  this.id = this.id || v4()
  this.created = this.created || new Date().toISOString()
  this.updated = this.created
}

HindawiBaseModel.prototype.saveRecursively = async function saveRecursively() {
  return this.constructor
    .query()
    .upsertGraph(this, { insertMissing: true, relate: true, noDelete: true })
}

const {
  Team,
  User,
  File,
  Review,
  Journal,
  Comment,
  Identity,
  Manuscript,
  TeamMember,
} = require('@pubsweet/models')

const GLOBAL_ROLES = {
  admin: 'admin',
  eic: 'editorInChief',
  he: 'handlingEditor',
}

class Entity extends BaseModel {
  static get tableName() {
    return 'entities'
  }

  static get schema() {
    return {
      properties: {
        id: { type: 'string', format: 'uuid' },
        data: { type: 'string', format: 'jsonb' },
      },
    }
  }
}

module.exports = {
  up: async () => {
    const entities = await Entity.all()
    const journals = await Journal.all()
    const journalId = journals[0].id

    const { globalHeTeam, eicTeam, adminTeam } = await findOrCreateGlobalTeams(
      journalId,
    )

    await createUsers({
      users: entities.filter(entity => entity.data.type === 'user'),
      globalTeams: { globalHeTeam, eicTeam, adminTeam },
    })

    await createManuscripts({
      entities,
      journalId,
    })
  },
}

const createManuscripts = async ({ entities, journalId }) => {
  const collections = entities.filter(
    entity => entity.data.type === 'collection',
  )

  if (collections.length === 0) return

  return Promise.each(collections, async collection => {
    const fragments = entities.filter(
      entity =>
        entity.data.type === 'fragment' &&
        entity.data.collectionId === collection.id,
    )

    if (fragments.length === 0) return

    return Promise.each(fragments, async fragment => {
      const manuscript = new Manuscript({
        id: fragment.id,
        created: new Date(collection.data.created),
        updated: new Date(collection.data.created),
        journalId,
        customId: collection.data.customId,
        submissionId: collection.id,
        version: fragment.data.version,
        title: fragment.data.metadata.title,
        articleType: fragment.data.metadata.type,
        abstract: fragment.data.metadata.abstract,
        agreeTc: getAgreeToTerms(fragment.data),
        publicationDates: setPublicationDates({
          fragment: fragment.data,
          status: collection.data.status,
        }),
        technicalCheckToken: getTechnicalCheckToken(collection.data),
        hasPassedEqa: hasPassedEQA(collection.data),
        hasPassedEqs: hasPassedEQS(collection.data),
        conflicts: getConflicts(fragment.data),
      })

      manuscript.updateProperties({
        status: collection.data.status || Manuscript.Statuses.draft,
      })

      await createFiles({ fragment: fragment.data, manuscript })

      await createReviewerInvitations({
        fragment: fragment.data,
        journalId,
        manuscript,
      })

      if (collection.handlingEditor) {
        await addHandlingEditor({ collection, manuscript, journalId })
      }

      if (fragment.data.revision) {
        await createManuscriptFromRevision({
          collection: collection.data,
          journalId,
          fragment: fragment.data,
        })
      }

      await createReviews({
        fragment: fragment.data,
        manuscript,
      })

      await addAuthors({
        fragment: fragment.data,
        manuscript,
        journalId,
      })

      await createResponseToReviewers({
        responseToReviewers: fragment.data.responseToReviewers,
        manuscript,
        fragment: fragment.data,
      })

      await manuscript.saveRecursively()

      logger.info(`Saved Manuscript recursively with UUID ${manuscript.id}`)
    })
  })
}

const createUsers = async ({ users, globalTeams }) =>
  Promise.each(users, async entityUser => {
    if (
      await Identity.findOneBy({
        queryObject: { email: entityUser.data.email },
      })
    ) {
      logger.warn(`${entityUser.data.email} already exists.`)
      return
    }

    const user = new User({
      id: entityUser.id,
      defaultIdentity: 'local',
      isSubscribedToEmails: entityUser.data.notifications.email.user,
      unsubscribeToken: v4(),
      agreeTc: true,
      isActive: entityUser.data.isActive,
    })

    const identity = new Identity({
      type: 'local',
      isConfirmed: entityUser.data.isConfirmed,
      email: entityUser.data.email,
      givenNames: entityUser.data.firstName,
      surname: entityUser.data.lastName,
      title: setTitle(entityUser.data.title),
      aff: entityUser.data.affiliation,
      country: entityUser.data.country,
      passwordHash: entityUser.data.passwordHash,
    })

    await user.assignIdentity(identity)
    await user.saveRecursively()
    logger.info(`Saved User recursively with UUID ${user.id}`)

    if (hasGlobalRole(entityUser.data)) {
      addUserToGlobalTeam({ globalTeams, user, entityUser: entityUser.data })
    }
  })

const findOrCreateGlobalTeams = async journalId => {
  let globalHeTeam = await Team.findOneBy({
    queryObject: {
      role: GLOBAL_ROLES.he,
      manuscriptId: null,
    },
  })

  if (!globalHeTeam) {
    globalHeTeam = await createTeam({
      Team,
      role: GLOBAL_ROLES.he,
      journalId,
    })
  }

  let eicTeam = await Team.findOneBy({
    queryObject: {
      role: GLOBAL_ROLES.eic,
      manuscriptId: null,
    },
  })

  if (!eicTeam) {
    eicTeam = await createTeam({
      Team,
      role: GLOBAL_ROLES.eic,
      journalId,
    })
  }

  let adminTeam = await Team.findOneBy({
    queryObject: {
      role: GLOBAL_ROLES.admin,
      manuscriptId: null,
    },
  })

  if (!adminTeam) {
    adminTeam = await createTeam({
      Team,
      role: GLOBAL_ROLES.admin,
      journalId,
    })
  }

  return { globalHeTeam, eicTeam, adminTeam }
}

const createTeam = async ({ Team, role, journalId }) => {
  const team = new Team({
    role,
    journalId,
  })

  await team.save()
  return team
}

const hasGlobalRole = user =>
  user.editorInChief || user.admin || user.handlingEditor

const addUserToGlobalTeam = async ({ globalTeams, user, entityUser }) => {
  let team = {}
  if (entityUser.admin) {
    team = globalTeams.adminTeam
  } else if (entityUser.editorInChief) {
    team = globalTeams.eicTeam
  } else if (entityUser.handlingEditor) {
    team = globalTeams.globalHeTeam
  }
  await team.addMember(user)
  await team.saveRecursively()
}

const getTechnicalCheckToken = collection => {
  if (collection.technicalChecks) {
    return collection.technicalChecks.token
  }

  return null
}

const hasPassedEQS = collection => {
  if (
    collection.technicalChecks &&
    !collection.technicalChecks.token &&
    !collection.technicalChecks.eqa
  ) {
    return true
  }

  return false
}

const hasPassedEQA = collection => {
  if (collection.technicalChecks) {
    return collection.technicalChecks.eqa
  }

  return false
}

const setPublicationDates = ({ fragment, status }) => {
  const pubDates = []
  let publicationType = ''
  if (fragment.submitted) {
    publicationType = Manuscript.Statuses.technicalChecks
    pubDates.push({
      type: publicationType,
      date: fragment.submitted,
    })
  }

  if (status === Manuscript.Statuses.inQA) {
    publicationType = Manuscript.Statuses.inQA
    pubDates.push({
      type: publicationType,
      date: fragment.submitted,
    })
  }

  return pubDates
}

const createFiles = async ({ fragment, manuscript }) => {
  if (!fragment.files) return

  await Promise.each(Object.keys(fragment.files), async key => {
    await Promise.each(fragment.files[key], async (f, index) => {
      const file = new File({
        manuscriptId: manuscript.id,
        commentId: undefined,
        type: key === 'manuscripts' ? 'manuscript' : key,
        fileName: f.name,
        size: f.size,
        originalName: f.originalName,
        position: index,
        mimeType: mimetype.lookup(f.originalName),
      })

      manuscript.assignFile(file)
    })
  })
}

const getAgreeToTerms = fragment => {
  if (fragment.declarations) {
    return fragment.declarations.agree
  }

  return false
}

const getConflicts = fragment => {
  if (fragment.conflicts) {
    fragment.conflicts.hasDataAvailability =
      fragment.conflicts.hasDataAvailability === 'yes'
    fragment.conflicts.hasConflicts = fragment.conflicts.hasConflicts === 'yes'
    fragment.conflicts.hasFunding = fragment.conflicts.hasFunding === 'yes'
  } else {
    fragment.conflicts = {}
  }

  return {
    type: 'object',
    default: {},
    properties: {
      hasConflicts: fragment.conflicts.hasConflicts,
      message: fragment.conflicts.message,
      hasDataAvailability: fragment.conflicts.hasDataAvailability,
      dataAvailabilityMessage: fragment.conflicts.dataAvailabilityMessage,
      hasFunding: fragment.conflicts.hasFunding,
      fundingMessage: fragment.conflicts.fundingMessage,
    },
  }
}

const setTitle = title => {
  const allowedTitles = ['mr', 'mrs', 'miss', 'ms', 'dr', 'prof', null]
  if (allowedTitles.includes(title)) {
    return title
  }

  return null
}

const createReviewerInvitations = async ({
  fragment,
  journalId,
  manuscript,
}) => {
  const reviewerTeam = new Team({
    journalId,
    role: Team.Role.reviewer,
  })
  manuscript.assignTeam(reviewerTeam)

  if (!fragment.invitations || fragment.invitations.length === 0) return
  await Promise.each(fragment.invitations, async invitation => {
    let user
    try {
      user = await User.find(invitation.userId, 'identities')
    } catch (e) {
      logger.warn(`Fragment ${manuscript.id} might have corrupt invitations.`)
      return
    }

    const options = {
      id: invitation.id,
      status: getInvitationStatus(invitation),
      reviewerNumber: invitation.reviewerNumber,
      responded: invitation.respondedOn
        ? new Date(invitation.respondedOn)
        : null,
      created: new Date(invitation.invitedOn),
    }

    logger.info(`Added Reviewer TeamMember with UUID ${invitation.id}`)
    reviewerTeam.addMember(user, options)
  })
  await reviewerTeam.saveRecursively()
}

const getInvitationStatus = invitation => {
  if (invitation.hasAnswer) {
    if (invitation.isAccepted) {
      return TeamMember.Statuses.accepted
    }
    return TeamMember.Statuses.declined
  }
  return TeamMember.Statuses.pending
}

const createReviews = async ({ fragment, manuscript }) => {
  if (!fragment.recommendations || fragment.recommendations.length === 0) return
  await Promise.each(fragment.recommendations, async rec => {
    const teamMember = await TeamMember.findOneByField('userId', rec.userId)
    if (!teamMember) {
      logger.warn(
        `Fragment ${manuscript.id} migth have corrupt recommendations`,
      )
      return
    }
    const review = createNewReview({ rec, teamMember })
    review.assignMember(teamMember)

    if (rec.comments) {
      rec.comments.forEach(comm => {
        const comment = review.addComment({
          type: comm.public ? Comment.Types.public : Comment.Types.private,
          content: comm.content,
        })

        if (comm.files) {
          comm.files.forEach((file, index) => {
            if (file === null) return
            const commentFile = createCommentFile({ file, index })
            comment.assignFile(commentFile)
          })
        }
      })
    }
    manuscript.assignReview(review)
  })
}

const getRecommendation = recommendation => {
  if (recommendation === 'return-to-handling-editor')
    return Review.Recommendations.returnToHE
  return recommendation
}

const addHandlingEditor = async ({ collection, journalId, manuscript }) => {
  const heTeam = new Team({
    journalId,
    role: Team.Role.handlingEditor,
  })
  manuscript.assignTeam(heTeam)

  const heInvitation = collection.invitations.find(
    inv => inv.role === Team.Role.handlingEditor,
  )

  let user
  try {
    user = await User.find(heInvitation.userId, 'identities')
  } catch (e) {
    logger.warn(
      `Collection ${manuscript.submissionId} might have corupt invitations.`,
    )
    return
  }

  const options = {
    id: heInvitation.id,
    status: getInvitationStatus(heInvitation),
    responded: new Date(heInvitation.respondedOn),
    created: new Date(heInvitation.invitedOn),
  }

  logger.info(`Added HE TeamMember with UUID ${heInvitation.id}`)
  heTeam.addMember(user, options)
  await heTeam.saveRecursively()
}

const createManuscriptFromRevision = async ({
  collection,
  journalId,
  fragment,
}) => {
  const { revision } = fragment
  const manuscript = new Manuscript({
    created: new Date(collection.created),
    journalId,
    customId: collection.customId,
    submissionId: collection.id,
    version: fragment.version + 1,
    title: revision.metadata.title,
    articleType: revision.metadata.type,
    abstract: revision.metadata.abstract,
    agreeTc: true,
    technicalCheckToken: getTechnicalCheckToken(collection),
    hasPassedEqa: hasPassedEQA(collection),
    hasPassedEqs: hasPassedEQS(collection),
    conflicts: getConflicts(fragment),
  })

  manuscript.updateProperties({
    status: Manuscript.Statuses.draft,
  })

  if (collection.handlingEditor) {
    await addHandlingEditor({ collection, manuscript, journalId })
  }

  await manuscript.saveRecursively()

  await createFiles({ fragment: revision, manuscript })

  await addAuthors({ fragment: revision, manuscript, journalId })

  await createResponseToReviewers({
    responseToReviewers: revision.responseToReviewers,
    manuscript,
    fragment: revision,
  })

  await manuscript.saveRecursively()
  logger.info(`Saved Manuscript from Revision with UUID ${manuscript.id}`)
}

const createResponseToReviewers = async ({
  responseToReviewers,
  manuscript,
  fragment,
}) => {
  if (responseToReviewers) {
    if (!fragment.authors || fragment.authors.length === 0) return
    const authorTeamMember = await TeamMember.findOneByField(
      'userId',
      fragment.authors[0].id,
    )
    if (!authorTeamMember) {
      logger.warn(
        `Fragment ${manuscript.id} migth have corrupt recommendations`,
      )
      return
    }

    const review = new Review({
      recommendation: Review.Recommendations.responseToRevision,
      teamMemberId: authorTeamMember.id,
    })
    review.assignMember(authorTeamMember)

    const comment = review.addComment({
      type: Comment.Types.public,
      content: responseToReviewers.content ? responseToReviewers.content : '',
    })

    if (responseToReviewers.file) {
      const file = createCommentFile({
        file: responseToReviewers.file,
        index: 1,
      })
      comment.assignFile(file)
    }
    manuscript.assignReview(review)
  }
}

const createNewReview = ({ rec, teamMember }) => {
  const newReview = new Review({
    teamMemberId: teamMember.id,
    created: new Date(rec.createdOn),
    submitted: rec.submittedOn ? new Date(rec.submittedOn) : null,
    recommendation: getRecommendation(rec.recommendation),
  })
  return newReview
}

const createCommentFile = ({ file, index }) => {
  const commentFile = new File({
    manuscriptId: undefined,
    fileName: file.name,
    size: file.size,
    originalName: file.originalName,
    type: File.Types.reviewComment,
    mimeType: mimetype.lookup(file.originalName),
    position: index,
  })
  return commentFile
}
const addAuthors = async ({ fragment, journalId, manuscript }) => {
  const authorTeam = new Team({
    journalId,
    role: Team.Role.author,
  })
  manuscript.assignTeam(authorTeam)

  if (!fragment.authors || fragment.authors.length === 0) return
  await Promise.each(fragment.authors, async author => {
    let user
    try {
      user = await User.find(author.id, 'identities')
    } catch (e) {
      logger.warn(`Fragment ${manuscript.id} might have corrupt authors.`)
      return
    }

    const options = {
      status: TeamMember.Statuses.accepted,
    }

    logger.info(
      `Added author TeamMember with to Manuscript with UUID ${manuscript.id}`,
    )
    authorTeam.addMember(user, options)
  })
  await authorTeam.saveRecursively()
}
