module.exports = [
  {
    value: 'publish',
    label: 'Publish',
  },
  {
    value: 'major',
    label: 'Major Revision',
  },
  {
    value: 'minor',
    label: 'Minor Revision',
  },
  {
    value: 'reject',
    label: 'Reject',
  },
  {
    value: 'revision',
    label: 'Revision',
  },
]
