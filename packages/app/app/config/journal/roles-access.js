module.exports = {
  global: ['admin', 'editorInChief', 'author', 'handlingEditor'],
  collection: ['handlingEditor', 'reviewer', 'author'],
  inviteRights: {
    admin: ['admin', 'editorInChief', 'author', 'handlingEditor', 'author'],
    editorInChief: ['handlingEditor'],
    handlingEditor: ['reviewer'],
  },
}
