import React, { Fragment } from 'react'
import { Route } from 'react-router'
import { DragDropContext } from 'react-dnd'
import { Footer } from 'component-hindawi-ui'
import HTML5Backend from 'react-dnd-html5-backend'
import { createGlobalStyle } from 'styled-components'
import { AppBar } from 'component-authentication/client'
import { AutosaveIndicator, SubmitDraft } from 'component-submission/client'
import { queries } from 'component-dashboard/client'

const GlobalStyles = createGlobalStyle`
  body {
    height: 100vh;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale; 

    #root, #root > div, #root > div > div {
      height: 100%;
    }
  }
`

const HideOnPath = ({ component: Component, pathname }) => (
  <Route
    render={({ location }) => {
      if (location.pathname === pathname) return null
      return <Component />
    }}
  />
)

const App = ({ autosave, journal = {}, goTo, children }) => (
  <Fragment>
    <GlobalStyles />

    <HideOnPath
      component={() => (
        <AppBar
          autosaveIndicator={AutosaveIndicator}
          queries={{
            getManuscripts: queries.getManuscripts,
          }}
          submitButton={SubmitDraft}
        />
      )}
      pathname="/404"
    />

    {children}

    <HideOnPath component={Footer} pathname="/404" />
  </Fragment>
)

export default DragDropContext(HTML5Backend)(App)
