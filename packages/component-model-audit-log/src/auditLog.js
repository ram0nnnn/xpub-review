const { HindawiBaseModel } = require('component-model')

class AuditLog extends HindawiBaseModel {
  static get tableName() {
    return 'audit_log'
  }

  static get schema() {
    return {
      properties: {
        userId: { type: ['string', null], format: 'uuid' },
        manuscriptId: { type: ['string', null], format: 'uuid' },
        action: {
          type: 'string',
        },
        objectType: {
          type: 'string',
          enum: ['manuscript', 'user', 'review', 'file'],
        },
        objectId: { type: 'string', format: 'uuid' },
        journalId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      user: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-user').model,
        join: {
          from: 'audit_log.userId',
          to: 'user.id',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('component-model-manuscript').model,
        join: {
          from: 'audit_log.manuscriptId',
          to: 'manuscript.id',
        },
      },
    }
  }

  toDTO() {
    return {
      ...this,
      user: this.user ? this.user.toDTO() : undefined,
    }
  }
}

module.exports = AuditLog
