process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const { unlinkOrcidUseCase } = require('../src/use-cases')

const chance = new Chance()

describe('Unlink orcid account', () => {
  it('should unlink the orcid account of the user', async () => {
    const user = fixtures.generateUser({})
    const orcidIdentity = fixtures.generateIdentity({
      id: chance.guid(),
      type: 'orcid',
      userId: user.id,
      identifier: chance.guid(),
      surname: chance.last(),
      givenNames: chance.first(),
      isConfirmed: true,
    })
    await user.assignIdentity(orcidIdentity)
    await user.saveRecursively()

    const mockedModels = models.build(fixtures)
    await unlinkOrcidUseCase
      .initialize(mockedModels)
      .execute({ currentUser: user.id })
    expect(user.identities.length).toEqual(1)
    expect(user.identities[0].type).not.toEqual('orcid')
  })
  it('should not return an error when the user Ocid account is already unlinked', async () => {
    const user = fixtures.generateUser({})
    const mockedModels = models.build(fixtures)
    const result = unlinkOrcidUseCase
      .initialize(mockedModels)
      .execute({ currentUser: user.id })

    expect(result).rejects.toThrow('There is no Orcid account linked.')
  })
})
