const initialize = ({ User }) => ({
  execute: async ({ currentUser }) => {
    const user = await User.find(currentUser)
    if (user.isSubscribedToEmails) return
    user.updateProperties({ isSubscribedToEmails: true })
    await user.save()
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
