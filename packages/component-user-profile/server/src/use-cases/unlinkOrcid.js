const initialize = ({ User }) => ({
  execute: async ({ currentUser }) => {
    const user = await User.find(currentUser, 'identities')
    const orcidIdentity = user.identities.find(i => i.type === 'orcid')
    if (!orcidIdentity) throw new Error('There is no Orcid account linked.')
    user.removeIdentity(orcidIdentity.id)
    await user.saveRecursively()
  },
})
const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
