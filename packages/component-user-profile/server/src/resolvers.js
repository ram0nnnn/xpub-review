const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const {
  token: tokenService,
} = require('@pubsweet/model-user/src/authentication')

const useCases = require('./use-cases')

const resolvers = {
  Mutation: {
    async updateUser(_, { input }, ctx) {
      return useCases.updateUserUseCase
        .initialize(models)
        .execute({ currentUser: ctx.user, input })
    },
    async subscribeToEmails(_, { input }, ctx) {
      return useCases.subscribeToEmailsUseCase
        .initialize(models)
        .execute({ currentUser: ctx.user })
    },
    async unsubscribeToEmails(_, { input }, ctx) {
      return useCases.unsubscribeToEmailsUseCase
        .initialize(models)
        .execute({ currentUser: ctx.user, input })
    },
    async changePassword(_, { input }, ctx) {
      return useCases.changePasswordUseCase
        .initialize(tokenService, models)
        .execute(input, ctx.user)
    },
    async unlinkOrcid(_, { input }, ctx) {
      return useCases.unlinkOrcidUseCase
        .initialize(models)
        .execute({ currentUser: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
