const OrcidStrategy = require('passport-orcid')
const config = require('config')
const { get, words, last, initial } = require('lodash')

const { clientID, clientSecret, callbackPath, successPath } = config.get(
  'orcid',
)
const models = require('@pubsweet/models')

let userId
const LinkOrcid = app => {
  const { passport } = app.locals
  passport.serializeUser((user, done) => {
    done(null, user)
  })

  passport.deserializeUser((user, done) => {
    done(null, user)
  })

  passport.use(
    new OrcidStrategy(
      {
        sandbox: process.env.NODE_ENV !== 'production',
        callbackURL: `${config.get('pubsweet-client.baseUrl')}${callbackPath}`,
        clientID,
        clientSecret,
      },
      (accessToken, refreshToken, params, profile, done) => {
        profile = {
          orcid: params.orcid,
          name: params.name,
          accessToken,
          refreshToken,
          scope: params.scope,
          expiry: params.expires_in,
        }
        return done(null, profile)
      },
    ),
  )

  app.get(
    '/api/users/orcid',
    (req, res, next) => {
      userId = get(req, 'query.userId')
      next()
    },
    passport.authenticate('orcid'),
  )

  app.get(
    callbackPath,
    passport.authenticate('orcid', {
      failureRedirect: successPath,
    }),
    linkOrcidHandler(models),
  )
}

const linkOrcidHandler = ({ User, Identity }) => async (req, res) => {
  const user = await User.find(userId, 'identities')
  let orcidIdentity = user.identities.find(i => i.type === 'orcid')
  if (!orcidIdentity) {
    orcidIdentity = new Identity({
      type: 'orcid',
      userId: user.id,
      identifier: req.user.orcid,
      surname: last(words(req.user.name)),
      givenNames: initial(words(req.user.name)).toString(),
      isConfirmed: true,
    })
    await user.assignIdentity(orcidIdentity)
    await user.saveRecursively()
  }
  res.redirect(successPath)
}

module.exports = LinkOrcid
