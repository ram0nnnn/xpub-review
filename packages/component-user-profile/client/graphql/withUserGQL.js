import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as mutations from './mutations'
import * as queries from './queries'
import * as query from '../../../component-authentication/client/graphql/queries'

export default compose(
  graphql(queries.getCurrentUser),
  graphql(mutations.changePassword, {
    name: 'changePassword',
  }),
  graphql(mutations.updateUser, {
    name: 'updateUser',
    options: {
      refetchQueries: [
        { query: queries.getCurrentUser },
        { query: query.currentUser },
      ],
    },
  }),
  graphql(mutations.subscribeToEmails, {
    name: 'subscribeToEmails',
    options: {
      refetchQueries: [{ query: queries.getCurrentUser }],
    },
  }),
  graphql(mutations.unsubscribeToEmails, {
    name: 'unsubscribeToEmails',
    options: {
      refetchQueries: [{ query: queries.getCurrentUser }],
    },
  }),
  graphql(mutations.unlinkOrcid, {
    name: 'unlinkOrcid',
    options: {
      refetchQueries: [{ query: queries.getCurrentUser }],
    },
  }),
)
