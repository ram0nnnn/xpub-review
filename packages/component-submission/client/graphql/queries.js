import gql from 'graphql-tag'

import { draftManuscriptDetails } from './fragments'

export const getManuscript = gql`
  query getManuscript($manuscriptId: String!) {
    getManuscript(manuscriptId: $manuscriptId) {
      ...draftManuscriptDetails
    }
  }
  ${draftManuscriptDetails}
`

export const autosaveState = gql`
  query autosaveState {
    autosave @client {
      error
      updatedAt
      inProgress
    }
  }
`
