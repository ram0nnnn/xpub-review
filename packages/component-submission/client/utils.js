import moize from 'moize'
import {
  chain,
  cloneDeep,
  get,
  omit,
  omitBy,
  isUndefined,
  isEqual,
  debounce,
  set,
} from 'lodash'

export const parseAuthor = a =>
  omit({ ...a, ...get(a, 'alias'), ...get(a, 'alias.name') }, [
    '__typename',
    'id',
    'name',
    'alias',
    'user',
    'status',
    'invited',
    'responded',
    'reviewerNumber',
  ])

const booleanKeys = ['hasConflicts', 'hasDataAvailability', 'hasFunding']
export const convertKeysToBooleans = (obj, keys = booleanKeys) =>
  Object.keys(obj).reduce((acc, k) => {
    if (booleanKeys.includes(k)) {
      return { ...acc, [k]: obj[k] === 'yes' }
    }
    return { ...acc, [k]: obj[k] || '' }
  }, {})

export const convertBooleansToStrings = (obj, keys = booleanKeys) =>
  Object.keys(obj).reduce((acc, k) => {
    if (booleanKeys.includes(k)) {
      return { ...acc, [k]: obj[k] ? 'yes' : 'no' }
    }
    return { ...acc, [k]: obj[k] }
  }, {})

export const parseConflicts = conflicts => {
  const message = conflicts.hasConflicts ? conflicts.message : undefined
  const dataAvailabilityMessage = conflicts.hasDataAvailability
    ? undefined
    : conflicts.dataAvailabilityMessage
  const fundingMessage = conflicts.hasFunding
    ? undefined
    : conflicts.fundingMessage

  return omitBy(
    {
      ...conflicts,
      message,
      dataAvailabilityMessage,
      fundingMessage,
    },
    isUndefined,
  )
}

export const parseFormValues = values => {
  const manuscriptId = get(values, 'id', '')
  const meta = omit(get(values, 'meta', {}), '__typename')
  const conflicts = get(values, 'meta.conflicts', {}) || {}

  const authors = chain(values)
    .get('authors', [])
    .filter(a => a.id !== 'unsaved-author')
    .map(parseAuthor)
    .value()

  const files = chain(values)
    .get('files', {})
    .flatMap()
    .map(({ mimeType, originalName, __typename, ...f }) => ({
      ...f,
      name: originalName,
    }))
    .value()

  return {
    manuscriptId,
    autosaveInput: {
      authors,
      files,
      meta: {
        ...meta,
        conflicts: parseConflicts(
          omit(convertKeysToBooleans(conflicts), '__typename'),
        ),
      },
    },
  }
}

export const autosaveRequest = ({ values, updateDraft, updateAutosave }) => {
  const variables = parseFormValues(values)
  updateAutosave({
    variables: {
      params: {
        error: null,
        inProgress: true,
        updatedAt: null,
      },
    },
  })
  updateDraft({
    variables,
  }).then(r => {
    updateAutosave({
      variables: {
        params: {
          error: null,
          inProgress: false,
          updatedAt: Date.now(),
        },
      },
    })
  })
}

const memoizedAutosaveRequest = moize(autosaveRequest, {
  equals: ({ values: valuesOne }, { values: valuesTwo }) =>
    isEqual(valuesOne, valuesTwo),
})

export const autosaveForm = debounce(memoizedAutosaveRequest, 1000)

export const validateWizard = step => values => {
  const errors = {}
  if (values.isEditing) {
    errors.isEditing = 'An author is being edited.'
  }

  if (step === 2 && get(values, 'files.manuscript').length === 0) {
    set(errors, 'fileError', 'At least one manuscript is required.')
  }

  return errors
}

export const parseManuscriptFiles = (files = []) =>
  files.reduce(
    (acc, file) => ({
      ...acc,
      [file.type]: [...acc[file.type], file],
    }),
    {
      manuscript: [],
      coverLetter: [],
      supplementary: [],
    },
  )

export const removeTypename = (inputObject = {}) => {
  const o = cloneDeep(inputObject)
  Object.keys(inputObject).forEach(key => {
    if (key === '__typename') delete o[key]
    if (o[key] !== null && typeof o[key] === 'object') {
      o[key] = removeTypename(o[key])
    }
  })
  return o
}

export const setInitialValues = values => {
  const id = get(values, 'id')
  const meta = omit(get(values, 'meta', {}), '__typename')
  const conflicts = convertBooleansToStrings(
    omit(get(values, 'meta.conflicts', {}), '__typename'),
  )

  const authors = chain(values)
    .get('authors', [])
    .map(removeTypename)
    .value()

  const files = chain(values)
    .get('files', [])
    .map(removeTypename)
    .value()

  return {
    id,
    authors,
    files: parseManuscriptFiles(files),
    meta: {
      ...meta,
      conflicts,
    },
  }
}
