import React, { Fragment } from 'react'
import { get } from 'lodash'
import { H2 } from '@pubsweet/ui'
import { Row, Text, Label, Icon, ScrollContainer } from 'component-hindawi-ui'

import { WizardFiles } from './'

const WizardStepThree = ({
  formValues,
  wizardErrors,
  onUploadFile,
  onDeleteFile,
  onChangeList,
}) => (
  <Fragment>
    <H2>3. Manuscript Files Upload</H2>
    <Row>
      <Text
        align="center"
        display="inline-block"
        mb={1 / 4}
        mt={1 / 4}
        secondary
      >
        Drag & Drop files in the specific section or click{' '}
        <Icon
          bold
          color="colorSecondary"
          fontSize="10px"
          icon="expand"
          mr={1 / 2}
        />
        to upload.
      </Text>
    </Row>

    <Row mb={2}>
      <Text secondary>
        Use the{' '}
        <Icon color="colorSecondary" fontSize="16px" icon="move" mr={1 / 2} />
        icon to reorder or move files to a different type.
      </Text>
    </Row>

    <ScrollContainer>
      <Row justify="flex-start">
        <Label mb={1} mt={1}>
          Files
        </Label>
      </Row>

      <WizardFiles
        files={get(formValues, 'files', [])}
        onChangeList={onChangeList}
        onDeleteFile={onDeleteFile}
        onUploadFile={onUploadFile}
      />

      {get(wizardErrors, 'fileError', false) && (
        <Row justify="flex-start" mt={1}>
          <Text error>{get(wizardErrors, 'fileError', '')}</Text>
        </Row>
      )}
    </ScrollContainer>
  </Fragment>
)

export default WizardStepThree
