import React, { Fragment } from 'react'
import { withHandlers } from 'recompose'
import { required } from 'xpub-validators'
import {
  Row,
  Text,
  Item,
  Label,
  Textarea,
  ActionLink,
  ScrollContainer,
  RowOverrideAlert,
  ItemOverrideAlert,
  RadioWithComments,
  ValidatedFormField,
  Menu,
} from 'component-hindawi-ui'
import { H2, TextField } from '@pubsweet/ui'

import { WizardAuthors } from './'

const WizardStepTwo = ({
  journal,
  formValues,
  isAuthorEdit,
  getTooltipContent,
  journal: {
    manuscriptTypes = [],
    submission: { questions = [] },
  },
  setWizardEditMode,
  ...rest
}) => (
  <Fragment>
    <Row alignItems="center">
      <H2>2. Manuscript & Author Details</H2>
    </Row>
    <Row flexDirection="column" mb={3}>
      <Text align="center" mb={1} mt={1} secondary>
        Please provide the details of all the authors of this manuscript, in the
        order that they appear on the manuscript.
      </Text>
      <Text align="center" secondary>
        Your details have been prefilled as the submitting author.
      </Text>
    </Row>

    <ScrollContainer>
      <Row mb={1}>
        <Item data-test-id="submission-title" flex={3} mr={1} vertical>
          <Label required>Manuscript Title</Label>
          <ValidatedFormField
            component={TextField}
            inline
            name="meta.title"
            validate={[required]}
          />
        </Item>
        <ItemOverrideAlert data-test-id="submission-type" vertical>
          <Label required>Manuscript Type</Label>
          <ValidatedFormField
            component={Menu}
            name="meta.articleType"
            options={manuscriptTypes}
            placeholder="Please select"
            validate={[required]}
          />
        </ItemOverrideAlert>
      </Row>

      <RowOverrideAlert mb={2}>
        <Item data-test-id="submission-abstract" vertical>
          <Label required>Abstract</Label>
          <ValidatedFormField
            component={Textarea}
            minHeight={15}
            name="meta.abstract"
            validate={[required]}
          />
        </Item>
      </RowOverrideAlert>

      <WizardAuthors
        formValues={formValues}
        journal={journal}
        setWizardEditMode={setWizardEditMode}
        {...rest}
      />

      {questions.map(q => (
        <RadioWithComments
          formValues={formValues}
          key={q.id}
          tooltipContent={getTooltipContent(q.id)}
          {...q}
        />
      ))}
    </ScrollContainer>
  </Fragment>
)

export default withHandlers({
  getTooltipContent: () => questionId => () => {
    switch (questionId) {
      case 'conflictsOfInterest':
        return <ConflictsTooltip />
      case 'dataAvailability':
        return <DataAvailabilityTooltip />
      case 'funding':
        return <FundingTooltip />
      default:
        return null
    }
  },
  setWizardEditMode: ({ setFieldValue }) => value => {
    setFieldValue('isEditing', value)
  },
})(WizardStepTwo)

const ConflictsTooltip = () => (
  <Text secondary>
    When an author, editor, or reviewer has a financial/personal interest or
    belief that could affect his/her objectivity, or inappropriately influence
    his/her actions, a potential conflict of interest exists.{' '}
    <ActionLink to="https://www.hindawi.com/editors/coi/">More info</ActionLink>
  </Text>
)

const DataAvailabilityTooltip = () => (
  <Text secondary>
    Statement about where data supporting the results reported in a published
    article can be found, including, where applicable, hyperlinks to publicly
    archived datasets analysed or generated during the study.
  </Text>
)

const FundingTooltip = () => (
  <Text secondary>
    Statement about how the research and publication of an article is funded,
    naming each financially supporting body followed by any associated grant
    numbers in square brackets.
  </Text>
)
