The submission statement confirmation modal at the end of the submission process.

```js
const { ShadowedBox } = require('component-hindawi-ui')

;<ShadowedBox center>
  <SubmissionStatement />
</ShadowedBox>
```
