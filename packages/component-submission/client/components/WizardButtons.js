/* eslint-disable no-nested-ternary */
import React, { Fragment } from 'react'
import { Row, Icon } from 'component-hindawi-ui'
import { Button, Spinner } from '@pubsweet/ui'
import { withTheme } from 'styled-components'

const WizardButtons = ({
  theme,
  isLast,
  isFirst,
  history,
  prevStep,
  isFetching,
  handleSubmit,
  editMode,
}) => (
  <Row justify="center" mt={4}>
    {isFetching ? (
      <Spinner />
    ) : (
      <Fragment>
        <Button
          mr={6}
          onClick={isFirst ? () => history.goBack() : prevStep}
          width={24}
        >
          <Icon color="colorSecondary" icon="caretLeft" pb={1 / 4} pr={1 / 2} />
          BACK
        </Button>
        <Button onClick={handleSubmit} primary width={24}>
          {isLast
            ? editMode
              ? 'SAVE CHANGES'
              : 'SUBMIT MANUSCRIPT'
            : 'NEXT STEP'}
          <Icon
            color="colorBackgroundHue"
            icon="caretRight"
            pb={1 / 4}
            pl={1 / 2}
          />
        </Button>
      </Fragment>
    )}
  </Row>
)

export default withTheme(WizardButtons)
// #endregion
