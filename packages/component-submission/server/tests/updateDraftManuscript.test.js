process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { updateDraftManuscriptUseCase } = require('../src/use-cases')

const chance = new Chance()

describe('Update Draft Manuscript Use Case', () => {
  it('should change the metadata info', async () => {
    const mockedModels = models.build(fixtures)
    const manuscript = fixtures.generateManuscript({})

    await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'author',
    })

    const input = {
      meta: {
        title: chance.word(),
        abstract: chance.paragraph(),
      },
      authors: [],
    }

    await updateDraftManuscriptUseCase.initialize(mockedModels).execute({
      manuscriptId: manuscript.id,
      autosaveInput: input,
    })

    expect(manuscript.abstract).toEqual(input.meta.abstract)
    expect(manuscript.title).toEqual(input.meta.title)
  })
  it('should reorder authors if the order is changed', async () => {
    const mockedModels = models.build(fixtures)
    const manuscript = fixtures.generateManuscript({})

    const authorTeamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'author',
    })
    const coAuthorTeamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'author',
    })

    const input = {
      meta: {
        title: chance.word(),
        abstract: chance.paragraph(),
      },
      authors: [
        {
          email: coAuthorTeamMember.alias.email,
        },
        {
          email: authorTeamMember.alias.email,
        },
      ],
    }

    const resultingManuscript = await updateDraftManuscriptUseCase
      .initialize(mockedModels)
      .execute({
        manuscriptId: manuscript.id,
        autosaveInput: input,
      })

    expect(resultingManuscript.authors[0].alias.email).toEqual(
      coAuthorTeamMember.alias.email,
    )
  })
  it('should reorder files if the order is changed', async () => {
    const mockedModels = models.build(fixtures)
    const manuscript = fixtures.generateManuscript({})
    const manuscriptFile = fixtures.generateFile({
      manuscriptId: manuscript.id,
      type: 'manuscript',
      position: 0,
    })

    const manuscriptFile2 = fixtures.generateFile({
      manuscriptId: manuscript.id,
      type: 'manuscript',
      position: 1,
    })

    const supplementaryFile = fixtures.generateFile({
      manuscriptId: manuscript.id,
      type: 'supplementary',
      position: 0,
    })

    manuscript.files.push(manuscriptFile, manuscriptFile2, supplementaryFile)

    const authorTeamMember = await dataService.createUserOnManuscript({
      manuscript,
      fixtures,
      role: 'author',
    })

    const input = {
      meta: {
        title: chance.word(),
        abstract: chance.paragraph(),
      },
      authors: [
        {
          email: authorTeamMember.alias.email,
        },
      ],
      files: [
        {
          id: manuscriptFile2.id,
          type: manuscriptFile2.type,
        },
        {
          id: manuscriptFile.id,
          type: manuscriptFile.type,
        },
        {
          id: supplementaryFile.id,
          type: supplementaryFile.type,
        },
      ],
    }

    const resultingManuscript = await updateDraftManuscriptUseCase
      .initialize(mockedModels)
      .execute({
        manuscriptId: manuscript.id,
        autosaveInput: input,
      })

    expect(resultingManuscript.files[0].type).toEqual('manuscript')
    expect(resultingManuscript.files[0].id).toEqual(manuscriptFile2.id)
  })
})
