const { get } = require('lodash')

const initialize = ({
  models,
  sendPackage,
  notificationService,
  logEvent,
}) => ({
  execute: async ({ manuscriptId, reqUserId }) => {
    const manuscript = await models.Manuscript.find(
      manuscriptId,
      '[files,teams.members]',
    )

    await manuscript.submitManuscript()

    await manuscript.save()

    await sendPackage({ manuscript })

    const authorTeam = await models.Team.findOneBy({
      queryObject: {
        manuscriptId,
        role: 'author',
      },
      eagerLoadRelations: 'members.[user.[identities]]',
    })
    const submittingAuthor = authorTeam.members.find(
      author => author.isSubmitting,
    )

    const confirmedCoAuthors = authorTeam.members
      .filter(author => !author.isSubmitting)
      .filter(author => get(author, 'user.identities.0.isConfirmed'))

    const unconfirmedCoAuthors = authorTeam.members
      .filter(author => !author.isSubmitting)
      .filter(author => !get(author, 'user.identities.0.isConfirmed'))

    confirmedCoAuthors.forEach(author =>
      notificationService.sendToConfirmedAuthors(author, {
        manuscript,
        submittingAuthor,
      }),
    )

    unconfirmedCoAuthors.forEach(author =>
      notificationService.sendToUnconfirmedAuthors(author, {
        manuscript,
        submittingAuthor,
      }),
    )

    notificationService.sendSubmittingAuthorConfirmation({
      manuscript,
      submittingAuthor,
    })
    notificationService.sendEQSEmail({ manuscript })

    logEvent({
      userId: reqUserId,
      manuscriptId,
      action: logEvent.actions.manuscript_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
