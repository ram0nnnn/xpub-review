const { findIndex, sortBy } = require('lodash')

const initialize = ({ models: { Team, Manuscript }, logEvent }) => ({
  execute: async ({ manuscriptId, authorTeamMemberId, reqUserId }) => {
    const authorTeam = await Team.findOneByField(
      'manuscriptId',
      manuscriptId,
      'members.[user.[identities]]',
    )

    authorTeam.removeMember(authorTeamMemberId)

    const teamHasCorrespondingAuthor = authorTeam.members.some(
      a => a.isCorresponding,
    )
    if (!teamHasCorrespondingAuthor) {
      const submittingAuthorIndex = findIndex(authorTeam.members, [
        'isSubmitting',
        true,
      ])
      authorTeam.members[submittingAuthorIndex].isCorresponding = true
    }

    await authorTeam.saveRecursively()
    const manuscript = await Manuscript.find(manuscriptId)
    if (manuscript.status !== 'draft') {
      logEvent({
        userId: reqUserId,
        manuscriptId,
        action: logEvent.actions.author_removed,
        objectType: logEvent.objectType.user,
        objectId: authorTeamMemberId,
      })
    }

    return sortBy(authorTeam.members, 'position').map(a => a.toDTO())
  },
})

const authsomePolicies = ['authenticatedUser']

module.exports = {
  initialize,
  authsomePolicies,
}
