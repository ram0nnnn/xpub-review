const {
  jobs: { connectToJobQueue },
} = require('pubsweet-server/src')
const logger = require('@pubsweet/logger')

module.exports = {
  schedule: async ({ queue, jobHandler, executionDate, params }) => {
    const jobQueue = await connectToJobQueue()

    // Add job to the queue
    await jobQueue.publishAfter(queue, params, {}, executionDate)

    // Subscribe to the job queue with an async handler
    await jobQueue.subscribe(queue, jobHandler)

    await jobQueue.onComplete(queue, job => {
      logger.info(job.data.response.value)
    })
  },
  cancelQueue: async name => {
    const jobQueue = await connectToJobQueue()

    try {
      await jobQueue.unsubscribe(name)
      logger.info(`Successfully unsubscribed from queue: ${name}`)
    } catch (e) {
      logger.error(e)
    }
  },
}
