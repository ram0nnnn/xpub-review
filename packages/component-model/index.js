const HindawiBaseModel = require('./src/hindawiBaseModel')
const typeDefs = require('./graphql-schema')

module.exports = {
  HindawiBaseModel,
  typeDefs,
}
