const BaseModel = require('@pubsweet/base-model')
const { NotFoundError } = require('@pubsweet/errors')

const parseEagerRelations = relations =>
  Array.isArray(relations) ? `[${relations.join(', ')}]` : relations

class HindawiBaseModel extends BaseModel {
  static async find(id, eagerLoadRelations) {
    const object = await this.query()
      .findById(id)
      .eager(parseEagerRelations(eagerLoadRelations))

    if (!object) {
      throw new NotFoundError(`Object not found: ${this.name} with 'id' ${id}`)
    }

    return object
  }

  static async findOneByField(field, value, eagerLoadRelations) {
    const object = await this.query()
      .where(field, value)
      .limit(1)
      .eager(parseEagerRelations(eagerLoadRelations))

    if (!object.length) {
      return
    }

    return object[0]
  }

  async saveRecursively() {
    return this.constructor
      .query()
      .upsertGraph(this, { insertMissing: true, relate: true })
  }

  static async all(eagerLoadRelations) {
    return this.query().eager(parseEagerRelations(eagerLoadRelations))
  }

  static async findAllOrderedBy({ eagerLoadRelations, orderByField, order }) {
    return this.query()
      .orderBy(orderByField, order)
      .eager(parseEagerRelations(eagerLoadRelations))
  }
  static async findOrderedBy({
    queryObject,
    eagerLoadRelations,
    orderByField,
    order,
  }) {
    return this.query()
      .where(queryObject)
      .orderBy(orderByField, order)
      .eager(parseEagerRelations(eagerLoadRelations))
  }
  static async findOneBy({ queryObject, eagerLoadRelations }) {
    const object = await this.query()
      .where(queryObject)
      .limit(1)
      .eager(parseEagerRelations(eagerLoadRelations))

    if (!object.length) {
      return
    }

    return object[0]
  }

  static findBy(queryObject, eagerLoadRelations) {
    return this.query()
      .where(queryObject)
      .eager(parseEagerRelations(eagerLoadRelations))
  }

  static findIn(field, options, eagerLoadRelations) {
    return this.query()
      .whereIn(field, options)
      .eager(parseEagerRelations(eagerLoadRelations))
  }

  static async findManuscriptsBySubmissionId({
    submissionId,
    excludedStatus,
    eagerLoadRelations,
    orderByField,
    order,
  }) {
    return this.query()
      .skipUndefined()
      .whereNot({ status: excludedStatus })
      .andWhere({ submissionId })
      .orderBy(orderByField, order)
      .eager(parseEagerRelations(eagerLoadRelations))
  }
}

module.exports = HindawiBaseModel
